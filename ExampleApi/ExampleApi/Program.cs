using ExampleApi.Controllers;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers().ConfigureApiBehaviorOptions(options =>
{
    options.SuppressConsumesConstraintForFormFileParameters = false; // set true to disable Multipart/form-data request inference eg when [FromForm] attribute is used - The multipart/form-data request content type is inferred
    options.SuppressInferBindingSourcesForParameters = false; // set true to disable inference rules eg [FromBody] is inferred for complex type parameters. 
    options.SuppressModelStateInvalidFilter = false; // set true to disable automatic 400 response
    options.SuppressMapClientErrors = false; // set true to disable ProblemDetails response
    options.ClientErrorMapping[StatusCodes.Status404NotFound].Link = "https://httpstatuses.com/404";
}); 

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { 
        Title = "My API", 
        Version = "v1",
        Description = "Calculates the cost of a picture frame based on its dimensions.",
        TermsOfService = new Uri("https://go.microsoft.com/fwlink/?LinkID=206977"),
        Contact = new OpenApiContact
        {
            Name = "Your name",
            Email = "mymail@test.com",
            Url = new Uri("https://www.microsoft.com/learn")
        }
    });

    // csproj requires <GenerateDocumentationFile>True</GenerateDocumentationFile>
    // This then adds a xml to the build which swagger can pick up and include
    // things such as /// <summary>  /// <remarks> are added to the xml file
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});

builder.Services.AddSingleton(new List<Data>()
        {
            new Data(1, "bob"),
            new Data(2, "jane")
        });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
    });
}

app.UseAuthorization();

app.MapControllers();

app.Run();
