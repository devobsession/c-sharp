using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ExampleApi.Controllers
{
    // https://docs.microsoft.com/en-us/aspnet/core/web-api/?view=aspnetcore-6.0
    [ApiController]
    [Route("[controller]")]
    public class CrudController : ControllerBase
    {       
        private readonly ILogger<CrudController> _logger;
        private List<Data> _testData;

        public CrudController(ILogger<CrudController> logger, List<Data> testData)
        {
            _logger = logger;
            _testData = testData;
        }

        [HttpGet()]
        public IActionResult Get()
        {
            return Ok(_testData);
        }

        // ActionResults such as NotFound will return problem details automaticly. See Program.cs on how to disable
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var item = _testData.SingleOrDefault(x => x.Id == id);

            if (item is null)
            {
                return NotFound();
            }

            return Ok();
        }

        [HttpPost()]
        public IActionResult Add(Data data)
        {
            data.Id = 10;

            _testData.Add(data);

            return CreatedAtAction(nameof(Add), new { id = data.Id }, data);
        }

        // Note: auto validation will throw a 400 if the json does not include the fields required for the data model. See Program.cs on how to disable
        // Returns IActionResult because the ActionResult return type isn't known until runtime
        [HttpPut("{id}")]
        public IActionResult Update(int id, Data data)
        {
            if (id != data.Id)
            {
                return BadRequest(); // BadRequestResult
            }

            var item = _testData.SingleOrDefault(x => x.Id == id);

            if (item is null)
            {
                return NotFound(); // NotFoundResult
            }

            item.Name = data.Name;

            return NoContent(); // NoContentResult
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, Data data)
        {
            var item = _testData.RemoveAll(x => x.Id == id);

            return NoContent();
        }


        /// <summary>
        /// Example summary for swagger. xxxxxxxx
        /// to show the operation Id add the following to the url: ?displayOperationId=1
        /// </summary>
        /// <remarks>
        /// Here is a sample remarks placeholder. xxxxxxxxxxxx
        ///
        /// Sample request:
        ///
        ///     Get /api/priceframe/5/10
        ///
        /// </remarks>
        /// <param name="id">The id of the data xxxxxxx</param>
        /// <param name="data">The data xxxxxxxxxxx</param>
        /// <returns>The id xxxxxxxxx</returns>
        /// <response code="200">Adds comments to the response type xxxxxxxxx</response>
        [HttpPost("SwaggerDocumentationExample/{id}", Name = "This is an operationId: optional unique string used to identify an operation xxxxxxx")]
        [Produces("text/plain", "application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Data), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        // [ApiConventionMethod(typeof(DefaultApiConventions))] // can use this instead of specifying individual StatusCodes
        public IActionResult SwaggerDocumentationExample(int id, Data data)
        {           
            return Ok(data);
        }
    }

    // https://docs.microsoft.com/en-us/aspnet/core/mvc/models/validation?view=aspnetcore-6.0
    public class Data
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string Name { get; set; }

        public Data(int id, string name)
        {
            Id = id;
            Name = name;
        }

    }
}