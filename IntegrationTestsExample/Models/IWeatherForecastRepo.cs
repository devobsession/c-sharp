﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shared
{
    public interface IWeatherForecastRepo
    {
        Task<List<WeatherForecast>> Get();
    }
}