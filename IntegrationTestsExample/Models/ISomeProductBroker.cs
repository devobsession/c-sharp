﻿using Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shared
{
    public interface ISomeProductBroker
    {
        Task<List<SomeProduct>> GetSomeProducts();
    }
}