﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo
{
    public class DbContext
    {
        public IMongoDatabase Database { get; private set; }

        public DbContext(MongoSettings mongoSettings)
        {
            ConventionPack();
            RegisterClassMap();

            var client = new MongoClient(mongoSettings.ConnectionString);
            Database = client.GetDatabase("WeatherForecastCollection");
        }

        private void ConventionPack()
        {
            ConventionPack conventionPack = new ConventionPack
            {
                new CamelCaseElementNameConvention(),
                new IgnoreExtraElementsConvention(true),
                new EnumRepresentationConvention(BsonType.String)
            };
            ConventionRegistry.Register("DbConventions", conventionPack, t => true);
        }

        private void RegisterClassMap()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(WeatherForecast)))
            {
                BsonClassMap.RegisterClassMap<WeatherForecast>(cm =>
                {
                    cm.AutoMap();
                    cm.SetIgnoreExtraElements(true);
                    cm.MapIdMember(x => x.Id);
                });
            }
        }
    }
}
