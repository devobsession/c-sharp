﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Repo
{
    public class SomeProductBroker : ISomeProductBroker
    {
        private readonly HttpClient _httpClient;

        public SomeProductBroker(SomeProductSettings someProdcutSettings)
        {
            _httpClient = new HttpClient()
            { 
                BaseAddress = new Uri(someProdcutSettings.BaseUrl)
            };
        }

        public async Task<List<SomeProduct>> GetSomeProducts()
        {
            var getProductResponse = await _httpClient.GetAsync("/SomeProduct");

            getProductResponse.EnsureSuccessStatusCode();

            var json = await getProductResponse.Content.ReadAsStringAsync();

            var someProducts = JsonConvert.DeserializeObject<List<SomeProduct>>(json);

            return someProducts;
        }
    }
}
