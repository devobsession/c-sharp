﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repo
{
    public class WeatherForecastRepo : IWeatherForecastRepo
    {
        private readonly IMongoCollection<WeatherForecast> _collection;

        public WeatherForecastRepo(DbContext dbContext)
        {
            _collection = dbContext.Database.GetCollection<WeatherForecast>(nameof(WeatherForecast));
        }

        public async Task<List<WeatherForecast>> Get()
        {
            return await _collection
            .Find("{}")
            .ToListAsync();
        }
    }
}
