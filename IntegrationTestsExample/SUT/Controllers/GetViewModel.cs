﻿using Shared;
using System.Collections.Generic;

namespace SUT.Controllers
{
    public class GetViewModel
    {
        public List<WeatherForecast> WeatherForecast { get; set; }
        public List<SomeProduct> SomeProducts { get; set; }
    }
}
