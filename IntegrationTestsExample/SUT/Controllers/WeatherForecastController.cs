﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SUT.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly IWeatherForecastRepo _weatherForecastRepo;
        private readonly ISomeProductBroker _someProductBroker;

        public WeatherForecastController(IWeatherForecastRepo weatherForecastRepo, ISomeProductBroker someProductBroker)
        {
            _weatherForecastRepo = weatherForecastRepo;
            _someProductBroker = someProductBroker;
        }

        [HttpGet]
        public async Task<GetViewModel> Get()
        {
            var products = await _someProductBroker.GetSomeProducts();

            var weatherForecast = await _weatherForecastRepo.Get();

            return new GetViewModel()
            { 
                WeatherForecast = weatherForecast,
                SomeProducts = products
            };
        }
    }
}
