using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using NUnit.Framework;
using Repo;
using Shared;
using SUT;
using SUT.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace IntegrationTests
{
    public class GetWeatherForecastTests
    {
        private static WeatherForecast[] _weatherForecasts;

        [SetUp]
        public void Setup()
        {
            SetupData();
        }

        [Test]
        public async Task GetWeatherForecast_Succeeds()
        {
            // Arrange

            // Act
            var response = await SetUpFixture.HttpClient.GetAsync("/WeatherForecast");

            // Assert
            response.EnsureSuccessStatusCode();
            var json = await response.Content.ReadAsStringAsync();
            var getViewModel = JsonConvert.DeserializeObject<GetViewModel>(json);
            Assert.IsTrue(getViewModel.WeatherForecast.Count == _weatherForecasts.Length);
        }

        private static void SetupData()
        {
            string[] Summaries = new[]
            {
                "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
            };
            var rng = new Random();
            _weatherForecasts = Enumerable.Range(1, 5).Select(index => new WeatherForecast()
            {
                Id = Guid.NewGuid(),
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();

            using (var scope = SetUpFixture.Server.Host.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<DbContext>();
                dbContext.Database.DropCollection(nameof(WeatherForecast));

                var collection = dbContext.Database.GetCollection<WeatherForecast>(nameof(WeatherForecast));

                collection.InsertMany(_weatherForecasts);
            }
        }
    }
}