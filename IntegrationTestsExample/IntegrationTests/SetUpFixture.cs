﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Repo;
using Shared;
using SUT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IntegrationTests
{
    [SetUpFixture]
    public class SetUpFixture
    {
        public static TestServer Server;
        public static HttpClient HttpClient;
        public static MongoDocker MongoDocker;

        [OneTimeSetUp]
        public static void Init()
        {
            var dockerUtilities = new DockerUtilities();
            MongoDocker = new MongoDocker(
                dockerUtilities,
                "adminuser",
                "mypassword",
                "IntegrationTestsMongo",
                "IntegrationTestsVolume",
                dockerUtilities.GetFreePort());
            MongoDocker.EnsureContainerRunning().GetAwaiter().GetResult();

            Environment.SetEnvironmentVariable("MongoSettings__ConnectionString", MongoDocker.GetConnectionString());

            var configuration = new ConfigurationBuilder()
                .AddEnvironmentVariables().Build();

            var builder = new WebHostBuilder()
                .UseEnvironment("Integration Tests")
                .UseConfiguration(configuration)
                .UseStartup<Startup>();

            Server = new TestServer(builder);
            HttpClient = Server.CreateClient();
        }

        [OneTimeTearDown]
        public static void TearDown()
        {
            MongoDocker.TearDownContainers().GetAwaiter().GetResult();
        }
    }
}
