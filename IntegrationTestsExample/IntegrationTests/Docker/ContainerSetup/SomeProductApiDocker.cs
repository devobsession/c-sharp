﻿using MongoDB.Bson;
using MongoDB.Driver;
using Shared;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace IntegrationTests
{
    public class SomeProductApiDocker
    {
        private readonly DockerUtilities _dockerUtilities;
        private readonly string _imageName;
        private readonly string _containerName;
        private readonly string _exposePort;

        public SomeProductApiDocker(DockerUtilities dockerUtilities, string containerName, string exposePort = "80")
        {
            _dockerUtilities = dockerUtilities;
            _imageName= "developerbrandonp/integration-test-sample:latest";
            _containerName = containerName;
            _exposePort = exposePort;
        }

        public async Task EnsureContainerRunning()
        {
            await _dockerUtilities.RemoveContainers(_containerName);

            await _dockerUtilities.DownloadImage(_imageName);

            var envVars = new List<string>
                        {
                            $"APP_MODE=MockService"
                        };

            var portBindings = new List<DockerPortBinding>()
            { 
                new DockerPortBinding("80", _exposePort)
            };

            var containerId = await _dockerUtilities.CreateContainer(_containerName, _imageName, envVars, portBindings);

            await WaitUntilAvailable();
        }

        public async Task TearDownContainers()
        {
            await _dockerUtilities.RemoveContainers(_containerName, 0);
        }

        public async Task WaitUntilAvailable(int timeout = 60)
        {
            var start = DateTime.UtcNow;
            var connectionEstablished = false;
            while (!connectionEstablished && start.AddSeconds(timeout) > DateTime.UtcNow)
            {
                try
                {
                    using (HttpClient httpClient = new HttpClient())
                    {
                        var getProductResponse = await httpClient.GetAsync($"localhost:{_exposePort}/SomeProduct");

                        getProductResponse.EnsureSuccessStatusCode();
                    }
                                                      
                    connectionEstablished = true;
                }
                catch
                {
                    await Task.Delay(500);
                }
            }

            if (!connectionEstablished)
            {
                throw new Exception($"Connection to the Mongo docker database could not be established within {timeout} seconds.");
            }

            return;
        }

    }
}
