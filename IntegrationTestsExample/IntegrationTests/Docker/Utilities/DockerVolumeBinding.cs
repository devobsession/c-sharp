﻿namespace IntegrationTests
{
    public class DockerVolumeBinding
    {
        public string Volume { get; set; }
        public string HostLocation { get; set; }

        public DockerVolumeBinding(string volume, string hostLocation)
        {
            Volume = volume;
            HostLocation = hostLocation;
        }
    }
}
