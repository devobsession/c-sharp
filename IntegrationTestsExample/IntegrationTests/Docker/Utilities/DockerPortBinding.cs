﻿namespace IntegrationTests
{
    public class DockerPortBinding
    {
        public string AppsPort { get; set; }
        public string ExposedPort { get; set; }

        public DockerPortBinding(string appsPort, string exposedPort)
        {
            AppsPort = appsPort;
            ExposedPort = exposedPort;
        }
    }
}
