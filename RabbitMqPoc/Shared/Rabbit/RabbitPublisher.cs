﻿using RabbitMQ.Client;
using System.Text;

namespace Shared
{
    public class RabbitPublisher
    {
        private readonly RabbitConnection _rabbitConnection;
        private readonly IBasicProperties _props;
        private ExchangeDefinition _outboundExchangeDefinition;

        public RabbitPublisher(RabbitConnection rabbitConnection, RabbitBrokerDefinitions rabbitBrokerDefinitions)
        {
            _rabbitConnection = rabbitConnection;
            _props = _rabbitConnection.Channel.CreateBasicProperties();
            _props.DeliveryMode = 2;
            _outboundExchangeDefinition = rabbitBrokerDefinitions.OutboundExchange;
        }

        public void Publish(string message, string routingKey)
        {
            byte[] body = Encoding.UTF8.GetBytes(message);

            try
            {
                _rabbitConnection.Channel.BasicPublish(exchange: _outboundExchangeDefinition.Name,
                                     routingKey: routingKey,
                                     basicProperties: _props,
                                     body: body);
            }
            catch(System.Exception)
            {
                _rabbitConnection.Connect();
                Publish(message, routingKey);
            }
            
        }
        
    }
}
