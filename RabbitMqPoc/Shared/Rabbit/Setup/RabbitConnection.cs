﻿using Shared;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Shared
{
    public class RabbitConnection : IDisposable
    {
        private readonly string _connectionString;
        private readonly ConnectionFactory _factory;
        private IConnection _connection;
        public IModel Channel { get; private set; }
        
        public RabbitConnection(RabbitConnectionSettings rabbitConnectionSettings)
        {
            _connectionString = $"amqp://{rabbitConnectionSettings.UserName}:{rabbitConnectionSettings.Password}@{rabbitConnectionSettings.HostName}:{rabbitConnectionSettings.Port}{rabbitConnectionSettings.VirtualHost}";
         
            _factory = new ConnectionFactory()
            {
                HostName = rabbitConnectionSettings.HostName,
                Port = rabbitConnectionSettings.Port,
                UserName = rabbitConnectionSettings.UserName,
                Password = rabbitConnectionSettings.Password,
                VirtualHost = rabbitConnectionSettings.VirtualHost
            };

            Connect();
        }

        public void Connect(int count = 1)
        {
            try
            {
                _connection = _factory.CreateConnection();
                Channel = _connection.CreateModel();
            }
            catch(Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Rabbit connection failed: {ex.Message}");
                int reconnectTimeout = count * 1000;
                Console.WriteLine($"Timeout for {reconnectTimeout}ms before attempting to reconnect");
                Console.WriteLine($"Attempting to reconnect to: {_connectionString}");
                Console.ForegroundColor = ConsoleColor.Gray;
                Thread.Sleep(reconnectTimeout);
                Connect(count*2);
            }
        }
        
        #region IDisposable Support
        private bool disposedValue = false;
        
        protected virtual void Dispose(bool disposing)
        {
            if(!disposedValue)
            {
                if(disposing)
                {
                    _connection.Dispose();
                    Channel.Dispose();
                }
                
                disposedValue = true;
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
