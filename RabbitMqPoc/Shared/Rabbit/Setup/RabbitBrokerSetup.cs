﻿using Shared;
using RabbitMQ.Client;

namespace Shared
{
    public class RabbitBrokerSetup
    {
        public RabbitBrokerSetup(RabbitBrokerDefinitions rabbitBrokerDefinitions, RabbitConnection rabbitConnection)
        {            
            BindInboundToQueue(rabbitConnection.Channel, rabbitBrokerDefinitions.InboundExchange, rabbitBrokerDefinitions.Queue);
            BindEventbusToInbound(rabbitConnection.Channel, rabbitBrokerDefinitions.InboundExchange);
            BindRerouterToQueue(rabbitConnection.Channel, rabbitBrokerDefinitions.Queue);

            BindOutboundToEventbus(rabbitConnection.Channel, rabbitBrokerDefinitions.OutboundExchange);
        }

        private void BindOutboundToEventbus(IModel channel, ExchangeDefinition outboundExchange)
        {
            channel.ExchangeDeclare(
               exchange: outboundExchange.Name,
               type: outboundExchange.Type,
               autoDelete: outboundExchange.AutoDelete,
               durable: outboundExchange.Durable);

            channel.ExchangeDeclare(exchange: "eventbus", type: "fanout", durable: true, autoDelete: false);

            foreach(string routingKey in outboundExchange.RoutingKeys)
            {
                channel.ExchangeBind("eventbus", outboundExchange.Name, routingKey);
            }
        }
        
        private void BindInboundToQueue(IModel channel, ExchangeDefinition inboundExchange, QueueDefinition queue)
        {
            channel.ExchangeDeclare(
               exchange: inboundExchange.Name,
               type: inboundExchange.Type,
               durable: inboundExchange.Durable,
               autoDelete: inboundExchange.AutoDelete);

            channel.QueueDeclare(
             queue: queue.Name,
             durable: queue.Durable,
             exclusive: queue.Exclusive,
             autoDelete: queue.AutoDelete,
             arguments: queue.Arguments);

            foreach(string routingKey in inboundExchange.RoutingKeys)
            {
                channel.QueueBind(queue.Name, inboundExchange.Name, routingKey);
            }
        }

        private void BindRerouterToQueue(IModel channel, QueueDefinition queue)
        {
            channel.ExchangeDeclare(
              exchange: "rerouter",
              type: "topic",
              autoDelete: false,
              durable: true);

            channel.QueueBind(queue.Name, "rerouter", $"{queue.Name}.#");
        }

        private void BindEventbusToInbound(IModel channel, ExchangeDefinition inboundExchange)
        {
            channel.ExchangeDeclare(exchange: "eventbus", type: "fanout", durable: true, autoDelete: false);
            channel.ExchangeBind(inboundExchange.Name, "eventbus", "#");
        }

    }
}
