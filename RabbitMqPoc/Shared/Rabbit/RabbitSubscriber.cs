﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace Shared
{
    public class RabbitSubscriber
    {
        private const string _retryCount = "retry-count";
        private readonly RabbitConnection _rabbitConnection;
        private readonly QueueDefinition _queueDefinition;
        private readonly RabbitRetry _rabbitRetry;

        public RabbitSubscriber(RabbitConnection rabbitConnection, RabbitBrokerDefinitions rabbitBrokerDefinitions, RabbitRetry rabbitRetry)
        {
            _rabbitConnection = rabbitConnection;
            _queueDefinition = rabbitBrokerDefinitions.Queue;
            _rabbitRetry = rabbitRetry;
        }

        /// <summary>
        /// Throw Exception to reject and send to dead letter exchange
        /// return false to put on retry queue
        /// </summary>
        /// <param name="funcReturnSuccessBool"></param>
        public void Subscribe(Func<string, bool> funcReturnSuccessBool)
        {
            EventingBasicConsumer consumer = new EventingBasicConsumer(_rabbitConnection.Channel);

            consumer.Received += (model, ea) =>
            {
                string message = Encoding.UTF8.GetString(ea.Body.Span);

                try
                {
                    bool isSuccess = funcReturnSuccessBool(message);
                    if (isSuccess)
                    {
                        _rabbitConnection.Channel.BasicAck(ea.DeliveryTag, false);
                    }
                    else
                    {
                        _rabbitConnection.Channel.BasicReject(ea.DeliveryTag, requeue: false);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Message}");
                    _rabbitRetry.Retry(message, GetTryCount(ea));
                }
            };

            _rabbitConnection.Channel.BasicConsume(queue: _queueDefinition.Name,
                                 autoAck: false,
                                 consumer: consumer);
        }

        private int GetTryCount(BasicDeliverEventArgs ea)
        {
            int retryCount = 0;
            if (ea.BasicProperties.IsHeadersPresent() && ea.BasicProperties.Headers.TryGetValue(_retryCount, out object retryHeader))
            {
                try
                {
                    retryCount = Convert.ToInt32(retryHeader);
                }
                catch (Exception)
                {
                }
            }

            return retryCount + 1;
        }
    }
}
