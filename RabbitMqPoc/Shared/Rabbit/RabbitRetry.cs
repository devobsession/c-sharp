﻿using RabbitMQ.Client;
using System.Text;
using System;
using System.Collections.Generic;

namespace Shared
{
    public class RabbitRetry
    {
        private const string _retryCount = "retry-count";
        private readonly RabbitConnection _rabbitConnection;
        private readonly IBasicProperties _props;
        private ExchangeDefinition _retryExchange;
        private QueueDefinition _queueDefinition;

        public RabbitRetry(RabbitConnection rabbitConnection, RabbitBrokerDefinitions rabbitBrokerDefinitions)
        {
            _rabbitConnection = rabbitConnection;
            _props = _rabbitConnection.Channel.CreateBasicProperties();
            _props.DeliveryMode = 2;
            _retryExchange = rabbitBrokerDefinitions.RetryExchange;
            _queueDefinition = rabbitBrokerDefinitions.Queue;
        }
        
        public void Retry(string message, int retryCount)
        {
            int waitTime = GetWaitTime(retryCount);
            byte[] body = Encoding.UTF8.GetBytes(message);

            try
            {
                _props.Headers = new Dictionary<string, object>();
                _props.Headers.Add(_retryCount, retryCount+1);
                _rabbitConnection.Channel.BasicPublish(exchange: _retryExchange.Name,
                                     routingKey: $"{_queueDefinition.Name}.{waitTime}",
                                     basicProperties: _props,
                                     body: body);
            }
            catch(Exception)
            {
                _rabbitConnection.Connect();
                Retry(message, retryCount);
            }
        }

        private static int GetWaitTime(int retryCount)
        {
            switch(retryCount)
            {
                case 0:
                case 1:
                    return 0;
                case 2:
                    return 5;
                default:
                    return 20;
            }
        }
    }
}
