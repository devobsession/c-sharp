﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
    public class RabbitBrokerDefinitions
    {
        public ExchangeDefinition RetryExchange { get; set; }
        public ExchangeDefinition OutboundExchange { get; set; }
        public ExchangeDefinition InboundExchange { get; set; }
        public QueueDefinition Queue { get; set; }
    }    
}
