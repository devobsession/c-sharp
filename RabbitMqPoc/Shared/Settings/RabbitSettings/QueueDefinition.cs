﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
    public class QueueDefinition
    {
        public string Name { get; set; }
        public Dictionary<string, object> Arguments { get; set; }
        public bool Durable { get; set; }
        public bool Exclusive { get; set; }
        public bool AutoDelete { get; set; }
    }
}
