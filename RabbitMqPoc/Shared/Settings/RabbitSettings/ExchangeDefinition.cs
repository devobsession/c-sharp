﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
    public class ExchangeDefinition
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Durable { get; set; }
        public bool AutoDelete { get; set; }
        public List<string> RoutingKeys { get; set; }
    }
}
