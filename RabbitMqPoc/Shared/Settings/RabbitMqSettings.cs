﻿using Newtonsoft.Json;
using System.IO;

namespace Shared
{
    public class RabbitMqSettings
    {
        public EnvironmentSettings Env { get; set; }
        public AppSettings App { get; set; }

        public RabbitMqSettings(string path)
        {
            App = GetAppSettings(path);
            Env = new EnvironmentSettings();
        }

        public static AppSettings GetAppSettings(string path)
        {
            string appSettingsJson = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<AppSettings>(appSettingsJson);
        }
    }
}
