﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shared
{
    public class EnvironmentSettings
    {
        public RabbitConnectionSettings RabbitConnectionSettings { get; set; }
        
        public EnvironmentSettings()
        {
            bool portParsed = int.TryParse(Environment.GetEnvironmentVariable("RABBIT_PORT"), out int port);
            RabbitConnectionSettings = new RabbitConnectionSettings()
            {
                HostName = Environment.GetEnvironmentVariable("RABBIT_HOST_NAME") ?? "localhost",
                Port = portParsed ? port : 5672,
                UserName = Environment.GetEnvironmentVariable("RABBIT_USER_NAME") ?? "guest",
                Password = Environment.GetEnvironmentVariable("RABBIT_PASSWORD") ?? "guest",
                VirtualHost = Environment.GetEnvironmentVariable("RABBIT_VIRTUALHOST") ?? "/"
            };
        }
    }
}
