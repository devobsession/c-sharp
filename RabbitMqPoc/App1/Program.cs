﻿using Shared;
using System;

namespace App1
{
    class Program
    {
        static void Main(string[] args)
        {
            RabbitMqSettings settings = new RabbitMqSettings(@"./appSettings.json");

            // Rabbit Setup
            RabbitConnection rabbitConnection = new RabbitConnection(settings.Env.RabbitConnectionSettings);
            RabbitBrokerSetup rabbitBrokerSetup = new RabbitBrokerSetup(settings.App.RabbitBrokerDefinitions, rabbitConnection);
            RabbitPublisher rabbitPublisher = new RabbitPublisher(rabbitConnection, settings.App.RabbitBrokerDefinitions);
            RabbitRetry rabbitRetry = new RabbitRetry(rabbitConnection, settings.App.RabbitBrokerDefinitions);
            RabbitSubscriber rabbitSubscriber = new RabbitSubscriber(rabbitConnection, settings.App.RabbitBrokerDefinitions, rabbitRetry);

            rabbitSubscriber.Subscribe((msg) =>
            {
                if (msg == "ex") // example of bad message which never recovers
                {
                    return false;
                }
                else if (msg == "retry") // example of message which may be able to be processed after some time
                {
                    Random randomGen = new Random();
                    int randomInt = randomGen.Next();

                    if (randomInt % 3 == 0)
                    {
                        throw new Exception("bad message");
                    }
                }

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine();
                Console.WriteLine("Message Recieved: " + msg);
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Gray;
                return true;
            });

            Console.WriteLine("input \"ex\" for the subscriber to throw an exception and reject (sending it to )");

            Console.WriteLine($"Outbound routingKeys {string.Join(',', settings.App.RabbitBrokerDefinitions.OutboundExchange.RoutingKeys)}");
            Console.WriteLine("What routingKey for published messages?");
            string routingKey = Console.ReadLine();
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("What would you like to publish?");
                string input = Console.ReadLine();
                rabbitPublisher.Publish(input, routingKey);
                Console.WriteLine($"Publishing {input}");
            }
        }
    }
}
