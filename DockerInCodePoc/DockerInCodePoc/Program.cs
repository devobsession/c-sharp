﻿using SetupDockerMongo;
using SetupDockerSql;
using Shared;
using System;

namespace DockerInCodePoc
{
    class Program
    {
        static void Main(string[] args)
        {
            var dockerUtilities = new DockerUtilities();

            var mongoDocker = new MongoDocker(
                dockerUtilities,
                "myAdminUser",
                "bB8*12345678",
                "mongo_container_fromcode",
                "mongo_volume_fromcode");
            mongoDocker.EnsureContainerRunning().GetAwaiter().GetResult();
            // mongoDocker.TearDownContainers().GetAwaiter().GetResult();


            var sqlDocker = new SqlDocker(
                dockerUtilities,
                "bB8*12345678",
                "mssql_container_fromcode",
                "sql_volume_fromcode");
            sqlDocker.EnsureContainerRunning().GetAwaiter().GetResult();
            // sqlDocker.TearDownContainers().GetAwaiter().GetResult();

        }
    }
}
