﻿using Shared;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SetupDockerSql
{
    public class SqlDocker
    {
        private readonly DockerUtilities _dockerUtilities;
        private readonly string _sqlPassword;
        private readonly string _sqlUser;
        private readonly string _imageName;
        private readonly string _containerName;
        private readonly string _volumeName;
        private readonly string _exposePort;

        public SqlDocker(DockerUtilities dockerUtilities, string sqlPassword, string containerName, string volumeName, string exposePort = "1433")
        {
            _dockerUtilities = dockerUtilities;
            _sqlPassword = sqlPassword;
            _sqlUser = "SA";
            _imageName = "mcr.microsoft.com/mssql/server:2019-latest";
            _containerName = containerName;
            _volumeName = volumeName;
            _exposePort = exposePort;
        }

        public async Task EnsureContainerRunning()
        {
            await _dockerUtilities.RemoveContainers(_containerName);
            await _dockerUtilities.RemoveVolumes(_volumeName);

            await _dockerUtilities.DownloadImage(_imageName);

            await _dockerUtilities.CreateVolume(_volumeName);

            var envVars = new List<string>
                        {
                            "ACCEPT_EULA=Y",
                            $"SA_PASSWORD={_sqlPassword}"
                        };

            var portBindings = new List<DockerPortBinding>()
            { 
                new DockerPortBinding("1433", _exposePort)
            };

            var volumeBindings = new List<DockerVolumeBinding>()
            { 
                new DockerVolumeBinding(_volumeName, "/my_sql_volume")
            };
            var containerId = await _dockerUtilities.CreateContainer(_containerName, _imageName, envVars, portBindings, volumeBindings);

            await WaitUntilAvailable();
        }

        public async Task TearDownContainers()
        {
            await _dockerUtilities.RemoveContainers(_containerName, 0);
            await _dockerUtilities.RemoveVolumes(_volumeName, 0);
        }

        public string GetConnectionString()
        {
            return $"Data Source=localhost,{_exposePort}; Integrated Security=False; User ID={_sqlUser}; Password={_sqlPassword};";
        }

        public async Task WaitUntilAvailable(int timeout = 60)
        {
            var start = DateTime.UtcNow;
            var connectionEstablished = false;
            while (!connectionEstablished && start.AddSeconds(timeout) > DateTime.UtcNow)
            {
                try
                {
                    var connectionString = GetConnectionString();
                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        await sqlConnection.OpenAsync();
                        connectionEstablished = true;
                    }
                }
                catch
                {
                    await Task.Delay(500);
                }
            }

            if (!connectionEstablished)
            {
                throw new Exception($"Connection to the SQL docker database could not be established within {timeout} seconds.");
            }

            return;
        }

    }
}
