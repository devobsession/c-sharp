﻿using Docker.DotNet;
using Docker.DotNet.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shared
{
    public class DockerUtilities
    {
        private readonly DockerClient _dockerClient;

        public DockerUtilities()
        {
            var dockerUri = Environment.OSVersion.Platform == PlatformID.Win32NT // is running on windows
                ? "npipe://./pipe/docker_engine"
                : "unix:///var/run/docker.sock";

            _dockerClient = new DockerClientConfiguration(new Uri(dockerUri))
                .CreateClient();
        }

        public async Task DownloadImage(string imageName)
        {
            using (CancellationTokenSource cts = new CancellationTokenSource())
            {
                Console.WriteLine($"Downloading {imageName}");
                var cancellationToken = cts.Token;

                var waitingTask = Task.Run(() =>
                {
                    while (true)
                    {
                        if (cancellationToken.IsCancellationRequested)
                        {
                            break;
                        }
                        Console.Write(".");
                        Thread.Sleep(100);
                    }
                }, cancellationToken);

                await _dockerClient.Images.CreateImageAsync(new ImagesCreateParameters
                {
                    FromImage = imageName
                }, null, new Progress<JSONMessage>());

                cts.Cancel();
            }
        }

        public async Task<string> GetContainerId(string containerName)
        {
            var containerList = await _dockerClient
                  .Containers.ListContainersAsync(new ContainersListParameters() { All = true });
            var containerResponse = containerList
                .Where(c => c.Names.Any(n => n.Contains(containerName))).FirstOrDefault();

            return containerResponse?.ID;
        }

        public async Task<string> CreateContainer(string containerName, string imageNameWithTag, List<string> envVars, List<DockerPortBinding> dockerPortBindings, List<DockerVolumeBinding> volumeBindings = null)
        {
            string containerId = await GetContainerId(containerName);

            if (containerId == null)
            {
                var portBindings = new Dictionary<string, IList<PortBinding>>();
                foreach (var dockerPortBinding in dockerPortBindings)
                {
                    portBindings.Add($"{dockerPortBinding.AppsPort}/tcp", new PortBinding[]
                                    {
                                        new PortBinding
                                        {
                                            HostPort = dockerPortBinding.ExposedPort
                                        }
                                    });
                }

                var binds = new List<string>();
                if (volumeBindings != null)
                {
                    foreach (var volumeBinding in volumeBindings)
                    {
                        binds.Add($"{volumeBinding.Volume}:{volumeBinding.HostLocation}");
                    }
                }                

                var newContainer = await _dockerClient
                    .Containers
                    .CreateContainerAsync(new CreateContainerParameters
                    {
                        Name = containerName,
                        Image = imageNameWithTag,
                        Env = envVars,
                        HostConfig = new HostConfig()
                        {
                            PortBindings = portBindings,
                            Binds = binds
                        },
                    });
                containerId = newContainer.ID;
            }

            await StartContainer(containerId);
            return containerId;
        }

        public async Task StartContainer(string containerId)
        {
            //var containerList = await _dockerClient
            //                .Containers.ListContainersAsync(new ContainersListParameters() { All = true });
            //var containerResponse = containerList
            //    .FirstOrDefault(c => c.ID == containerId);

            //if (containerResponse.State == "running")
            //{
            //    return;
            //}

            await _dockerClient
                    .Containers
                    .StartContainerAsync(containerId, new ContainerStartParameters());
        }

        public async Task StopContainer(string dockerContainerId)
        {
            await _dockerClient.Containers
                .StopContainerAsync(dockerContainerId, new ContainerStopParameters());
        }

        public async Task RemoveContainer(string dockerContainerId)
        {
            await _dockerClient.Containers
                .RemoveContainerAsync(dockerContainerId, new ContainerRemoveParameters());
        }

        public async Task RemoveContainers(string containerName, int hoursTillExpiration = 0)
        {
            var runningContainers = await _dockerClient.Containers
                .ListContainersAsync(new ContainersListParameters());

            foreach (var runningContainer in runningContainers.Where(cont => cont.Names.Any(n => n.Contains(containerName))))
            {
                var expiration = hoursTillExpiration > 0
                    ? hoursTillExpiration * -1
                    : hoursTillExpiration;
                if (runningContainer.Created < DateTime.UtcNow.AddHours(expiration))
                {
                    try
                    {
                        await StopContainer(runningContainer.ID);
                        await RemoveContainer(runningContainer.ID);
                    }
                    catch
                    {
                    }
                }
            }
        }

        public async Task CreateVolume(string volumeName)
        {
            var volumeList = await _dockerClient.Volumes.ListAsync();
            var volumeCount = volumeList.Volumes.Where(v => v.Name == volumeName).Count();
            if (volumeCount <= 0)
            {
                await _dockerClient.Volumes.CreateAsync(new VolumesCreateParameters()
                {
                    Name = volumeName
                });
            }
        }

        public async Task RemoveVolume(string volumeName)
        {
            await _dockerClient.Volumes.RemoveAsync(volumeName);
        }

        public async Task RemoveVolumes(string volumeName, int hoursTillExpiration = 0)
        {
            var runningVolumes = await _dockerClient.Volumes.ListAsync();

            foreach (var runningVolume in runningVolumes.Volumes.Where(v => v.Name == volumeName))
            {
                var expiration = hoursTillExpiration > 0
                    ? hoursTillExpiration * -1
                    : hoursTillExpiration;
                if (DateTime.Parse(runningVolume.CreatedAt) < DateTime.UtcNow.AddHours(expiration))
                {
                    try
                    {
                        await RemoveVolume(runningVolume.Name);
                    }
                    catch
                    {
                    }
                }
            }
        }

        public string GetFreePort()
        {
            var tcpListener = new TcpListener(IPAddress.Loopback, 0);
            tcpListener.Start();
            var port = ((IPEndPoint)tcpListener.LocalEndpoint).Port;
            tcpListener.Stop();
            return port.ToString();
        }

    }
}
