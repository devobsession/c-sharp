﻿using MongoDB.Bson;
using MongoDB.Driver;
using Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SetupDockerMongo
{
    public class MongoDocker
    {
        private readonly DockerUtilities _dockerUtilities;
        private readonly string _mongoUserName;
        private readonly string _mongoPassword;
        private readonly string _imageName;
        private readonly string _containerName;
        private readonly string _volumeName;
        private readonly string _exposePort;

        public MongoDocker(DockerUtilities dockerUtilities, string mongoUserName, string mongoPassword, string containerName, string volumeName, string exposePort = "27017")
        {
            _dockerUtilities = dockerUtilities;
            _mongoUserName = mongoUserName;
            _mongoPassword = mongoPassword;
            _imageName = "mongo:4";
            _containerName = containerName;
            _volumeName = volumeName;
            _exposePort = exposePort;
        }

        public async Task EnsureContainerRunning()
        {
            await _dockerUtilities.RemoveContainers(_containerName);
            await _dockerUtilities.RemoveVolumes(_volumeName);

            await _dockerUtilities.DownloadImage(_imageName);

            await _dockerUtilities.CreateVolume(_volumeName);

            var envVars = new List<string>
                        {
                            $"MONGO_INITDB_ROOT_USERNAME={_mongoUserName}",
                            $"MONGO_INITDB_ROOT_PASSWORD={_mongoPassword}"
                        };

            var portBindings = new List<DockerPortBinding>()
            {
                new DockerPortBinding("27017", _exposePort)
            };

            var volumeBindings = new List<DockerVolumeBinding>()
            {
                new DockerVolumeBinding(_volumeName, "/my_mongo_volume")
            };
            var containerId = await _dockerUtilities.CreateContainer(_containerName, _imageName, envVars, portBindings, volumeBindings);

            await WaitUntilAvailable();
        }

        public async Task TearDownContainers()
        {
            await _dockerUtilities.RemoveContainers(_containerName, 0);
            await _dockerUtilities.RemoveVolumes(_volumeName, 0);
        }

        public string GetConnectionString()
        {
            return $"mongodb://{_mongoUserName}:{_mongoPassword}@localhost:{_exposePort}/admin?authSource=admin&readPreference=primary";
        }

        public async Task WaitUntilAvailable(int timeout = 60)
        {
            var start = DateTime.UtcNow;
            var connectionEstablished = false;
            while (!connectionEstablished && start.AddSeconds(timeout) > DateTime.UtcNow)
            {
                try
                {
                    var connectionString = GetConnectionString();

                    var client = new MongoClient(connectionString);
                    var db = client.GetDatabase("admin");
                    await db.RunCommandAsync<BsonDocument>(new BsonDocument("ping", 1));

                    connectionEstablished = true;
                }
                catch
                {
                    await Task.Delay(500);
                }
            }

            if (!connectionEstablished)
            {
                throw new Exception($"Connection to the Mongo docker database could not be established within {timeout} seconds.");
            }

            return;
        }

    }
}
