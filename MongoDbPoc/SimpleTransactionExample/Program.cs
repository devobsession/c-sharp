﻿using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace SimpleTransactionExample;

public static class Program
{      
    public static async Task Main(string[] args)
    {
        var client = new MongoClient("mongodb://mongodb-node1:40001,mongodb-node2:40002,mongodb-node3:40003/?replicaSet=mrs0");
        var database = client.GetDatabase("TestTransactions");
        var myModelCollection = database.GetCollection<MyModel>("MyModels");
        await database.DropCollectionAsync("MyModels");
        await database.CreateCollectionAsync("MyModels");

        var success = await InsertProductsAsync(client, myModelCollection);

        Console.ReadKey();
    }
    private static async Task<bool> InsertProductsAsync(MongoClient client, IMongoCollection<MyModel> myModelCollection)
    {         
        using (var session = await client.StartSessionAsync())
        {
            session.StartTransaction();
            try
            {
                await myModelCollection.InsertOneAsync(session, new MyModel(1,1));
                await myModelCollection.InsertOneAsync(session, new MyModel(2, 2));
              
                await session.CommitTransactionAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error writing to MongoDB: " + e.Message);
                await session.AbortTransactionAsync();
                return false;
            }
        }
            return true;
    }
}

public class MyModel
{
    public int Id { get; set; }
    public int Value { get; set; }

    public MyModel(int id, int value)
    {
        Id = id;
        Value = value;
    }
}
