﻿using System;
using MongoDB.Driver;

namespace Mongo_Sample;

public class UnitOfWork
{

    private IClientSessionHandle _session;
    private readonly DbContext _dbContext;
    private readonly string _dbName;

    public Model1TransactionsRepo Model1TransactionsRepo { get; set; }
    public Model2TransactionsRepo Model2TransactionsRepo { get; set; }

    public UnitOfWork(DbContext dbContext, string dbName)
    {
        _dbContext = dbContext;
        _dbName = dbName;
    }

    public void StartTransaction()
    {
        if (_session != null)
        {
            _session.AbortTransaction();
        }

        var database = _dbContext.Client.GetDatabase(_dbName);

        var model1Collection = database.GetCollection<Model1>("Model1");
        Model1TransactionsRepo = new Model1TransactionsRepo(model1Collection);

        var model2Collection = database.GetCollection<Model2>("Model2");
        Model2TransactionsRepo = new Model2TransactionsRepo(model2Collection);

        _session = _dbContext.Client.StartSession();
        Model1TransactionsRepo.SetTransactionSession(_session);
        Model2TransactionsRepo.SetTransactionSession(_session);
        _session.StartTransaction();
    }

    public void Complete()
    {
        try
        {
            _session.CommitTransaction();
            _session = null;
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error writing to MongoDB: " + ex.Message);
            _session.AbortTransaction();
            _session = null;
        }
    }

    public void Abort()
    {
        _session.AbortTransaction();
        _session = null;
    }
}
