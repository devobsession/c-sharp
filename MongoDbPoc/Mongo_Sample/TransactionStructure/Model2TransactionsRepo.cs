﻿using MongoDB.Driver;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class Model2TransactionsRepo
{
    private readonly IMongoCollection<Model2> _model2Collection;
    private IClientSessionHandle _session;

    public Model2TransactionsRepo(IMongoCollection<Model2> model2Collection)
    {
        _model2Collection = model2Collection;
    }
    
    public async Task<Model2> Get(int id)
    {
        return await _model2Collection
        .TransactionalFind(_session, x => x.Id == id)
        .Limit(1)
        .SingleOrDefaultAsync();
    }
            
    public async Task Add(Model2 model2)
    {
        await _model2Collection.TransactionalInsertOneAsync(_session, model2);
    }

    public async Task<Model2> Update(Model2 model2)
    {
        var result = await _model2Collection.TransactionalReplaceOneAsync(_session, x => x.Id == model2.Id, model2, new ReplaceOptions() { IsUpsert = false });

        if (result.IsAcknowledged && result.ModifiedCount > 0)
        {
            return model2;
        }
        else
        {
            return null;
        }
    }

    public async Task<bool> Delete(int id)
    {
        DeleteResult deleteResult = await _model2Collection.TransactionalDeleteOneAsync(_session, x => x.Id == id);
        return deleteResult.IsAcknowledged;
    }

    public void SetTransactionSession(IClientSessionHandle session)
    {
        _session = session;
    }
}
