﻿using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Mongo_Sample;

public static class OptionalMongoTransactionsExtensions
{
    public static IFindFluent<TDocument, TDocument> TransactionalFind<TDocument>(this IMongoCollection<TDocument> collection, IClientSessionHandle session, Expression<Func<TDocument, bool>> filter, FindOptions options = null)
    {
        if (session == null)
        {
            return collection.Find(filter, options);
        }
        else
        {
            return collection.Find(session, filter, options);
        }
    }

    public static Task TransactionalInsertOneAsync<TDocument>(this IMongoCollection<TDocument> collection, IClientSessionHandle session, TDocument document, InsertOneOptions options = null, CancellationToken cancellationToken = default)
    {
        if (session == null)
        {
            return collection.InsertOneAsync(document, options, cancellationToken);
        }
        else
        {
            return collection.InsertOneAsync(session, document, options, cancellationToken);
        }
    }

    public static Task<ReplaceOneResult> TransactionalReplaceOneAsync<TDocument>(this IMongoCollection<TDocument> collection, IClientSessionHandle session, Expression<Func<TDocument, bool>> filter, TDocument replacement, ReplaceOptions options = null, CancellationToken cancellationToken = default)
    {
        if (session == null)
        {
            return collection.ReplaceOneAsync(filter, replacement, options, cancellationToken);
        }
        else
        {
            return collection.ReplaceOneAsync(session, filter, replacement, options, cancellationToken);
        }
    }

    public static Task<DeleteResult> TransactionalDeleteOneAsync<TDocument>(this IMongoCollection<TDocument> collection, IClientSessionHandle session, Expression<Func<TDocument, bool>> filter, DeleteOptions options = null, CancellationToken cancellationToken = default)
    {
        if (session == null)
        {
            return collection.DeleteOneAsync(filter, options, cancellationToken);
        }
        else
        {
            return collection.DeleteOneAsync(session, filter, options, cancellationToken);
        }
    }
}
