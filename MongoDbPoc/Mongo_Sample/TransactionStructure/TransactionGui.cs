﻿using System;

namespace Mongo_Sample;

public class TransactionGui
{
    private readonly ConsoleIO _io;
    private readonly DbContext _mongoSetup;
    private readonly string _dbName;

    public TransactionGui(DbContext mongoSetup, string dbName)
    {
        _io = new ConsoleIO();
        _mongoSetup = mongoSetup;
        _dbName = dbName;
    }

    public void UseTransactionsRepo()
    {
        var uow = new UnitOfWork(_mongoSetup, _dbName);
        uow.StartTransaction();

        while (true)
        {
            Console.WriteLine();
            Console.WriteLine("============================================");
            Console.WriteLine("Available Commands:");
            Console.WriteLine("get1");
            Console.WriteLine("add1");
            Console.WriteLine("update1");
            Console.WriteLine("delete1");
            Console.WriteLine("get2");
            Console.WriteLine("add2");
            Console.WriteLine("update2");
            Console.WriteLine("delete2");
            Console.WriteLine("save");
            Console.WriteLine("============================================");
            Console.WriteLine();
            Console.WriteLine("Please type a command");
            var operation = Console.ReadLine();
            Console.WriteLine();

            switch (operation)
            {
                case "get1":
                    var get1Id = _io.RequestInputForInt("Id?");
                    var found1 = uow.Model1TransactionsRepo.Get(get1Id).GetAwaiter().GetResult();
                    if (found1 != null)
                    {
                        _io.PrintResult($"Get id:{found1.Id}, value:{found1.Value}, someOtherValue:{found1.SomeOtherValue}");
                    }
                    else
                    {
                        _io.PrintResult($"Get failed - id:{get1Id} not found");
                    }
                    break;

                case "add1":
                    var add1Model = _io.RequestInputForModel1();
                    uow.Model1TransactionsRepo.Add(add1Model).GetAwaiter().GetResult();
                    _io.PrintResult($"Add id:{add1Model.Id}, value:{add1Model.Value}, someOtherValue:{add1Model.SomeOtherValue}");
                    break;

                case "update1":
                    var update1Model = _io.RequestInputForModel1();
                    var returnUpdate1Model = uow.Model1TransactionsRepo.Update(update1Model).GetAwaiter().GetResult();
                    _io.PrintResult($"Update id:{returnUpdate1Model.Id}, value:{returnUpdate1Model.Value}, someOtherValue:{returnUpdate1Model.SomeOtherValue}");
                    break;

                case "delete1":
                    var delete1Id = _io.RequestInputForInt("Id?");
                    var is1Deleted = uow.Model1TransactionsRepo.Delete(delete1Id).GetAwaiter().GetResult();
                    _io.PrintResult($"Delete id:{delete1Id}, success:{is1Deleted}");
                    break;

                case "get2":
                    var get2Id = _io.RequestInputForInt("Id?");
                    var found2 = uow.Model1TransactionsRepo.Get(get2Id).GetAwaiter().GetResult();
                    if (found2 != null)
                    {
                        _io.PrintResult($"Get id:{found2.Id}, value:{found2.Value}, someOtherValue:{found2.SomeOtherValue}");
                    }
                    else
                    {
                        _io.PrintResult($"Get failed - id:{get2Id} not found");
                    }
                    break;

                case "add2":
                    var add2Model = _io.RequestInputForModel2();
                    uow.Model2TransactionsRepo.Add(add2Model).GetAwaiter().GetResult();
                    _io.PrintResult($"Add id:{add2Model.Id}, value:{add2Model.Value2}, someOtherValue:{add2Model.SomeOtherValue2}");
                    break;

                case "update2":
                    var update2Model = _io.RequestInputForModel2();
                    var returnUpdate2Model = uow.Model2TransactionsRepo.Update(update2Model).GetAwaiter().GetResult();
                    _io.PrintResult($"Update id:{returnUpdate2Model.Id}, value:{returnUpdate2Model.Value2}, someOtherValue:{returnUpdate2Model.SomeOtherValue2}");
                    break;

                case "delete2":
                    var delete2Id = _io.RequestInputForInt("Id?");
                    var is2Deleted = uow.Model2TransactionsRepo.Delete(delete2Id).GetAwaiter().GetResult();
                    _io.PrintResult($"Delete id:{delete2Id}, success:{is2Deleted}");
                    break;

                case "save":
                    uow.Complete();
                    uow.StartTransaction();
                    break;

                default:
                    _io.PrintResult("Please type a command from the menu");
                    break;
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
    }
}

//multi-document transactions are available for replica set deployments only
//you can use transactions even on a standalone server but you need to configure it as a replica set(with just one node)
//multi-document transactions are not available for sharded cluster
//hopefully transactions will be available from version 4.2
//multi-document transactions are available for the WiredTiger storage engine only

//a collection MUST exist in order to use transactions
//a collection cannot be created or dropped inside a transaction
//an index cannot be created or dropped inside a transaction
//non-CRUD operations are not permitted inside a transaction(for example, administrative commands like createUser are not permitted )
//a transaction cannot read or write in config, admin, and local databases
//a transaction cannot write to system.* collections
//the size of a transaction is limited to 16MB
//a single oplog entry is generated during the commit: the writes inside the transaction don’t have single oplog entries as in regular queries
//the limitation is a consequence of the 16MB maximum size of any BSON document in the oplog
//in case of larger transactions, you should consider splitting these into smaller transactions
//by default a transaction that executes for longer then 60 seconds will automatically expire
//you can change this using the configuration parameter transactionLifetimeLimitSeconds
//transactions rely on WiredTiger snapshot capability, and having a long running transaction can result in high pressure on WiredTiger’s cache to maintain snapshots, and lead to the retention of a lot of unflushed operations in memory
