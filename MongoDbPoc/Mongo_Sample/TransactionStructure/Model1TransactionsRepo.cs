﻿using MongoDB.Driver;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class Model1TransactionsRepo
{
    private readonly IMongoCollection<Model1> _model1Collection;
    private IClientSessionHandle _session;

    public Model1TransactionsRepo(IMongoCollection<Model1> model1Collection)
    {
        _model1Collection = model1Collection;
    }
    
    public async Task<Model1> Get(int id)
    {
        return await _model1Collection
        .TransactionalFind(_session, x => x.Id == id)
        .Limit(1)
        .SingleOrDefaultAsync();
    }
            
    public async Task Add(Model1 model)
    {
        await _model1Collection.TransactionalInsertOneAsync(_session, model);
    }

    public async Task<Model1> Update(Model1 model1)
    {
        var result = await _model1Collection.TransactionalReplaceOneAsync(_session, x => x.Id == model1.Id, model1, new ReplaceOptions() { IsUpsert = false });
       
        if (result.IsAcknowledged && result.ModifiedCount > 0)
        {
            return model1;
        }
        else
        {
            return null;
        }
    }
    
    public async Task<bool> Delete(int id)
    {
        DeleteResult deleteResult = await _model1Collection.TransactionalDeleteOneAsync(_session, x => x.Id == id);
        return deleteResult.IsAcknowledged;
    }

    public void SetTransactionSession(IClientSessionHandle session)
    {
        _session = session;
    }
}
