﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mongo_Sample;

public class SearchCriteria
{
    public bool FilterIsAnd { get; set; }
    public bool FilterIsExact { get; set; }
    public List<KeyValuePair<string, string>> Filters { get; set; }

    public string SortField { get; set; }
    public int SortDirection { get; set; }
    public int Skip { get; set; }
    public int Take { get; set; }

    public SearchCriteria(string sortField, int? sortDirection, int? skip, int? take, string filters, bool filterIsAnd = true, bool filterIsExact = true)
    {
        var typedFilters = ConvertFilters(filters);
        SetProperties(sortField, sortDirection, skip, take, typedFilters, filterIsAnd, filterIsExact);
    }
    
    private List<KeyValuePair<string, string>> ConvertFilters(string filtersString)
    {
        var filters = filtersString.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList();

        if (filters == null || filters.Count == 0)
        {
            return null;
        }

        List<KeyValuePair<string, string>> typedFilters = new List<KeyValuePair<string, string>>();

        foreach (var filter in filters)
        {
            string[] arrayKeyValue = filter.Split(':');
            string[] arrayValues = arrayKeyValue[1].Split(',');
            foreach (var arrayValue in arrayValues)
            {
                typedFilters.Add(new KeyValuePair<string, string>(arrayKeyValue[0], arrayValue));
            }
        }

        return typedFilters;
    }

    private void SetProperties(string sortField, int? sortDirection, int? skip, int? take, List<KeyValuePair<string, string>> filters = null, bool filterIsAnd = true, bool filterIsExact = true)
    {
        Filters = filters;
        FilterIsAnd = filterIsAnd;
        FilterIsExact = filterIsExact;

        if (filters != null)
        {
            foreach (var filter in filters)
            {
                var keyCount = filters.Where(x => x.Key == filter.Key).Count();
                if (keyCount > 1)
                {
                    FilterIsAnd = false;
                }
            }
        }
           
        SortField = sortField;
        int tempSortDirection = sortDirection ?? 1;
        SortDirection = Math.Abs(tempSortDirection) != 1 ? 1 : tempSortDirection;

        int tempSkip = skip ?? 0;
        Skip = tempSkip < 0 ? 0 : tempSkip;

        int tempTake = take ?? 0;
        Take = tempTake < 1 || tempTake > 100 ? 20 : tempTake; 
    }
}
