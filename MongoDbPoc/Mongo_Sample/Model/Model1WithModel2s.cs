﻿using System.Collections.Generic;

namespace Mongo_Sample;

public class Model1WithModel2s
{
    public int Id { get; set; }
    public string Value { get; set; }
    public string SomeOtherValue { get; set; }

    public List<int> Ids { get; set; }
    public IEnumerable<Model2> Model2s { get; set; }
}
