﻿namespace Mongo_Sample;

public class UnwoundModel
{
    public int Id { get; set; }
           
    public string Value { get; set; }
    public string SomeOtherValue { get; set; }

    public Model2 Model2s { get; set; }

    public override string ToString()
    {
        return $"Id:{Id}, Value:{Value}, SomeOtherValue:{SomeOtherValue}, Value2:{Model2s.Value2}, SomeOtherValue2:{Model2s.SomeOtherValue2}";
    }
}
