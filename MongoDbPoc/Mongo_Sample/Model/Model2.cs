﻿using System;
using System.Collections.Generic;

namespace Mongo_Sample;

public class Model2
{
    public int Id { get; set; }
    public string Value2 { get; set; }
    public string SomeOtherValue2 { get; set; }
    public Guid SomeGuid { get; set; }
    public List<NestedObject> NestedObjects { get; set; }

    public Model2(int a)
    {
        SomeGuid = Guid.NewGuid();

        NestedObjects = new List<NestedObject>()
        { 
            new NestedObject()
            { 
                Id = a,
                Value = a.ToString()
            },
            new NestedObject()
            {
                Id = 0,
                Value = "other"
            }
        };
    }

    public override string ToString()
    {
        return $"Id:{Id}, Value:{Value2}, SomeOtherValue:{SomeOtherValue2}";
    }


}
