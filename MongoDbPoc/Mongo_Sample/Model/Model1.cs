﻿using System;
using System.Collections.Generic;

namespace Mongo_Sample;

public class Model1
{
    public int Id { get; set; }
    public string Value { get; set; }
    public string SomeOtherValue { get; set; }

    public List<NestedObject> NestedObjects { get; set; }

    public List<int> Ids { get; set; }

    public DateTime DateTime { get; set; }
    public Guid SomeGuid { get; set; }

    public Model1()
    {
        SomeGuid = Guid.NewGuid();
    }
}

public class NestedObject
{
    public int Id { get; set; }
    public string Value { get; set; }
    public List<string> NestedList { get; set; }
}
