﻿using MongoDB.Driver.GeoJsonObjectModel;

namespace Mongo_Sample;

public class GeoSpaceModel
{
    public int Id { get; set; }
    public string Place { get; set; }
    public Address Address { get; set; }
    public GeoJsonPoint<GeoJson2DGeographicCoordinates> MongoLocation { get; set; }

    public GeoSpaceModel(int geoSpaceId, string place, double lat, double lng)
    {
        Id = geoSpaceId;

        Place = place;

        Address = new Address()
        {
            Street = "some address",
            Location = new Location()
        };
        Address.Location.SetLatitude(lat);
        Address.Location.SetLongitude(lng);

        MongoLocation = new GeoJsonPoint<GeoJson2DGeographicCoordinates>(new GeoJson2DGeographicCoordinates(lng, lat));
    }
}

public class Address
{
    public string Street { get; set; }
    public Location Location { get; set; }
}

public class Location
{
    private string _type;
    public string Type
    {
        get
        {
            if (string.IsNullOrEmpty(_type))
            {
                return "Point";
            }
            else
            {
                return _type;
            }
        }
        set
        {
            _type = value;
        }
    }

    public double[] Coordinates { get; set; }

    public Location()
    {
        ConfirmCoordinatesAreSet();
    }

    public double GetLatitude()
    {
        ConfirmCoordinatesAreSet();
        return Coordinates[1];
    }

    public void SetLatitude(double lat)
    {
        ConfirmCoordinatesAreSet();
        Coordinates[1] = lat;
    }

    public double GetLongitude()
    {
        ConfirmCoordinatesAreSet();
        return Coordinates[0];
    }

    public void SetLongitude(double lng)
    {
        ConfirmCoordinatesAreSet();
        Coordinates[0] = lng;
    }



    public void ConfirmCoordinatesAreSet()
    {
        if (Coordinates == null)
        {
            Coordinates = new double[2];
        }
    }
}
