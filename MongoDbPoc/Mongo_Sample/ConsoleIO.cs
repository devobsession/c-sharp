﻿using System;

namespace Mongo_Sample;

public class ConsoleIO
{
    public int RequestInputForInt(string prompt)
    {
        bool parsed = false;
        int id = 0;
        while (!parsed)
        {
            Console.WriteLine(prompt);
            var stringId = Console.ReadLine();
            parsed = int.TryParse(stringId, out id);
        }
        return id;
    }

    public GeoSpaceModel RequestInputForGeoModel()
    {
        var id = RequestInputForInt("Id?");

        Console.WriteLine("Lat?");
        var lat = Console.ReadLine();

        Console.WriteLine("Lng?");
        var lng = Console.ReadLine();

        Console.WriteLine("Place?");
        var place = Console.ReadLine();

        return new GeoSpaceModel(id,place, double.Parse(lat), double.Parse(lng));
    }

    public int RequestInputForBinary(string prompt)
    {
        bool isbin = false;
        int binNum = 0;
        while (!isbin)
        {
            Console.WriteLine(prompt);
            var stringId = Console.ReadLine();
            bool parsed = int.TryParse(stringId, out binNum);
            if (parsed && (binNum == 1 || binNum == 0))
            {
                isbin = true;
            }
        }
        return binNum;
    }

    public Model1 RequestInputForModel1()
    {
        var id = RequestInputForInt("Id?");

        Console.WriteLine("Value?");
        var value = Console.ReadLine();

        Console.WriteLine("SomeOtherValue?");
        var someOtherValue = Console.ReadLine();

        return new Model1()
        {
            Id = id,
            Value = string.IsNullOrEmpty(value) ? null : value,
            SomeOtherValue = string.IsNullOrEmpty(someOtherValue) ? null : someOtherValue,
        };
    }

    public string RequestInputForLabel(string label)
    {
        Console.WriteLine(label);
        return Console.ReadLine();
    }

    public Model2 RequestInputForModel2()
    {
        var id = RequestInputForInt("Id?");

        Console.WriteLine("Value?");
        var value = Console.ReadLine();

        Console.WriteLine("SomeOtherValue?");
        var someOtherValue = Console.ReadLine();

        return new Model2(id)
        {
            Id = id,
            Value2 = string.IsNullOrEmpty(value) ? null : value,
            SomeOtherValue2 = string.IsNullOrEmpty(someOtherValue) ? null : someOtherValue,
        };
    }

    public string RequestInputForObjectProperty<T>(string prompt)
    {
        while (true)
        {
            Console.WriteLine(prompt);
            var value = Console.ReadLine();

            var props = typeof(T).GetProperties();
            foreach (var prop in props)
            {
                if (value == prop.Name)
                {
                    return prop.Name;
                }
            }
        }
    }

    public void PrintResult(string result)
    {
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.WriteLine(result);
        Console.ForegroundColor = ConsoleColor.White;
    }
}
