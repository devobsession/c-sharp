﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class UnwindExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var collection = await Setup(database);

        var result = await collection
             .Aggregate()
             .Match(x => x.Id == 1)
             .Unwind<User, UserWithCar>(x => x.Cars)
             .ToListAsync();

        ExampleHelpers.Print(this, result);
    }

    private async Task<IMongoCollection<User>> Setup(IMongoDatabase database)
    {
        var collection = database.GetCollection<User>(this.GetType().Name);

        var testModels = new List<User>()
        {
            new User()
            {
                Id = 1,
                Name = "Bob",
                Cars = new List<string>()
                { 
                    "Lambo",
                    "BMW"
                }
            },
            new User()
            {
                Id = 2,
                Name = "Ben",
                Cars = new List<string>()
                {
                    "Beetle",
                    "Tesla"
                }
            }
        };

        var writeModels = new List<WriteModel<User>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<User>(Builders<User>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<string> Cars { get; set; }
    }

    public class UserWithCar
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Cars { get; set; }
    }
}
