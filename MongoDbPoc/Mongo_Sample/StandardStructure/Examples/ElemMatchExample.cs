﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;
using static Mongo_Sample.ElemMatchExample.MovieLibrary;

namespace Mongo_Sample;

public class ElemMatchExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var movieName = "Star Wars";
        var collection = await Setup(database, movieName);

        var filter = Builders<MovieLibrary>.Filter.ElemMatch(x => x.Movies, x => x.Name == movieName);

        var result = await collection
        .Find(filter)
        .ToListAsync();

        ExampleHelpers.Print(this, result);
    }

    private async Task<IMongoCollection<MovieLibrary>> Setup(IMongoDatabase database, string movieName)
    {
        var collection = database.GetCollection<MovieLibrary>(this.GetType().Name);

        var testModels = new List<MovieLibrary>()
        {
            new MovieLibrary()
            {
                Id = 1,
                Movies = new List<Movie>()
                {
                    new Movie()
                    {
                        Name = movieName
                    },
                    new Movie()
                    {
                        Name = "Avengers"
                    }
                }
            },
            new MovieLibrary()
            {
                Id = 2,
                Movies = new List<Movie>()
                {
                    new Movie()
                    {
                        Name = movieName
                    },
                    new Movie()
                    {
                        Name = "Goodfellas"
                    }
                }
            },
            new MovieLibrary()
            {
                Id = 3,
                Movies = new List<Movie>()
                {
                    new Movie()
                    {
                        Name = "A space odyssey"
                    },
                    new Movie()
                    {
                        Name = "The godfather"
                    }
                }
            }
        };

        var writeModels = new List<WriteModel<MovieLibrary>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<MovieLibrary>(Builders<MovieLibrary>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class MovieLibrary
    {
        public int Id { get; set; }
        public List<Movie> Movies { get; set; }

        public class Movie
        {
            public string Name { get; set; }
        }
    }
}
