﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class ProjectToNewModelExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var testModels = new List<User>()
        {
            new User()
            {
                Id = 1,
                Name = "Bob",
                Age = 20
            },
            new User()
            {
                Id = 2,
                Name = "Ben",
                Age = 22
            }
        };

        var collection = await Setup(database, testModels);

        var result1 = await collection.Find("{}")
            .Project(UserProjectorStrategy.GetIdentityExpression(ProjectType.Full))
            .ToListAsync();

        ExampleHelpers.Print(this, result1, testModels);
    }

    private async Task<IMongoCollection<User>> Setup(IMongoDatabase database, List<User> testModels)
    {
        var collection = database.GetCollection<User>(this.GetType().Name);

        var writeModels = new List<WriteModel<User>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<User>(Builders<User>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }

    public class OtherUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public static class UserProjectorStrategy
    {
        public static Expression<Func<User, OtherUser>> GetIdentityExpression(ProjectType projectType)
        {
            switch (projectType)
            {
                case ProjectType.Full:
                    return user => new OtherUser()
                    {
                        Id = user.Id,
                        Name = user.Name
                    };
                case ProjectType.Half:
                default:
                    return user => new OtherUser()
                    {
                        Id = user.Id
                    };
            }

            ;
        }
    }

    public enum ProjectType
    { 
        Full,
        Half
    }
}
