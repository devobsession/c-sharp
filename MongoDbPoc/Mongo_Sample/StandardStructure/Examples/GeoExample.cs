﻿using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class GeoExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var collection = await Setup(database);
        var longitude = 40.719296;
        var latitude = -73.99279;
        var maxDistanceInKm = 10;

        var point = GeoJson.Point(GeoJson.Geographic(longitude, latitude));
        var filter = Builders<Place>.Filter.Near(x => x.Address.Location, point, maxDistanceInKm);
        var result = await collection.Find(filter).ToListAsync();

        ExampleHelpers.Print(this, result);
    }

    private async Task<IMongoCollection<Place>> Setup(IMongoDatabase database)
    {
        var collection = database.GetCollection<Place>(this.GetType().Name);
        SetGeoSpaceIndex(collection);

        var testModels = new List<Place>()
        {
            new Place()
            {
                Id = 1,
                Name = "Central Park",
                Category = "Parks",
                Address = new Address()
                {
                    Area = "USA",
                    Location = new Location( -73.97, 40.77)
                }
            },
            new Place()
            {
                Id = 2,
                Name = "Sara D. Roosevelt Park",
                Category = "Parks",
                Address = new Address()
                {
                    Area = "USA",
                    Location = new Location( -73.9928, 40.7193)
                }
            },
            new Place()
            {
                Id = 3,
                Name = "Polo Grounds",
                Category = "Stadiums",
                Address = new Address()
                { 
                    Area = "UK",
                    Location = new Location( -73.9375, 40.8303)
                }                
            }
        };

        var writeModels = new List<WriteModel<Place>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<Place>(Builders<Place>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    private void SetGeoSpaceIndex(IMongoCollection<Place> collection)
    {
        var options = new CreateIndexOptions();
        var index = Builders<Place>.IndexKeys.Geo2DSphere(nameof(Address) + "." + nameof(Address.Location));
        var indexModel = new CreateIndexModel<Place>(index, options);

        try
        {
            collection.Indexes.CreateOne(indexModel);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public class Place
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public Address Address { get; set; }
    }

    public class Address
    {
        public string Area { get; set; }
        public Location Location { get; set; }
    }

    public class Location
    {
        private string _type;
        public string Type
        {
            get
            {
                if (string.IsNullOrEmpty(_type))
                {
                    return "Point";
                }
                else
                {
                    return _type;
                }
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _type = "Point";
                }
                else
                {
                    _type = value;
                }
            }
        }

        public double[] Coordinates { get; set; }

        public Location()
        {
        }

        public Location(double lat, double lng)
        {
            SetLatitude(lat);
            SetLongitude(lng);
            Type = "Point";
        }

        public double GetLatitude()
        {
            ConfirmCoordinatesAreSet();
            return Coordinates[1];
        }

        public void SetLatitude(double lat)
        {
            ConfirmCoordinatesAreSet();
            Coordinates[1] = lat;
        }

        public double GetLongitude()
        {
            ConfirmCoordinatesAreSet();
            return Coordinates[0];
        }

        public void SetLongitude(double lng)
        {
            ConfirmCoordinatesAreSet();
            Coordinates[0] = lng;
        }

        public void ConfirmCoordinatesAreSet()
        {
            if (Coordinates == null)
            {
                Coordinates = new double[2];
            }
        }
    }
}
