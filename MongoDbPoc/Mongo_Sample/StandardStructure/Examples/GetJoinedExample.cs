﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class GetJoinedExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var userCollection = await SetupUsers(database);
        var carsCollection = await SetupCars(database);

        var result = await userCollection.Aggregate()
            .Match(x => x.Id == 1)
            .Lookup<User, Car, FullUser>(
                carsCollection,
                a => a.CarIds,
                b => b.Id,
                a => a.Cars)
            .SingleOrDefaultAsync();

        ExampleHelpers.Print(this, result);
    }

    private async Task<IMongoCollection<User>> SetupUsers(IMongoDatabase database)
    {
        var collection = database.GetCollection<User>(this.GetType().Name + "_" + nameof(User));

        var testModels = new List<User>()
        {
            new User()
            {
                Id = 1,
                Name = "Bob",
                CarIds = new List<int>()
                { 
                    1,
                    2
                }
            },
            new User()
            {
                Id = 2,
                 Name = "Ben",
                CarIds = new List<int>()
                {
                    3
                }
            }
        };

        var writeModels = new List<WriteModel<User>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<User>(Builders<User>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    private async Task<IMongoCollection<Car>> SetupCars(IMongoDatabase database)
    {
        var collection = database.GetCollection<Car>(this.GetType().Name + "_" + nameof(Car));

        var testModels = new List<Car>()
        {
            new Car()
            {
                Id = 1,
                Name = "Lambo"
            },
            new Car()
            {
                Id = 2,
                 Name = "VW"
            },
            new Car()
            {
                Id = 3,
                 Name = "Merc"
            }
        };

        var writeModels = new List<WriteModel<Car>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<Car>(Builders<Car>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
        {
            IsOrdered = true,
        });

        return collection;
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<int> CarIds { get; set; }
    }

    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class FullUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Car> Cars { get; set; }
    }
}
