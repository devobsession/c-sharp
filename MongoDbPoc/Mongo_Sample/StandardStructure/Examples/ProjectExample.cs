﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class ProjectExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var collection = await Setup(database);

        var result1 = await collection.Find("{}")
            .Project(x => x.Name)
            .ToListAsync();

        ExampleHelpers.Print(this, result1);
    }

    private async Task<IMongoCollection<User>> Setup(IMongoDatabase database)
    {
        var collection = database.GetCollection<User>(this.GetType().Name);

        var testModels = new List<User>()
        {
            new User()
            {
                Id = 1,
                Name = "Bob"
            },
            new User()
            {
                Id = 2,
                Name = "Ben"
            }
        };

        var writeModels = new List<WriteModel<User>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<User>(Builders<User>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
