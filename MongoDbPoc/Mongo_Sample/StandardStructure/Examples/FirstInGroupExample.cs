﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class FirstInGroupExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var collection = await Setup(database);

        var result1 = await collection.Aggregate()
           .Sort(Builders<Car>.Sort.Descending(x => x.Year))
           .Group(e => e.Name, v => new
           {
               TopCar = v.Select(x => new
               {
                   x.Id,
                   x.Name,
                   x.Year
               }).First()
           })
         .ToListAsync();

        var cars = new List<Car>();
        foreach (var item in result1)
        {
            cars.Add(new Car()
            { 
                Id = item.TopCar.Id,
                Name = item.TopCar.Name,
                Year = item.TopCar.Year,
            });
        }

        ExampleHelpers.Print(this, cars);

        var result2 = collection.AsQueryable()
                               .OrderByDescending(e => e.Year)
                               .GroupBy(e => e.Name).Select(x => new Car
                               {
                                   Id = x.First().Id,
                                   Year = x.First().Year,
                                   Name = x.First().Name,
                               }).ToList();
 
        ExampleHelpers.Print(this, result2);       
    }

    private async Task<IMongoCollection<Car>> Setup(IMongoDatabase database)
    {
        var collection = database.GetCollection<Car>(this.GetType().Name);

        var testModels = new List<Car>()
        {
            new Car()
            {
                Id = 1,
                Name = "Lambo",
                Year = 2020
            },
            new Car()
            {
                Id = 2,
                Name = "Lambo",
                Year = 2003
            },
            new Car()
            {
                Id = 3,
                Name = "Tesla",
                Year = 2012
            },
            new Car()
            {
                Id = 4,
                Name = "Tesla",
                Year = 2016
            }
        };

        var writeModels = new List<WriteModel<Car>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<Car>(Builders<Car>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
    }
}
