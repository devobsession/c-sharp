﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class UnwindFilterExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var collection = await Setup(database);

        var result = await collection
             .Aggregate()
             .Match(x => x.Name == "Ben")
             .Unwind<User, UserWithCar>(x => x.Cars)
             .Match(x => x.Cars.Features.Any(y => y == "Fast"))
             .ToListAsync();

        ExampleHelpers.Print(this, result);
    }

    private async Task<IMongoCollection<User>> Setup(IMongoDatabase database)
    {
        var collection = database.GetCollection<User>(this.GetType().Name);

        var testModels = new List<User>()
        {
            new User()
            {
                Id = 1,
                Name = "Bob",
                Cars = new List<Car>()
                { 
                    new Car()
                    { 
                        Name = "Lambo",
                        Features = new List<string>()
                        { 
                            "Fast",
                            "Cool"
                        }
                    },
                     new Car()
                    {
                        Name = "BMW",
                        Features = new List<string>()
                        {
                            "Classy",
                            "Safe"
                        }
                    }
                }
            },
            new User()
            {
                Id = 2,
                Name = "Ben",
                Cars = new List<Car>()
                {
                    new Car()
                    {
                        Name = "Beetle",
                        Features = new List<string>()
                        {
                            "Cheap",
                            "Easy"
                        }
                    },
                     new Car()
                    {
                        Name = "Tesla",
                        Features = new List<string>()
                        {
                            "Eco",
                            "Safe",
                            "Fast",
                            "Cool"
                        }
                    }
                }
            }
        };

        var writeModels = new List<WriteModel<User>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<User>(Builders<User>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Car> Cars { get; set; }
    }

    public class Car
    {
        public string Name { get; set; }
        public List<string> Features { get; set; }
    }

    public class UserWithCar
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Car Cars { get; set; }
    }
}
