﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class UpsertCompoundIdExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var collection = await Setup(database);

        var newContact = new Contact()
        {
            Id =new Id() 
            {
                Name = "Bill",
                Number = 456
            },
            Country = "IE"
        };

        var filter = Builders<Contact>.Filter.Eq(doc => doc.Id, newContact.Id);

        var updateDefinition = Builders<Contact>.Update
            .SetOnInsert(doc => doc.Id, newContact.Id)
            .Set(doc => doc.Country, newContact.Country);

        var result = await collection.FindOneAndUpdateAsync(filter, updateDefinition, new FindOneAndUpdateOptions<Contact, Contact>()
        { 
            ReturnDocument = ReturnDocument.After,
            IsUpsert = true
        });

        ExampleHelpers.Print(this, result, newContact);
    }

    private async Task<IMongoCollection<Contact>> Setup(IMongoDatabase database)
    {
        var collection = database.GetCollection<Contact>(this.GetType().Name);
             
        return await Task.FromResult(collection);
    }

    public class Contact
    {
        public Id Id { get; set; }
        public string Country { get; set; }
    }

    public class Id
    {
        public string Name { get; set; }
        public int Number { get; set; }
    }
}
