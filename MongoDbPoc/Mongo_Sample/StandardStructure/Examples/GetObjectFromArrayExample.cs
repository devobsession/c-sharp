﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class GetObjectFromArrayExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var studentName = "Jane";
        var collection = await Setup(database, studentName);

        var result = await collection
            .Find(x => x.Id == 1)
            .Project(x => x.Students.SingleOrDefault(y => y.Name == studentName))
            .SingleOrDefaultAsync();

        ExampleHelpers.Print(this, result);
    }

    private async Task<IMongoCollection<Classroom>> Setup(IMongoDatabase database, string studentName)
    {
        var collection = database.GetCollection<Classroom>(this.GetType().Name);

        var testModels = new List<Classroom>()
        {
            new Classroom()
            {
                Id = 1,
                ClassroomName = "Class A",
                Students = new List<Classroom.Student>()
                { 
                    new Classroom.Student()
                    { 
                        Name = "Bob"
                    },
                     new Classroom.Student()
                    {
                        Name = studentName
                    }
                }
            }
        };

        var writeModels = new List<WriteModel<Classroom>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<Classroom>(Builders<Classroom>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class Classroom
    {
        public int Id { get; set; }
        public string ClassroomName { get; set; }
        public List<Student> Students { get; set; }
        public class Student
        {
            public string Name { get; set; }
        }
    }
}
