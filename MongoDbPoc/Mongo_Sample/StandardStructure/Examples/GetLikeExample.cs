﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class GetLikeExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var collection = await Setup(database);

        // options "i" makes it case insensitive
        var result = await collection
            .Find(Builders<Address>.Filter.Regex(x => x.Street, new BsonRegularExpression("dub", "i")))
            .ToListAsync(); 

        ExampleHelpers.Print(this, result);
    }

    private async Task<IMongoCollection<Address>> Setup(IMongoDatabase database)
    {
        var collection = database.GetCollection<Address>(this.GetType().Name);

        var testModels = new List<Address>()
        {
            new Address()
            {
                Id = 1,
                Street = "Main street, Sydney"
            },
            new Address()
            {
                Id = 2,
                Street = "Long street, Dublin"
            },
            new Address()
            {
                Id = 3,
                Street = "John street, Dublin"
            }
        };

        var writeModels = new List<WriteModel<Address>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<Address>(Builders<Address>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class Address
    {
        public int Id { get; set; }
        public string Street { get; set; }
    }
}
