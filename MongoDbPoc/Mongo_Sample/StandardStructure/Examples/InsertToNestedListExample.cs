﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class InsertToNestedListExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var movieName = "Star Wars";
        var collection = await Setup(database, movieName);

        var originalDoc = await collection.Find(x => x.Id == 1).SingleOrDefaultAsync();

        var update = Builders<MovieLibrary>.Update.Push(x => x.Movies[-1].Actors, "Hayden Christensen");
        var result = await collection.FindOneAndUpdateAsync<MovieLibrary, MovieLibrary>(x => x.Id == 1 && x.Movies.Any(y => y.Name == movieName), update, new FindOneAndUpdateOptions<MovieLibrary, MovieLibrary>()
        {
            ReturnDocument = ReturnDocument.After
        });

        ExampleHelpers.Print(this, result, originalDoc);
    }

    private async Task<IMongoCollection<MovieLibrary>> Setup(IMongoDatabase database, string movieName)
    {
        var testModels = new List<MovieLibrary>()
        {
            new MovieLibrary()
            {
                Id = 1,
                Movies = new List<Movie>()
                {
                    new Movie()
                    {
                        Name = movieName,
                        Actors = new List<string>()
                        { 
                            "Mark Hamill",
                            "Carrie Fisher"
                        }
                    },
                    new Movie()
                    {
                        Name = "Avengers",
                        Actors = new List<string>()
                        {
                            "Robert Downey Jr",
                             "Scarlett Johansson"
                        }
                    }
                }
            },
            new MovieLibrary()
            {
                Id = 2,
                Movies = new List<Movie>()
                {
                    new Movie()
                    {
                        Name = movieName,
                        Actors = new List<string>()
                        {
                             "Mark Hamill",
                             "Carrie Fisher"
                        }
                    },
                    new Movie()
                    {
                        Name = "Goodfellas",
                        Actors = new List<string>()
                        {
                            "Robert De Niro",
                            "Paul Sorvino"
                        }
                    }
                }
            },
            new MovieLibrary()
            {
                Id = 3,
                Movies = new List<Movie>()
                {
                    new Movie()
                    {
                        Name = "A space odyssey",
                        Actors = new List<string>()
                        {
                            "Douglas Rain",
                            "Gary Lockwood"
                        }
                    },
                    new Movie()
                    {
                        Name = "The godfather",
                        Actors = new List<string>()
                        {
                            "Al Pacino",
                            "Marlon Brando"
                        }
                    }
                }
            }
        };

        var writeModels = new List<WriteModel<MovieLibrary>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<MovieLibrary>(Builders<MovieLibrary>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        var collection = database.GetCollection<MovieLibrary>(this.GetType().Name);
        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class MovieLibrary
    {
        public int Id { get; set; }
        public List<Movie> Movies { get; set; }
    }

    public class Movie
    {
        public string Name { get; set; }
        public List<string> Actors { get; set; }
    }
}


