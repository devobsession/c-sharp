﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class PatchNestedExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var collection = await Setup(database);
        var id = 1;
        var originalDoc = await collection.Find(x => x.Id == id).SingleOrDefaultAsync();

        var contact = new Contact()
        {
            Name = "SuperMan",
            Number = 999999999
        };
        var update = GetPatchUpdateDefinitions<Address, Contact>(contact);
        if (update == null)
        {
            return;
        }

        var result = await collection.FindOneAndUpdateAsync<Address>(x => x.Id == id, update, new FindOneAndUpdateOptions<Address, Address>()
        {
            ReturnDocument = ReturnDocument.After
        });

        ExampleHelpers.Print(this, result, originalDoc);
    }

    public static UpdateDefinition<Parent> GetPatchUpdateDefinitions<Parent, Child>(Child item)
    {
        var updateList = new List<UpdateDefinition<Parent>>();

        var properties = item.GetType().GetProperties();
        if (properties == null)
        {
            return null;
        }

        foreach (var property in properties)
        {
            if (Attribute.IsDefined(property, typeof(SkipPatchAttribute)))
            {
                continue;
            }

            var value = property.GetValue(item);

            if (value != null)
            {
                updateList.Add(Builders<Parent>.Update.Set(typeof(Child).Name + "." + property.Name, value));
            }
        }

        if (updateList.Any() == false)
        {
            return null;
        }

        var update = Builders<Parent>.Update.Combine(updateList);
        return update;
    }

    private async Task<IMongoCollection<Address>> Setup(IMongoDatabase database)
    {
        var collection = database.GetCollection<Address>(this.GetType().Name);

        var testModels = new List<Address>()
        {
            new Address()
            {
                Id = 1,
                Street = "Main street",
                City = "Sydney",
                Contact = new Contact()
                { 
                    Name = "Joe",
                    Number = 876
                }
            },
            new Address()
            {
                Id = 2,
                Street = "Long street",
                City = "Dublin",
                Contact = new Contact()
                {
                    Name = "Sarah",
                    Number = 123
                }
            },
            new Address()
            {
                Id = 3,
                Street = "John street",
                City = "New York",
                Contact = new Contact()
                {
                    Name = "Bill",
                    Number = 456
                }
            }
        };

        var writeModels = new List<WriteModel<Address>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<Address>(Builders<Address>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class Address
    {
        [SkipPatch]
        public int Id { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public Contact Contact { get; set; }
    }
    public class Contact
    {
        public string Name { get; set; }
        public int? Number { get; set; }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class SkipPatchAttribute : Attribute
    {
    }
}
