﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class UpsertExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var oringinalContact = new Contact()
        {
            Id = 1,
            Name = "Bill",
            Number = 456
        };

        var collection = await Setup(database, oringinalContact);

        var newContact = new Contact()
        {
            Id = 2,
            Name = "Bill",
            Number = 456
        };

        var result = await collection.FindOneAndReplaceAsync<Contact>(x => x.Id == newContact.Id, newContact, 
            new FindOneAndReplaceOptions<Contact, Contact>()
                {
                    ReturnDocument = ReturnDocument.After,
                    IsUpsert = true
                });

        ExampleHelpers.Print(this, result, oringinalContact);
    }

    private async Task<IMongoCollection<Contact>> Setup(IMongoDatabase database, Contact contact)
    {
        var collection = database.GetCollection<Contact>(this.GetType().Name);
             
        await collection.InsertOneAsync(contact);

        return collection;
    }

    public class Contact
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
    }
}
