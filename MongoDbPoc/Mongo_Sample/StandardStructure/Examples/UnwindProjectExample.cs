﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class UnwindProjectExample : IExample
{
    public async Task Execute(IMongoDatabase database)
    {
        var collection = await Setup(database);

        // Db returns the list using the correct models
        // Less maintainable since the projection has to be updated with the models
        var result1 = await collection.Aggregate()
          .Unwind<User, UnwoundUser>(x => x.Emails)
          .Project(x => new {
              emailAddress = x.Emails.EmailAddress, // needs to be lower can properties to deserialize correctly
              verified = x.Emails.Verified
          })          
          .As<Email>()
          .ToListAsync();
        ExampleHelpers.Print(this, result1);

        var result2 = await collection.Aggregate()
            .Unwind<User, UnwoundUser>(x => x.Emails)
            .Project(x => new
            {
                Email = x.Emails
            })
            .ToListAsync();
        // The db returns the model we want nested in side another model so have to do a a little extra work in code
        // Big upside is that this option is maintainable since changes to model wont break this projection
        List<Email> emails = new List<Email>(result2.Count);
        foreach (var item in result2)
        {
            emails.Add(item.Email);
        }

        ExampleHelpers.Print(this, emails);
    }

    private async Task<IMongoCollection<User>> Setup(IMongoDatabase database)
    {
        var collection = database.GetCollection<User>(this.GetType().Name + "_" + nameof(User));

        var testModels = new List<User>()
        {
            new User()
            {
                Id = 1,
                Name = "Bob",
                Emails = new List<Email>()
                { 
                    new Email()
                    { 
                        EmailAddress = "bob@test.com",
                        Verified = false
                    },
                     new Email()
                    {
                        EmailAddress = "bob@gmail.com",
                        Verified = true
                    }
                }
            },
            new User()
            {
                Id = 2,
                Name = "Ben",
                Emails = new List<Email>()
                {
                    new Email()
                    {
                        EmailAddress = "ben@test.com",
                        Verified = true
                    },
                     new Email()
                    {
                        EmailAddress = "ben@gmail.com",
                        Verified = false
                    }
                }
            }
        };

        var writeModels = new List<WriteModel<User>>();
        foreach (var testModel in testModels)
        {
            writeModels.Add(new ReplaceOneModel<User>(Builders<User>.Filter.Eq(x => x.Id, value: testModel.Id), testModel) { IsUpsert = true });
        }

        await collection.BulkWriteAsync(writeModels, new BulkWriteOptions()
                {
                    IsOrdered = true,
                });

        return collection;
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Email> Emails { get; set; }
    }

    public class Email
    {
        public string EmailAddress { get; set; }
        public bool Verified { get; set; }
    }

    public class UnwoundUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Email Emails { get; set; }
    }

}
