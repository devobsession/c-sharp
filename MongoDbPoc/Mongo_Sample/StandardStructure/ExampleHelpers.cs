﻿using System;
using System.Text.Json;

namespace Mongo_Sample;

public class ExampleHelpers
{    public static void Print(IExample example, object result, object originalDocument = null)
    {
        var jsonResult = JsonSerializer.Serialize(result);
        Console.WriteLine("=========" + example.GetType().Name + "=========");
        if (originalDocument != null)
        {
            var jsonPrewrite = JsonSerializer.Serialize(originalDocument);
            Console.WriteLine("Original document: " + jsonPrewrite);
            Console.WriteLine();
        }

        Console.WriteLine("Result: " + jsonResult);

        Console.WriteLine("============================================");
    }
}
