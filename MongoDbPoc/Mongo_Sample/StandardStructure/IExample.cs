﻿using System.Threading.Tasks;
using MongoDB.Driver;

namespace Mongo_Sample;
public interface IExample
{
    public Task Execute(IMongoDatabase database);
}
