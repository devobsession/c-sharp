﻿using System;

namespace Mongo_Sample;

public class StandardGui
{
    private readonly ConsoleIO _io;
    private readonly DbContext _dbContext;

    public StandardGui(DbContext dbContext)
    {
        _io = new ConsoleIO();
        _dbContext = dbContext;
    }

    public void UseStandardRepo()
    {
        var repo = new StandardRepo(_dbContext);
        while (true)
        {
            var operation = Console.ReadLine();
            Console.WriteLine();
            operation = operation.ToLower();

            switch (operation)
            {     

                case "search":
                    var sortField = _io.RequestInputForObjectProperty<Model1>("Sort By Field?");
                    var sortDirection = _io.RequestInputForBinary("Sort Direction? (1/0)");
                    var skip = _io.RequestInputForInt("Skip?");
                    var take = _io.RequestInputForInt("Take?");

                    Console.WriteLine("Filters? (property:value1,value2 eg name:bob,sarah,joe)");
                    var filters = Console.ReadLine();

                    var isAnd = _io.RequestInputForBinary("IsAnd? (1/0)") == 1;
                    var isExact = _io.RequestInputForBinary("IsExact? (1/0)") == 1;

                    var searchCriteria = new SearchCriteria(sortField, sortDirection, skip, take, filters, isAnd, isExact);

                    var searchMinListItems = repo.SearchMinList(searchCriteria).GetAwaiter().GetResult();

                    foreach (var searchMinListItem in searchMinListItems)
                    {
                        _io.PrintResult($"Get someOtherValue:{searchMinListItem.SomeOtherValue}");
                    }
                    break;

                case "insert geo":
                    try
                    {
                        var addGeoModel = _io.RequestInputForGeoModel();

                        repo.GeoInsertModel(addGeoModel).GetAwaiter().GetResult();

                        _io.PrintResult($"Sucessfully inserted geo model");
                    }
                    catch (Exception ex)
                    {
                        _io.PrintResult($"Failed to insert geo model. " + ex.Message);
                    }
                    break;

                case "near geo":
                    try
                    {
                        Console.WriteLine("Lat?");
                        var lat = Console.ReadLine();

                        Console.WriteLine("Lng?");
                        var lng = Console.ReadLine();

                        Console.WriteLine("range?");
                        var range = Console.ReadLine();

                        var places = repo.GeoFindNear(double.Parse(lng), double.Parse(lat), double.Parse(range)).GetAwaiter().GetResult();

                        foreach (var place in places)
                        {
                            _io.PrintResult($"{place.Place}");
                        }
                    }
                    catch (Exception ex)
                    {
                        _io.PrintResult($"Failed to get geo model. " + ex.Message);
                    }
                    break;

                default:
                    _io.PrintResult("Please type a command from the menu");
                    break;
            }
                           
            Console.WriteLine();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
    }
}
