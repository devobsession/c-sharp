﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mongo_Sample;

public class StandardRepo
{
    private readonly ProjectionDefinition<Model1> _projection;
    private readonly DbContext _dbContext;

    public StandardRepo(DbContext dbContext)
    {
        _projection = Builders<Model1>.Projection
              .Include(user => user.SomeOtherValue);
        _dbContext = dbContext;
    }

    public async Task<List<Model1>> SearchMinList(SearchCriteria searchCriteria)
    {
        var filter = GenericMongoFilter.CreateFilter<Model1>(
            searchCriteria.FilterIsAnd,
            searchCriteria.FilterIsExact,
            searchCriteria.Filters);

        var results = await _dbContext.Model1s
        .Find(filter)
        .Sort(new BsonDocument(searchCriteria.SortField, searchCriteria.SortDirection))
        .Skip(searchCriteria.Skip)
        .Limit(searchCriteria.Take)
        .Project(_projection)
        .ToListAsync();

        List<Model1> minModel1s = new List<Model1>();
        foreach (var result in results)
        {
            try
            {
                minModel1s.Add(BsonSerializer.Deserialize<Model1>(result));
            }
            catch (Exception ex)
            {
            }
        }
        return minModel1s;
    }

    public async Task GeoInsertModel(GeoSpaceModel geoSpaceModel)
    {
        await _dbContext.GeoSpaceModels.InsertOneAsync(geoSpaceModel);
    }

    public async Task<List<GeoSpaceModel>> GeoFindNear(double longitude, double latitude, double maxDistanceInKm)
    {
        var point = GeoJson.Point(GeoJson.Geographic(longitude, latitude));
        var filter = Builders<GeoSpaceModel>.Filter.Near(x => x.Address.Location, point, maxDistanceInKm * 1000);
        return await _dbContext.GeoSpaceModels.Find(filter).Limit(10).ToListAsync();
    }
}
