﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace Mongo_Sample;

public static class GenericMongoFilter
{
    public static FilterDefinition<T> CreateFilter<T>(bool isAnd, bool isExact, List<KeyValuePair<string, string>> filters)
    {
        if ((filters == null || filters.Count == 0))
        {
            return "{}";
        }

        return filters.Filters<T>(isExact).Join(isAnd);            
    }
 
    public static List<FilterDefinition<T>> Filters<T>(this List<KeyValuePair<string, string>> filters, bool isExact)
    {
        List<FilterDefinition<T>> definitions = new List<FilterDefinition<T>>();

        if (filters != null || filters.Count > 0)
        {
            if (isExact)
            {
                foreach (var property in filters)
                {
                    definitions.Add(Builders<T>.Filter.Eq(property.Key, property.Value));
                }
            }
            else
            {
                foreach (var property in filters)
                {
                    definitions.Add(Builders<T>.Filter.Regex(property.Key, new BsonRegularExpression(property.Value, "i")));
                }
            }
        }

        return definitions;
    }

    public static FilterDefinition<T> Filter<T>(this KeyValuePair<string, string> filter, bool isExact = true)
    {
        if (isExact)
        {
            return Builders<T>.Filter.Eq(filter.Key, filter.Value);
        }
        else
        {
            return Builders<T>.Filter.Regex(filter.Key, new BsonRegularExpression(filter.Value, "i"));
        }
    }
    
    public static FilterDefinition<T> Join<T>(this List<FilterDefinition<T>> definitions, bool isAnd = true)
    {
        if (isAnd)
        {
            return Builders<T>.Filter.And(definitions);
        }
        else
        {
            return Builders<T>.Filter.Or(definitions);
        }
    }
    
    public static List<FilterDefinition<T>> InFilters<T>(this List<KeyValuePair<string, List<string>>> inFilters)
    {
        List<FilterDefinition<T>> definitions = new List<FilterDefinition<T>>();

        foreach (var inFilter in inFilters)
        {
            var inDef = Builders<T>.Filter.In(inFilter.Key, inFilter.Value);
            definitions.Add(inDef);
        }

        return definitions;
    }

    public static FilterDefinition<T> InFilter<T>(this KeyValuePair<string, List<string>> inFilter)
    {
        return Builders<T>.Filter.In(inFilter.Key, inFilter.Value);
    }
}
