﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace Mongo_Sample;

public class DbContext
{
    // Making MongoClient & IMongoDatabase static is fine as they are thread-safe.
    // MongoClient can be created over and over again as long it has the same connection string (under the covers it will use the same connection pool)

    private MongoClient _client;
    private IMongoDatabase _database;

    public IMongoClient Client
    {
        get { return _client; }
    }

    public IMongoCollection<Model1> Model1s
    {
        get { return _database.GetCollection<Model1>("Model1"); }
    }

    public IMongoCollection<Model2> Model2s
    {
        get { return _database.GetCollection<Model2>("Model2"); }
    }

    public IMongoCollection<GeoSpaceModel> GeoSpaceModels
    {
        get { return _database.GetCollection<GeoSpaceModel>("GeoSpaceModel"); }
    }
    
    public DbContext(string dbName, string connectionString)
    {
        ConventionPacks();
        ClassMaps();
        _client = new MongoClient(connectionString);

        _database = _client.GetDatabase(dbName);

        SetAscendingIndex();

        SetTtlIndex();

        SetGeoSpaceIndex();
    }

    private void SetAscendingIndex()
    {
        var index = Builders<Model2>.IndexKeys.Ascending(x => x.SomeOtherValue2);
        var indexModel = new CreateIndexModel<Model2>(index);

        try
        {
            Model2s.Indexes.CreateOne(indexModel);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    private void SetTtlIndex()
    {
        var options = new CreateIndexOptions()
        {
            ExpireAfter = new TimeSpan(0, 2, 0)
        };
    
        var index = Builders<Model1>.IndexKeys.Ascending(x => x.DateTime);
        var indexModel = new CreateIndexModel<Model1>(index, options);

        try
        {
            Model1s.Indexes.CreateOne(indexModel);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    private void SetGeoSpaceIndex()
    {
        var options = new CreateIndexOptions();
        var index = Builders<GeoSpaceModel>.IndexKeys.Geo2DSphere("Address.Location");
        var indexModel = new CreateIndexModel<GeoSpaceModel>(index, options);

        try
        {
            GeoSpaceModels.Indexes.CreateOne(indexModel);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    /// <summary>
    /// mappings need to be done before creation of the client
    /// http://mongodb.github.io/mongo-csharp-driver/2.9/reference/bson/mapping/
    /// </summary>
    private void ClassMaps()
    {
        //// http://mongodb.github.io/mongo-csharp-driver/2.11/reference/bson/guidserialization/guidrepresentationmode/guidrepresentationmode/
        BsonDefaults.GuidRepresentationMode = GuidRepresentationMode.V3; // must be used until V3 is made the default
        BsonSerializer.RegisterSerializer(new GuidSerializer(GuidRepresentation.Standard));

        BsonClassMap.RegisterClassMap<Model1>(cm =>
        {
            cm.AutoMap();
        });

        BsonClassMap.RegisterClassMap<Model2>(cm =>
        {
            cm.AutoMap();
        });

        BsonClassMap.RegisterClassMap<GeoSpaceModel>(cm =>
        {
            cm.AutoMap();
        });
        
    }

    private void ConventionPacks()
    {
        ConventionPack conventionPack = new ConventionPack
        {
            new IgnoreExtraElementsConvention(true),
            new EnumRepresentationConvention(BsonType.String)
        };

        ConventionRegistry.Register("DbConventions", conventionPack, t => true);
    }        
}
