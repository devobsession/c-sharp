﻿using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Infrastructure;

public class BaseGenericRepo
{
    private IMongoCollection<BsonDocument> _collection;

    public BaseGenericRepo(IMongoCollection<BsonDocument> collection)
    {
        _collection = collection;
    }

    public async Task<JsonNode> Get(string id)
    {
        var filter = Builders<BsonDocument>.Filter.Eq("id", id);
        var document = await _collection.Find(filter).SingleOrDefaultAsync();
        var json = document.ToJson();
        var jsonNode = JsonSerializer.Deserialize<JsonNode>(json, new JsonSerializerOptions()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        });

        return jsonNode;
    }

    public async Task<List<JsonNode>> Search(List<FilterDefinition<BsonDocument>> filters)
    {
        var document = await _collection.Find(Builders<BsonDocument>.Filter.And(filters)).ToListAsync();
        var json = document.ToJson();
        var jsonNode = JsonSerializer.Deserialize<List<JsonNode>>(json, new JsonSerializerOptions()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        });

        return jsonNode;
    }

    public async Task Upsert(string id, JsonNode entity)
    {
        var json = entity.ToJsonString();
        var document = BsonDocument.Parse(json);

        var filter = Builders<BsonDocument>.Filter.Eq("id", id);

        await _collection.ReplaceOneAsync(filter, document, new ReplaceOptions()
        {
            IsUpsert = true
        });
    }
}
