﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;

namespace Mongo_Sample;

class Program
{
    private static IMongoDatabase _database;

    static void Main(string[] args)
    {
        SetupDb();

        var examples = GetAllExamples();

        while (true)
        {
            int userSelection = IoMenu(examples);

            try
            {
                examples[userSelection].Execute(_database).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    private static int IoMenu(Dictionary<int, IExample> examples)
    {
        foreach (var exampleKvp in examples)
        {
            Console.WriteLine(exampleKvp.Key + " - " + exampleKvp.Value.GetType().Name);
        }

        var input = Console.ReadLine();
        int.TryParse(input, out int fetchIndex);
        return fetchIndex;
    }

    private static void SetupDb()
    {
        var standAloneConString = "mongodb://localhost:27018/admin";
        var replicaConString = "mongodb://mongodb-node1:40001,mongodb-node2:40002,mongodb-node3:40003/?replicaSet=mrs0";
        var conString3 = "mongodb://adminuser:mypassword@localhost:27017/gimsum?authSource=admin";

        ConventionPacks();
        ClassMaps();
        var client = new MongoClient(replicaConString);
        _database = client.GetDatabase("ComplexMongoExamples");
    }

    private static Dictionary<int, IExample> GetAllExamples()
    {
        var type = typeof(IExample);
        var exampleTypes = Assembly.GetAssembly(typeof(IExample))
            .GetTypes()
            .Where(x => type.IsAssignableFrom(x) && !x.IsInterface);

        var examples = new Dictionary<int, IExample>();
        var count = 1;
        foreach (var exampleType in exampleTypes)
        {
            var exampleObject = (IExample)Activator.CreateInstance(exampleType);
            examples.Add(count, exampleObject);
            count++;
        }

        return examples;
    }

    /// <summary>
    /// mappings need to be done before creation of the client
    /// http://mongodb.github.io/mongo-csharp-driver/2.9/reference/bson/mapping/
    /// </summary>
    private static void ClassMaps()
    {
        //// http://mongodb.github.io/mongo-csharp-driver/2.11/reference/bson/guidserialization/guidrepresentationmode/guidrepresentationmode/
        BsonDefaults.GuidRepresentationMode = GuidRepresentationMode.V3; // must be used until V3 is made the default
        BsonSerializer.RegisterSerializer(new GuidSerializer(GuidRepresentation.Standard));
    }

    private static void ConventionPacks()
    {
        ConventionPack conventionPack = new ConventionPack
            {
                new CamelCaseElementNameConvention(),
                new IgnoreExtraElementsConvention(true),
                new EnumRepresentationConvention(BsonType.String)
            };

        ConventionRegistry.Register("DbConventions", conventionPack, t => true);
    }
}
