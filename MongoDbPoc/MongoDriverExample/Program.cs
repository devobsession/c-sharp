﻿using System;

namespace MongoDriverExample;


class Program
{
    static void Main(string[] args)
    {
        DatabaseSettings databaseSettings = new DatabaseSettings()
        {
            DataBaseName = "testDb"
        };

        var conString3 = "mongodb://adminuser:mypassword@localhost:27018/admin";

        DbContext dbContext = new DbContext(databaseSettings, conString3);

        Meta meta = new Meta();
        meta.ListDataBaseNames(dbContext).GetAwaiter().GetResult();

        Console.ReadLine();
    }

}
