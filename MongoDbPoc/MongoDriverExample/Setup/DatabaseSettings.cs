﻿using System.Collections.Generic;

namespace MongoDriverExample;

public class DatabaseSettings
{
    public string DataBaseName { get; set; }
}

public class DbConnection
{
    public List<Server> Servers { get; set; }
    public string ReplicaSetName { get; set; }
    public string UserName { get; set; }
    public string Password { get; set; }
}

public class Server
{
    public string HostName { get; set; }
    public int Port { get; set; }
}
