﻿using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace MongoDriverExample;

public class Meta
{
    public async Task ListDataBaseNames(DbContext dbContext)
    {
        IAsyncCursor<MongoDB.Bson.BsonDocument> cursor = await dbContext.Client.ListDatabasesAsync();
        await cursor.ForEachAsync(db => Console.WriteLine(db["name"]));
    }
}
