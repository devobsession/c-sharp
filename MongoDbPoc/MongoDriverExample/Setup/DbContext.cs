﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace MongoDriverExample;

public class DbContext
{
    // Making MongoClient & IMongoDatabase static is fine as they are thread-safe.
    // MongoClient can be created over and over again as long it has the same connection string (under the covers it will use the same connection pool)

    private MongoClient _client;
    private IMongoDatabase _database;

    public IMongoClient Client
    {
        get { return _client; }
    }
    
    public IMongoCollection<PersonModel> PersonCollection
    {
        get { return _database.GetCollection<PersonModel>("Person"); }
    }

    public IMongoCollection<CompanyModel> CompanyCollection
    {
        get { return _database.GetCollection<CompanyModel>("Company"); }
    }

    public IMongoCollection<BsonDocument> BsonDocCollection
    {
        get { return _database.GetCollection<BsonDocument>("EgBsonCollection"); } // BsonDocument is a dynamic schema
    }

    public IMongoCollection<dynamic> DynamicCollection
    {
        get { return _database.GetCollection<dynamic>("EgDynamicCollection"); } // can also use dynamic instead of BsonDocument
    }
    
    public DbContext(DatabaseSettings databaseSettings, string connectionString)
    {
        ConventionPacks(); // must be first
        ClassMaps();           
        var mongoUrl = new MongoUrl(connectionString);
        _client = new MongoClient(mongoUrl);

        // Set MongoDb UUIDs to the new Binary subtypes 0x04 (default is 0x03)
        BsonSerializer.RegisterSerializer(new GuidSerializer(GuidRepresentation.Standard));

        // Case save Guids as strings but comes at a size cost 36 bytes vs 128 bits (but may provide better compatibility between systems)
        // BsonSerializer.RegisterSerializer(new GuidSerializer(BsonType.String));

        MongoDatabaseSettings mongoDatabaseSettings = new MongoDatabaseSettings()
        {
            ReadPreference = ReadPreference.PrimaryPreferred
        };

        _database = _client.GetDatabase(databaseSettings.DataBaseName, mongoDatabaseSettings);
    }

    public DbContext(DatabaseSettings databaseSettings, DbConnection dbConnection)
    {
        ConventionPacks();
        ClassMaps();
        SetMongoClientWithOutConnectionString(databaseSettings, dbConnection);
    }

    /// <summary>
    /// mappings need to be done before creation of the client
    /// http://mongodb.github.io/mongo-csharp-driver/2.9/reference/bson/mapping/
    /// </summary>
    private void ClassMaps()
    {
        BsonClassMap.RegisterClassMap<CompanyModel>(cm =>
        {
            cm.AutoMap();               
        });

        // an exception will be thrown if you try to register the same class map more than once
        // Call RegisterClassMap somewhere that will only execute once if that is not possible then can check it
        if (!BsonClassMap.IsClassMapRegistered(typeof(CompanyModel)))
        {
            BsonClassMap.RegisterClassMap<CompanyModel>(cm =>
            {
                cm.AutoMap();
            });
        }

        BsonClassMap.RegisterClassMap<PersonModel>(cm =>
        {
            cm.AutoMap();

            cm.SetIgnoreExtraElements(true); // If C# model does not have a property from mongo ignore it

            cm.MapIdMember(c => c.PersonId); // By convention, a public member called Id, id, or _id

            cm.UnmapMember(m => m.UselessProperty); // Do not save this property to the db

            cm.GetMemberMap(m => m.FirstName).SetElementName("name"); // Map a property to a different name in db

            cm.GetMemberMap(m => m.Age).SetDefaultValue(18); // Sets a default value during deserialization to assign a value to a field or property IF the document being deserialized doesn't have a value

            cm.GetMemberMap(c => c.Age).SetIgnoreIfDefault(true); // dont save if value is default eg 0

            cm.GetMemberMap(c => c.FavoriteFood).SetSerializer(new EnumSerializer<FavoriteFood>(BsonType.String));

            cm.MapProperty(c => c.ReadonlyProp); // readonly properties/fields will not be mapped automatically
            // readonly properties/fields will be saved to db but never read from db (This is useful for storing “computed” properties)

            cm.MapExtraElementsMember(c => c.ExtraElemets); // wiil get any extra elements that might be found in the db an populate it into the given property

            cm.MapCreator(x => new PersonModel(x.FirstName)); // Will use the following property from db to insert into constructor during deserialization
            cm.MapCreator(x => new PersonModel(x.FirstName, x.Age)); // will use the constructor that has the most matching parameters (if phoneNumber is in db it will use this constructor)
        });

    }

    /// <summary>
    /// Conventions need to be done before anything else (even mappings)
    /// </summary>
    private void ConventionPacks()
    {
        ConventionPack conventionPack = new ConventionPack
        {
            new CamelCaseElementNameConvention(), // sets properties to camelCase
            new IgnoreExtraElementsConvention(true) // ignores elements in the db which are not in the model
        };

        ConventionRegistry.Register("DbConventions", conventionPack, t => t.Name == "PersonModel"); // Apply pack to Models with this Name 
        ConventionRegistry.Register("DbConventions", conventionPack, t => t.Namespace == "MongoDriverExample"); // Apply pack to Models which come from this namespace
        ConventionRegistry.Register("DbConventions", conventionPack, t => true); // Apply pack to ALL models
    }
    
    private void SetMongoClientWithOutConnectionString(DatabaseSettings databaseSettings, DbConnection dbConnection)
    {
        var servers = new List<MongoServerAddress>();
        foreach (var server in dbConnection.Servers)
        {
            servers.Add(new MongoServerAddress(server.HostName, server.Port));
        }

        MongoClientSettings settings = new MongoClientSettings()
        {
            WriteConcern = new WriteConcern(1, journal: false),
            ReadPreference = new ReadPreference(ReadPreferenceMode.PrimaryPreferred),
            Servers = servers,
            SocketTimeout = new TimeSpan(0, 0, 0, 2),
            WaitQueueTimeout = new TimeSpan(0, 0, 0, 2),
            ConnectTimeout = new TimeSpan(0, 0, 0, 2)
        };

        if (!string.IsNullOrEmpty(dbConnection.ReplicaSetName))
        {
            settings.ReplicaSetName = dbConnection.ReplicaSetName;
        }

        if (!string.IsNullOrEmpty(dbConnection.Password))
        {
            string mongoDbAuthMechanism = "SCRAM-SHA-1";
            MongoInternalIdentity internalIdentity = new MongoInternalIdentity("admin", dbConnection.UserName);
            PasswordEvidence passwordEvidence = new PasswordEvidence(dbConnection.Password);
            MongoCredential mongoCredential = new MongoCredential(mongoDbAuthMechanism, internalIdentity, passwordEvidence);

            settings.Credential = mongoCredential;
        }

        _client = new MongoClient(settings);

        MongoDatabaseSettings mongoDatabaseSettings = new MongoDatabaseSettings()
        {
            GuidRepresentation = GuidRepresentation.CSharpLegacy,
            ReadPreference = ReadPreference.PrimaryPreferred
        };
        _database = _client.GetDatabase(databaseSettings.DataBaseName, mongoDatabaseSettings);
    }
}
