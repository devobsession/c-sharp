﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;

namespace MongoDriverExample;

public class Insert
{
    private readonly IMongoCollection<PersonModel> _personCollection;
    private readonly IMongoCollection<BsonDocument> _anyCollection;

    public Insert(IMongoCollection<PersonModel> personCollection, IMongoCollection<BsonDocument> anyCollection)
    {
        _personCollection = personCollection;
        _anyCollection = anyCollection;
    }

    public async Task InsertOne()
    {
        PersonModel personModel = new PersonModel("Jack", 25);

        await _personCollection.InsertOneAsync(personModel);
    }

    public async Task InsertMany()
    {
        PersonModel personModel1 = new PersonModel("Jack", 25);
        PersonModel personModel2 = new PersonModel("Jill", 21);

        await _personCollection.InsertManyAsync(new[] { personModel1, personModel2 });
    }
    public async Task InsertManyDuplicationError()
    {
        PersonModel personModel1 = new PersonModel("Jack", 25);
        PersonModel personModel2 = new PersonModel("Jill", 21);

        await _personCollection.InsertManyAsync(new[] { personModel1, personModel2 },
            new InsertManyOptions()
            { 
                IsOrdered = false // continue trying to insert other documents even after one or more failing due to any reason
            });
    }

    public async Task UntypedInsertOne()
    {
        string json = "{\"firstName\": \"Jack\", \"age\": 25}";
        var model = BsonDocument.Parse(json);
        await _anyCollection.InsertOneAsync(model);
    }

    public async Task InsertOneIdPopulate()
    {
        PersonModel personModel = new PersonModel("Jack", 25);

        await _personCollection.InsertOneAsync(personModel); // id is default and will be created by Db
        Console.WriteLine(personModel.PersonId); // id is populated into object automatically after await/result
    }

    public async Task InsertOneDuplicationError()
    {
        PersonModel personModel = new PersonModel("Jack", 25);

        await _personCollection.InsertOneAsync(personModel); 

        try
        {
            _personCollection.InsertOne(personModel);  // an attempt to insert the same object again will result in a duplicate key exception 
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }
    }
}
