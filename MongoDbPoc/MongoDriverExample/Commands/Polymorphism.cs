﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System.Collections.Generic;

namespace MongoDriverExample;

public class Polymorphism
{
    public void Run(IMongoCollection<Pets> petsCollection)
    {           
        Pets pets = new Pets()
        {
            Animals = new List<Animal>()
            {
                new Dog("Large", "Woof"),
                new Cat("Cool", "Meow")
            }
        };

        petsCollection.InsertOne(pets);

        List<Pets> listResult = petsCollection.Find("{}").ToList();
    }
}

public class Pets
{
    public ObjectId Id { get; set; }
    public List<Animal> Animals { get; set; }
}

[BsonDiscriminator(RootClass = true)]
[BsonKnownTypes(typeof(Dog), typeof(Cat))]
public abstract class Animal
{
    public string Sound { get; set; }

}
class Dog : Animal
{
    public string Size { get; set; }

    public Dog(string size, string sound)
    {
        Size = size;
        Sound = sound;
    }
}
class Cat : Animal
{
    public string Personality { get; set; }

    public Cat(string personality, string sound)
    {
        Personality = personality;
        Sound = sound;
    }
}
