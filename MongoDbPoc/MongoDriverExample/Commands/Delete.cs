﻿using MongoDB.Driver;
using System.Threading.Tasks;

namespace MongoDriverExample;

public class Delete
{
    FilterDefinition<PersonModel> _filter = Builders<PersonModel>.Filter.Lt(x => x.Age, 30);

    public async Task Deleting(IMongoCollection<PersonModel> personCollection)
    {
        DeleteResult deleteResult1 = await personCollection.DeleteOneAsync(_filter);

        DeleteResult deleteResult2 = await personCollection.DeleteManyAsync(_filter);

        PersonModel deletedModel = personCollection.FindOneAndDelete(_filter, // returns the entry which was deleted
            new FindOneAndDeleteOptions<PersonModel, PersonModel>
            {
                Sort = Builders<PersonModel>.Sort.Ascending(x => x.Age) // deletes the first one, so sort will control that order 
            });
    }
}
