﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MongoDriverExample;

public class Read
{

    public async Task ReadingLists(IMongoCollection<PersonModel> personCollection)
    {
        // FindAsync returns cursor which doesn't load all documents at once and provides you interface to retrieve documents one by one from DB cursor. It's helpful in case when query result is huge.
        var asyncCursor  = await personCollection.FindAsync(new BsonDocument());

        // Find provides you more simple syntax through method ToListAsync where it inside retrieves documents from cursor and returns all documents at once.
        var model = await personCollection.Find(new BsonDocument()).FirstAsync();

        // ReadToList
        List<PersonModel> modelList = await personCollection.Find(new BsonDocument()).ToListAsync(); // all entries will be in memory

        // ReadForEach
        await personCollection.Find(new BsonDocument()).ForEachAsync(doc => Console.WriteLine(doc));

        // ReadToCursor
        using (var cursor = await personCollection.Find(new BsonDocument()).ToCursorAsync())
        {
            while (await cursor.MoveNextAsync()) // cursor.MoveNextAsync  Moves the cursor to the Next Batch of records
            {
                foreach (var doc in cursor.Current) // cursor.Current is all the records in this batch
                {
                    Console.WriteLine(doc);
                }
            }
        }
    }

    public void TypedFilters(IMongoCollection<PersonModel> personCollection)
    {
        // single
        var eq = Builders<PersonModel>.Filter.Eq(x => x.FirstName, "Bob"); // Equal to
        var ne = Builders<PersonModel>.Filter.Ne(x => x.FirstName, "Bob"); // Not Equal to
        var lt = Builders<PersonModel>.Filter.Lt(x => x.Age, 30); // Less Than
        var lte = Builders<PersonModel>.Filter.Lte(x => x.Age, 30); // Less Than or Equal to
        var gt = Builders<PersonModel>.Filter.Gt(x => x.Age, 30); // Greater Than
        var gte = Builders<PersonModel>.Filter.Gte(x => x.Age, 30); // Greater Than or Equal to

        // Array
        var anyEq = Builders<PersonModel>.Filter.AnyEq(x => x.Savings, 10); // Any in list which are Equal to
        var anyNe = Builders<PersonModel>.Filter.AnyNe(x => x.Savings, 10);  // Any in list which are Not Equal to
        var anyGt = Builders<PersonModel>.Filter.AnyGt(x => x.Savings, 10); // Any in list which are Greater Than
        var anyGte = Builders<PersonModel>.Filter.AnyGte(x => x.Savings, 10); // Any in list which are Greater Than or Equal to
        var anyLt = Builders<PersonModel>.Filter.AnyLt(x => x.Savings, 10); // Any in list which are Less Than
        var anyLte = Builders<PersonModel>.Filter.AnyLte(x => x.Savings, 10); // Any in list which are Less Than or Equal to

        var size = Builders<PersonModel>.Filter.Size(x => x.Savings, 5); // Where the list has 5 elements
        var sizeGt = Builders<PersonModel>.Filter.SizeGt(x => x.Savings, 5); // Where the list has more than 5 elements
        var sizeGte = Builders<PersonModel>.Filter.SizeGte(x => x.Savings, 5); // Where the list has more than or equal to 5 elements
        var sizeLt = Builders<PersonModel>.Filter.SizeLt(x => x.Savings, 5); // Where the list has less than 5 elements
        var sizeLte = Builders<PersonModel>.Filter.SizeLte(x => x.Savings, 5); // Where the list has less than or equal to 5 elements

        var inFilter = Builders<PersonModel>.Filter.In(x => x.Age, new[] { 10, 20, 30 }); // Field exists in list
        var nin = Builders<PersonModel>.Filter.Nin(x => x.Age, new[] { 10, 20, 30 }); // Field doesn't exist in list

        var anyIn = Builders<PersonModel>.Filter.AnyIn(x => x.Savings, new[] { 10, 20, 30 }); // Any in list which are in given list
        var anyNin = Builders<PersonModel>.Filter.AnyNin(x => x.Savings, new[] { 10, 20, 30 }); // Any in list which are Not in given list

        var regex = Builders<PersonModel>.Filter.Regex(x => x.FirstName, new BsonRegularExpression("a", "i")); // regex a with options i(case insensitive)

        // Array Objects
        var elemMatch = Builders<PersonModel>.Filter.ElemMatch(x => x.FavoriteMusic, x => x.Artist == "JT" || x.Name == "Sexy Back"); // Match element in array

        // And Or
        var and = Builders<PersonModel>.Filter.And(eq, lt);
        var or = Builders<PersonModel>.Filter.Or(new List<FilterDefinition<PersonModel>>() { eq, lt, anyIn, gte } );
        
        // Use Filter
        var option1 = personCollection.Find(eq).FirstOrDefault();
        var option2 = personCollection.Find(model => model.Age < 35 && model.FirstName == "bob").FirstOrDefault(); 
        
    }

    public void UntypedFilters(IMongoCollection<PersonModel> personCollection, IMongoCollection<BsonDocument> genericCollection)
    {
        //===== Equal to string =====
        string stringFilter1 = "{name:\"bob\"}";
        BsonDocument bsonDocFilter1 = new BsonDocument("name", "bob");
        FilterDefinition<BsonDocument> builderFilter1 = Builders<BsonDocument>.Filter.Eq("name", "bob");

        // =====Less than int =====
        string stringFilter2 = "{Age:{$lt:30}}";
        BsonDocument bsonDocFilter2 = new BsonDocument("Age", new BsonDocument("$lt", 30));
        FilterDefinition<BsonDocument> builderFilter2 = Builders<BsonDocument>.Filter.Lt("Age", 35);

        //===== Equal to string AND Less than int =====
        string stringFilter3 = "{ $and : [{firstname: \"bob\"},{Age:{$lt:35} }]}";
        BsonDocument bsonDocFilter3 = new BsonDocument("$and", new BsonArray() {
            new BsonDocument("Age", new BsonDocument("$lt", 35)),
            new BsonDocument("firstname", "bob")
         });
        FilterDefinitionBuilder<BsonDocument> builder = Builders<BsonDocument>.Filter;
        FilterDefinition<BsonDocument> builderFilter3 = builder.And(builder.Lt("Age", 35), builder.Eq("Name", "bob"));
        FilterDefinition<BsonDocument> builderFilter3_2 = builder.Lt("Age", 35) & !builder.Eq("Name", "bob"); // overloaded & | ! operators

        //===== Use filter =====
        var result1 = personCollection.Find(bsonDocFilter3).FirstOrDefault();
        var result2 = personCollection.Find(stringFilter3).FirstOrDefault();
        var result3 = genericCollection.Find(builderFilter3).FirstOrDefault(); // cant use builder on typed collection like Person
    }

    public async Task Sort(IMongoCollection<PersonModel> personCollection)
    {
        List<PersonModel> sorts = await personCollection.Find(model => model.Age < 35)
                     .SortBy(x => x.Age)
                     .ThenBy(x => x.FirstName)
                     //.SortByDescending(x => x.Age)
                     //.ThenByDescending(x => x.FirstName)
                     //.Sort("{Age:1}")
                     //.Sort(new BsonDocument("Age", 1))
                     //.Sort(Builders<PersonModel>.Sort.Ascending(x => x.Age))                 
                     .ToListAsync();
    }

    public async Task Count(IMongoCollection<PersonModel> personCollection)
    {
        long resultCount = await personCollection.Find(model => model.Age < 35).CountDocumentsAsync();
    }

    public async Task SkipTake(IMongoCollection<PersonModel> personCollection)
    {
        List<PersonModel> limitedResult = await personCollection.Find(model => model.Age < 35)
            .SortByDescending(x => x.Age)
            .Skip(10)
            .Limit(10)
            .ToListAsync();
    }

    public async Task Projections(IMongoCollection<PersonModel> personCollection)
    {
        BsonDocument projectionMethods = await personCollection.Find(model => model.Age < 35)
             .Project(Builders<PersonModel>.Projection.Include(x => x.FirstName).Exclude(x => x.PersonId))
           //.Project("{Name:1, _id:0}")
           //.Project(new BsonDocument("Name", 0).Add("_id", 0))
           //.Project(Builders<PersonModel>.Projection.Include("Name").Exclude("_id"))
           .FirstOrDefaultAsync();

        BsonDocument resultProjection1 = await personCollection.Find(model => model.Age < 35)
           .Project(Builders<PersonModel>.Projection.Include(x => x.FirstName).Exclude(x => x.PersonId)) // Project returns BsonDocuments as it is no longer contains all the field of the class
           .FirstOrDefaultAsync();

        PersonModel resultProjection2 = await personCollection.Find(model => model.Age < 35)
            .Project<PersonModel>(Builders<PersonModel>.Projection.Include(x => x.FirstName).Exclude(x => x.PersonId)) // can specify the type of the project to return that type (all fields not included will be default)
            .FirstOrDefaultAsync();

        string resultProjection3 = await personCollection.Find(model => model.Age < 35)
           .Project(x => x.FirstName) // get one of the values directly
           .FirstOrDefaultAsync();

        var resultProjection4 = await personCollection.Find(model => model.Age < 35)
           .Project(x => new { x.FirstName, NewAge = x.Age + 20 }) // return an anonymous type
           .FirstOrDefaultAsync();
    }

    public async Task Join(IMongoCollection<PersonModel> personCollection, IMongoCollection<CompanyModel> companyCollection, int id)
    {
        CompanyModel join1 = await companyCollection.Aggregate().
        Lookup<CompanyModel, PersonModel, CompanyModel>(
            personCollection,
            companyModel => companyModel.EmployeeIds,
            person => person.PersonId,
            companyModel => companyModel.Employees)
        .Match(x => x.Id == id)
        .SingleOrDefaultAsync();

        CompanyPersonModel join2 = await companyCollection.Aggregate().
            Lookup<CompanyModel, PersonModel, CompanyPersonModel>(
                personCollection,
                companyModel => companyModel.EmployeeIds,
                personModel => personModel.PersonId,
                companyPersonModel => companyPersonModel.Employees)
            .Match(x => x.Id == id)
            .SingleOrDefaultAsync();
    }

}
