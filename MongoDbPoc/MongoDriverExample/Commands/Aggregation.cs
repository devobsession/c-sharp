﻿using MongoDB.Driver;
using System.Threading.Tasks;
using System.Linq;

namespace MongoDriverExample;

public class Aggregation
{
    private async Task Match(IMongoCollection<PersonModel> personCollection)
    {
        var matching = await personCollection.Aggregate()
         .Match(x => x.Age > 18)
         //.Match(new BsonDocument("Age", new BsonDocument("$gte",18)))
         //.Match("{Age: {$gte: 18}}")
         .ToListAsync();
    }

    private async Task Group(IMongoCollection<PersonModel> personCollection)
    {
        var grouping = await personCollection.Aggregate()
          .Match(x => x.Age > 18)
          .Group(doc => doc.FirstName, group => new { FirstName = group.Key, egTotal = group.Sum(x => x.Age) })
          //.Group(new BsonDocument("_id", "$firstname").Add("egTotal", new BsonDocument("$sum", "$Age")))
          //.Group("{_id: '$firstname', egTotal: {$sum: '$Age'}}")   
          .ToListAsync();
    }

    private async Task Lookup(IMongoCollection<PersonModel> personCollection, IMongoCollection<CompanyModel> companyCollection, int id)
    {
        CompanyPersonModel join2 = await companyCollection.Aggregate().
         Lookup<CompanyModel, PersonModel, CompanyPersonModel>(
             personCollection,
             companyModel => companyModel.EmployeeIds,
             personModel => personModel.PersonId,
             companyPersonModel => companyPersonModel.Employees)
         .Match(x => x.Id == id)
         .SingleOrDefaultAsync();
    }
}
