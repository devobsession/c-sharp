﻿using System.Collections.Generic;
using MongoDB.Driver;
using System.Threading.Tasks;

namespace MongoDriverExample;

public class Update
{
    FilterDefinition<PersonModel> _filter = Builders<PersonModel>.Filter.Lt(x => x.Age, 30);
    PersonModel _model = new PersonModel("Bob", 20);
    UpdateDefinition<PersonModel> _updateDefinition = Builders<PersonModel>.Update.Set(x => x.FirstName, "barry");

    /// <summary>
    /// Update the entire model
    /// </summary>
    /// <returns></returns>
    public async Task Replacing(IMongoCollection<PersonModel> personCollection)
    {
        var replaceOneResult = await personCollection.ReplaceOneAsync(_filter, _model, new ReplaceOptions()
        { 
            IsUpsert = true // upsert will insert if the filter returns nothing
        }); 

        PersonModel personModel = await personCollection.FindOneAndReplaceAsync(_filter, _model,
          new FindOneAndReplaceOptions<PersonModel, PersonModel>
          {
              IsUpsert = true,
              ReturnDocument = ReturnDocument.After, // return modified doc (default returns doc before modification)
              Sort = Builders<PersonModel>.Sort.Ascending(x => x.Age) // updates the first one, so sort will control that order 
          });
    }

    /// <summary>
    /// Patch specific properties of the model
    /// </summary>
    /// <returns></returns>
    public async Task Updating(IMongoCollection<PersonModel> personCollection)
    {
        UpdateResult updateResult1 = await personCollection.UpdateOneAsync(_filter, _updateDefinition, new UpdateOptions() { IsUpsert = true });

        UpdateResult updateResult2 = await personCollection.UpdateManyAsync(_filter, _updateDefinition, new UpdateOptions() { IsUpsert = true });

        PersonModel personModel = await personCollection.FindOneAndUpdateAsync(_filter, _updateDefinition,
           new FindOneAndUpdateOptions<PersonModel, PersonModel>
           {
               IsUpsert = true,
               ReturnDocument = ReturnDocument.After, // return modified doc (default returns doc before modification)
               Sort = Builders<PersonModel>.Sort.Ascending(x => x.Age) // updates the first one, so sort will control that order 
           });
    }

    // Update Definitions
 
    UpdateDefinition<PersonModel> set = Builders<PersonModel>.Update.Set(x => x.FirstName, "barry");
    UpdateDefinition<PersonModel> setNew = Builders<PersonModel>.Update.Set("newElement", "added"); // will insert the new element if it does not exist (have to ignore extra elements, otherwise an exception is thrown)

    UpdateDefinition<PersonModel> addToSet = Builders<PersonModel>.Update.AddToSet(x => x.MoviesWatched, "Star Wars"); // do not add the item if it already exists
    UpdateDefinition<PersonModel> push = Builders<PersonModel>.Update.Push(x => x.MoviesWatched, "Avengers"); // add the given item whether it exists or not.
    UpdateDefinition<PersonModel> pushEach = Builders<PersonModel>.Update.PushEach(x => x.MoviesWatched, new List<string>() { "Avengers", "Star Wars" }); // add the given items whether they exists or not.
    UpdateDefinition<PersonModel> pull = Builders<PersonModel>.Update.Pull(x => x.MoviesWatched, "Avengers"); // removes all instances of an item eg will remove all instance of "Avengers"
    UpdateDefinition<PersonModel> pullFilter = Builders<PersonModel>.Update.PullFilter(x => x.MoviesWatched, x => x == "Avengers"); // removes all instances of an item matching the filter eg will remove all instance of "Avengers"
    UpdateDefinition<PersonModel> pullAll = Builders<PersonModel>.Update.PullAll(x => x.MoviesWatched, new List<string>() { "Avengers", "Star Wars" }); // removes all instances of an item eg will remove all instance of "Avengers" and "Star Wars" 
    UpdateDefinition<PersonModel> popFirst = Builders<PersonModel>.Update.PopFirst(x => x.MoviesWatched); // remove first element of array
    UpdateDefinition<PersonModel> popLast = Builders<PersonModel>.Update.PopLast(x => x.MoviesWatched); // remove last element of array

    UpdateDefinition<PersonModel> inc = Builders<PersonModel>.Update.Inc(x => x.Age, 5); // increment by a given value
    UpdateDefinition<PersonModel> max = Builders<PersonModel>.Update.Max(x => x.Age, 18); // Updates record to use the highest of the current value and given value eg use higher of Age or 18
    UpdateDefinition<PersonModel> min = Builders<PersonModel>.Update.Min(x => x.Age, 18); // Updates record to use the lowest of the current value and given value eg use lower of Age or 18

    public void MultipleFieldUpdate()
    {
        var updateDefinition = new List<UpdateDefinition<PersonModel>>
        {
            Builders<PersonModel>.Update.Set(x => x.FirstName, "Bob"),
            Builders<PersonModel>.Update.Set(x => x.Age, 40)
        };

        _updateDefinition = Builders<PersonModel>.Update.Combine(updateDefinition);
    }
}
