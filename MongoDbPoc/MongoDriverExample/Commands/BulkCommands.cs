﻿using MongoDB.Driver;
using System.Threading.Tasks;

namespace MongoDriverExample;

public class BulkCommands
{
    FilterDefinition<PersonModel> _filter = Builders<PersonModel>.Filter.Lt(x => x.Age, 30);
    PersonModel _model = new PersonModel("Bob");

    public async Task BulkWrite(IMongoCollection<PersonModel> personCollection)
    {
        var result = await personCollection.BulkWriteAsync(new WriteModel<PersonModel>[]
        {
            new DeleteOneModel<PersonModel>(_filter),
            new DeleteManyModel<PersonModel>(_filter),
            new InsertOneModel<PersonModel>(_model),
            new UpdateOneModel<PersonModel>(_filter, Builders<PersonModel>.Update.Set(x=>x.FirstName,"barry")),
            new UpdateManyModel<PersonModel>(_filter, Builders<PersonModel>.Update.Inc(x=>x.Age,1)),
            new ReplaceOneModel<PersonModel>(_filter, _model)
        }, new BulkWriteOptions()
        {
            IsOrdered = true
        });
    }
}
