﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;

namespace MongoDriverExample;

public class Documents
{

    public void Deserialize(BsonDocument doc)
    {
        BsonSerializer.Deserialize<PersonModel>(doc);

        // or specific property
        var person = new PersonModel("Bob");
        person.FirstName = doc["name"].AsString;
    }

    // BsonDocument can be used to manage documents
    // it works similar to C# Dictionaries

    public void CreateUpdate()
    {
        // Create
        var doc = new BsonDocument()
        {
            {"name","bob" },
            {"age",20 }
        };

        // Add
        doc.Add("surname", "jones");
        doc.Add(new BsonElement("number", "032165478"));
        
        // Add Array
        BsonArray myNumbersArray = new BsonArray() { 1, 2 };
        myNumbersArray.Add(3);
        doc.Add("myNumbersArray", myNumbersArray);

        // Add nested doc
        BsonArray myNestedDocArray = new BsonArray()
        {
            new BsonDocument("color", "red")
        };
        doc.Add("myNestedDocArray", myNestedDocArray);

        // Update/Add
        doc["profession"] = "builder";

        // Remove elements
        doc.Remove("surname");
        doc.RemoveElement(new BsonElement("name", "greg"));
        doc.RemoveAt(0);
    }
    
    public void Read(BsonDocument doc)
    {
        // Get Elements
        BsonElement ele1 = doc.GetElement(0);
        BsonElement ele2 = doc.GetElement("name");
        doc.TryGetElement("name", out ele1);
              
        // Get Values            
        BsonValue val1 = doc.GetValue(0);
        BsonValue val2 = doc.GetValue("name");
        BsonValue val3 = doc.GetValue("name", "bob");
        doc.TryGetValue("bob", out val3);

        // get array
        BsonValue arrayVal = doc["myNumbersArray"][0];

        // get nested
        BsonValue nestedVal = doc["myNestedDocArray"][0]["color"];

        // Query
        bool fieldExists = doc.Contains("name");
        bool valueExists = doc.ContainsValue("bob");
    }          
}
