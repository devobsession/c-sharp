﻿using System;
using System.Collections.Generic;

namespace MongoDriverExample;

public class PersonModel
{
    public Guid PersonId { get; set; }

    public string FirstName { get; set; }

    public int NumberAsString { get; set; }

    public int Age { get; set; }
    
    public FavoriteFood FavoriteFood { get; set; }

    public List<string> MoviesWatched { get; set; }
    public List<int> Savings { get; set; }
    public List<Music> FavoriteMusic { get; set; }

    public dynamic ExtraElemets { get; set; }

    public int UselessProperty { get; set; }

    private readonly string _readonly;
    public string ReadonlyProp
    {
        get { return _readonly; }
    }

    public PersonModel(string firstName)
    {
        FirstName = firstName;
    }

    public PersonModel(string firstName, int age)
    {
        FirstName = firstName;
        Age = age;
    }
}

public class Music
{
    public string Name { get; set; }
    public string Artist { get; set; }
    public List<string> NestedList { get; set; }
}

public enum FavoriteFood
{
    Pizza,
    Burger
}
