﻿using System;
using System.Collections.Generic;

namespace MongoDriverExample;

public class CompanyPersonModel
{
    public int Id { get; set; }
    public string Name { get; set; }

    public List<Guid> EmployeeIds { get; set; }
    public List<PersonModel> Employees { get; set; }
}
