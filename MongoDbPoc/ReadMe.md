

# Setting up mongo locally
The docker compose file will start mongo up as 3 node replica set. docker compose require docker to be installed on the local machine.

### 1. Install Docker
https://www.docker.com/

### 2. edit /etc/hosts file
127.0.0.1       mongodb-node1
127.0.0.1       mongodb-node2
127.0.0.1       mongodb-node3

### 3. run the docker-compose file with
docker-compose up -d

### 4. access the replica set with:
mongodb://mongodb-node1:40001,mongodb-node2:40002,mongodb-node3:40003/?replicaSet=mrs0

# Troubleshooting 

### Transactions do not work with a stand alone mongo server
Follow the instructions in this readme to setup a local replica set

### Replica Set keeps crashing
If you changed the image version, then delete the data on you local machine because the different version data may be clashing. the data for the docker-compose is found in a folder called data next to the compose file.

### Cant connect to my local replica set
1. Check that you have set up your /etc/hosts file correctly
2. make sure that you are using the correct connection string. Cant use localhost must use "mongo1" "mongo2" "mongo3" and must use the exact replica set Name
3. Check if the replica set running, since it may have crashed a few minutes after start up.
4. Check container logs, if it is looping trying to connect to the replica set, then try shutting down the containers and running them again.

# References
https://docs.mongodb.com/manual/reference/connection-string/
https://github.com/UpSync-Dev/docker-compose-mongo-replica-set
https://github.com/mongodb/mongo-csharp-driver/blob/master/tests/MongoDB.Driver.Tests
https://mongodb.github.io/mongo-csharp-driver/2.9/reference/
