﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JwtPoc
{
    public class AuthUtilities
    {
        private readonly SecurityConfig _securityConfig;
        private readonly TokenValidationParameters _tokenValidationParameters;

        public AuthUtilities(SecurityConfig securityConfig)
        {
            _securityConfig = securityConfig;

            _tokenValidationParameters = new TokenValidationParameters
            {
                ValidIssuer = securityConfig.JwtValidIssuer,
                ValidAudiences = new[] { securityConfig.JwtValidAudience },
                IssuerSigningKeys = new[] { new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityConfig.JwtKey)) }
            };
        }

        public string GenerateJwt(List<Claim> claims, DateTime expiryDateTime)
        {
            var jwtSecurityToken = new JwtSecurityToken(
                  issuer: _securityConfig.JwtValidIssuer,
                  audience: _securityConfig.JwtValidAudience,
                  claims: claims,
                  expires: expiryDateTime,
                  signingCredentials: new SigningCredentials(
                      key: new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_securityConfig.JwtKey)),
                      algorithm: SecurityAlgorithms.HmacSha256)
              );

            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }

        public ClaimsPrincipal ValidateToken(string jwt)
        {
            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            try
            {
                return handler.ValidateToken(jwt, _tokenValidationParameters, out SecurityToken validatedToken);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
