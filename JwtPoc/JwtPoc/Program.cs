﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace JwtPoc
{
    class Program
    {
        static void Main(string[] args)
        {

            var authUtilities = new AuthUtilities(new SecurityConfig("1234567890123456", "issuer", "audience"));

            var claims = new List<Claim>()
            {
                new Claim("my-custom-claim", "123abc")
            };

            var jwt = authUtilities.GenerateJwt(claims, DateTime.Now.AddMinutes(1));
            Console.WriteLine(jwt);

            var principle = authUtilities.ValidateToken(jwt);
            foreach (var claim in principle.Claims)
            {
                Console.WriteLine(claim.Type + " - " + claim.Value);
            }
        }
    }
}
