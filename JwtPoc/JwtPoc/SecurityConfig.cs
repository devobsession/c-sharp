﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JwtPoc
{
    public class SecurityConfig
    {
        public string JwtKey { get; }
        public string JwtValidIssuer { get; }
        public string JwtValidAudience { get; }

        public SecurityConfig(string jwtKey, string jwtValidIssuer, string jwtValidAudience)
        {
            JwtKey = jwtKey ?? Environment.GetEnvironmentVariable("JWT_SECRET_KEY");
            if (JwtKey is null)
            {
                Console.WriteLine("=================== WARNING: POTENTIAL SECURITY RISK ======================");
                Console.WriteLine("Default JWT_SECRET_KEY should not be used in Live");

                throw new Exception("JWT_SECRET_KEY not found");
            }

            if (JwtKey.Length < 16)
            {
                Console.WriteLine("JWT_SECRET_KEY must be >= 16 characters");
                throw new Exception("JWT_SECRET_KEY must be >= 16 characters");
            }

            JwtValidIssuer = jwtValidIssuer ?? Environment.GetEnvironmentVariable("JWT_VALID_ISSUER");
            if (JwtValidIssuer is null)
            {
                throw new Exception("JWT_VALID_ISSUER not set and jwt tokens will not work");
            }

            JwtValidAudience = jwtValidAudience ?? Environment.GetEnvironmentVariable("JWT_VALID_AUDIENCE");
            if (JwtValidAudience is null)
            {
                throw new Exception("JWT_VALID_AUDIENCE not set and jwt tokens will not work");
            }
        }
    }
}
