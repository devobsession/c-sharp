﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProductServices
{
    public class ProductService
    {
        private readonly List<Product> _productRepo;

        public ProductService(List<Product> productRepo)
        {
            _productRepo = productRepo;
        }

        public void Add(Product product)
        {

            if (product == null)
            {
                throw new Exception("Null product");
            }

            if (product.Id <= 0 || product.Price <= 0 || string.IsNullOrEmpty(product.Name))
            {
                throw new Exception("Missing data");
            }

            if (_productRepo.Any(x => x.Id == product.Id))
            {
                throw new Exception("Product already exists");
            }

            _productRepo.Add(product);
        }

        public Product UntestedGet(int id)
        {
            if (id <= 0)
            {
                throw new Exception("Invalid Id");
            }

            var product = _productRepo.SingleOrDefault(x => x.Id == id);

            if (product == null)
            {
                throw new Exception("Not Found");
            }

            return product;
        }
    }
}
