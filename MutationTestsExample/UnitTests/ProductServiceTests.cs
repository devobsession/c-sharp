using ProductServices;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace UnitTests
{
    public class ProductServiceTests
    {

        [Fact]
        public void WhenProduct_IsValid_Insert()
        {
            // Arrange
            var repo = new List<Product>();
            var productService = new ProductService(repo);
            var product = new Product()
            {
                Id = 1,
                Name = "a",
                Price = 1
            };

            // Act
            productService.Add(product);

            // Assert
            Assert.NotNull(repo.SingleOrDefault(x => x.Id == product.Id));
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void WhenProduct_HasNegativeOr0Id_ThrowException(int id)
        {
            // Arrange
            var productService = new ProductService(new List<Product>());
            var product = new Product()
            {
                Id = id,
                Name = "abc",
                Price = 10
            };

            // Act
            Action act = () =>
            {
                productService.Add(product);
            };

            // Assert
            var ex = Assert.Throws<Exception>(act);
            Assert.Equal("Missing data", ex.Message);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void WhenProduct_HasNullOrEmptyName_ThrowException(string name)
        {
            // Arrange
            var productService = new ProductService(new List<Product>());
            var product = new Product()
            {
                Id = 1,
                Name = name,
                Price = 10
            };

            // Act
            Action act = () =>
            {
                productService.Add(product);
            };

            // Assert
            var ex = Assert.Throws<Exception>(act);
            Assert.Equal("Missing data", ex.Message);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public void WhenProduct_HasNegativeOr0Price_ThrowException(int price)
        {
            // Arrange
            var productService = new ProductService(new List<Product>());
            var product = new Product()
            {
                Id = 1,
                Name = "abc",
                Price = price
            };

            // Act
            Action act = () =>
            {
                productService.Add(product);
            };

            // Assert
            var ex = Assert.Throws<Exception>(act);
            Assert.Equal("Missing data", ex.Message);
        }

        [Fact]
        public void WhenProduct_IsNull_ThrowException()
        {
            // Arrange
            var productService = new ProductService(new List<Product>());

            // Act
            Action act = () =>
            {
                productService.Add(null);
            };

            // Assert
            var ex = Assert.Throws<Exception>(act);
            Assert.Equal("Null product", ex.Message);
        }

        [Fact]
        public void WhenProduct_AlreadyExists_ThrowException()
        {
            // Arrange
            var product = new Product()
            { 
                Id = 1,
                Name = "abc",
                Price = 10
            };
            var productService = new ProductService(new List<Product>()
            {
                product
            });

            // Act
            Action act = () =>
            {
                productService.Add(product);
            };

            // Assert
            var ex = Assert.Throws<Exception>(act);
            Assert.Equal("Product already exists", ex.Message);
        }
    }
}
