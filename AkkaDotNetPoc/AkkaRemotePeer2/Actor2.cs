﻿using Akka.Actor;

namespace AkkaRemotePeer2;

public class Actor2 : ReceiveActor
{
    public Actor2()
    {
        Receive<string>((msg) => {
            Console.WriteLine("Actor2: " + msg);
        });
    }
}
