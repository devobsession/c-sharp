﻿using Akka.Actor;
using Akka.Configuration;

namespace AkkaRemotePeer2;

class Program
{
    static void Main(string[] args)
    {
        var hoconText = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "hocon.json"));
        Config hoconConfig = ConfigurationFactory.ParseString(hoconText);

        ActorSystem myActorSystem = null;
        try
        {
            myActorSystem = ActorSystem.Create("myActorSystem", hoconConfig);
        }
        catch (Exception)
        {
        }

        IActorRef localActor = myActorSystem.ActorOf(Props.Create(() => new Actor2()), "Actor2");
        localActor.Tell("Actor 2 up");
              
        ActorSelection remoteActor = myActorSystem.ActorSelection(@"akka.tcp://myActorSystem@localhost:8500/user/Actor1");
        Console.WriteLine("Write messages to send to actor 1:");
        while (true)
        {
            var message = Console.ReadLine();
            remoteActor.Tell(message + " from AkkaRemotePeer1");
        }
    }
}
