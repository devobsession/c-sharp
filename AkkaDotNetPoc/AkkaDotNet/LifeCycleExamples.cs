﻿using Akka.Actor;

namespace AkkaDotNetBasics;

public class LifeCycleExamples : ReceiveActor
{
    // Life cycle
    // Start = being initialized by the ActorSystem
    // Receiving = available to process messages
    // Stopping = actor is cleaning up its state. either saving(restrating) or sending message to dead letter box(terminating)
    // Terminated = dead: Any messages sent will go to DeadLetters
    // Restarting = about to restart and go back into a Starting

    // This method is run before the actor can begin receiving messages
    // a good place to put initialization logic
    // Gets called during restarts too
    protected override void PreStart()
    {
    }

    // if the actor's parent restarts the actor
    // can hook in to do cleanup
    // can save the current message for reprocessing later
    protected override void PreRestart(Exception reason, object message)
    {
        base.PreRestart(reason, message);
    }

    // Runs when the actor has stopped and is no longer receiving messages
    // a good place to include clean-up logic
    // also gets called during PreRestart, but you can override PreRestart and simply not call base.PreRestart
    protected override void PostStop()
    {
    }

    // is called during restarts after PreRestart but before PreStart. ie PreRestart => PostRestart => PreStart
    // a good place to do any additional reporting or diagnosis on the error 
    protected override void PostRestart(Exception reason)
    {
    }

}
