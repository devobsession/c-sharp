﻿using Akka.Actor;

namespace AkkaDotNetBasics;

// ========================================================
// ============= Hierarchies and Supervision ==============
// ========================================================
// There are two key reasons actors exist in a hierarchy:
// 1. To atomize work and turn massive amounts of data into manageable chunks
// 2. To contain errors and make the system resilient

public class TopLevelActor : ReceiveActor
{
    public TopLevelActor()
    {
        // When an actor is created inside another actor using its Context it becomes a child
        IActorRef egChildActor = Context.ActorOf(Props.Create(() => new ChildActor()), "egChildActor");
    }

    // Higher-level actors are more supervisional in nature, and this allows the actor system to push risk down and to the edges. 
    // By pushing risky operations to the edges of the hierarchy, the system can isolate risk and recover from errors without the whole system crashing.
    // Every actor has a supervisor, and every actor can also be a supervisor.

    // The Root Guardian: Is the base actor of the entire actor system. "/" and has two children
    //          1. The System Guardian: Runs framework level features "/system" 
    //          2. The Guardian Actor / Root actor: The root of your actor system "/user"
    //                  - Top Level Actors

    // Actors only supervise their children, the level immediately below them in the hierarchy


    // Supervision strategies and directives contain failure within the system.
    // eg by having dangerous code in child actors it doesnt break the system or loose data in the parent actor.
    protected override SupervisorStrategy SupervisorStrategy()
    {
        // OneForOneStrategy(DEFAULT)= directive issued by the parent only applies to the failing child actor and it has no effect on the siblings of the failing child.
        // AllForOneStrategy =  directive issued by the parent applies to the failing child actor AND all of its siblings

        return new OneForOneStrategy(// or AllForOneStrategy
            maxNrOfRetries: 10, // how many times a child can fail within below period of time before it is shut down 
            withinTimeRange: TimeSpan.FromSeconds(30), // time period for retries
            localOnlyDecider: x =>
            {
                if (x is ArithmeticException)
                {
                    return Directive.Resume; // ignores the error: Maybe ArithmeticException is not application critical so we just ignore the error and keep going.
                }
                else if (x is FormatException)
                {
                    return Directive.Escalate; // gets strategy from its parent: Error that we have no idea what to do with
                }
                else if (x is NotSupportedException)
                {
                    return Directive.Stop; // permanently terminates the child actor: Error that we can't recover from, stop the failing child
                }
                else
                {
                    return Directive.Restart; // otherwise restart the failing child (DEFAULT)
                }
            });
    }
}

public class ChildActor : ReceiveActor
{
    // Whenever a child actor has an unhandled exception, then it's up to the parent to decide what to do.

    // 1. Unhandled exception occurs in child actor
    // 2. child suspends operations
    // 3. The system sends a Failure message from child to parent, with the Exception that was raised
    // 4. Parent issues a directive to child telling it what to do        
}
