﻿using Akka.Actor;

namespace AkkaDotNetBasics;

public class EgReceiveActor : ReceiveActor
{
    public EgReceiveActor()
    {
        // can start with a predicate to filter the messages recieved
        Receive<string>(x => x.Length > 10, (msg) => {
            Console.WriteLine("From EgReceiveActor (long msg): " + msg);
        });

        // The second recieve will trigger if the others above did not
        Receive<string>((msg) => {
            Console.WriteLine("From EgReceiveActor (short msg): " + msg);
        });
    }
}
