﻿using Akka.Actor;

namespace AkkaDotNetBasics;

public class ActorRefsExamples : ReceiveActor
{
    public ActorRefsExamples(IActorRef anotherActor)
    {
        // ========== Parameter Ref Actors ==========
        IActorRef refAsParamter = anotherActor;

        // ========== Create Child Actors ==========
        IActorRef childOfThisActor = Context.ActorOf(Props.Create(() => new EgReceiveActor()), "LittleBilly");

        // ========== Context ==========
        // The Context holds metadata about the current state of the actor
        IActorRef parent = Context.Parent;
        IActorRef child = Context.Child("LittleBilly");
        IActorRef sender = Context.Sender;
        IActorRef sender2 = Sender;
        IActorRef self = Context.Self;
        IActorRef self2 = Self;
    }
}
