﻿using Akka.Actor;

namespace AkkaDotNetBasics;

public class ActorSelectionExamples : ReceiveActor
{
    public ActorSelectionExamples()
    {
        // {Protocol}://{ActorSystem}@{Address}/{path}
        // akka.tcp://MyActorSystem@localhost:9001/user/TopLevelActor/ChildActor

        ActorSelection egSelectedActor = Context.ActorSelection("akka.tcp://MyActorSystem@localhost:9001/user/TopLevelActor/ChildActor");

        // the ActorSelection object does not point to a specific IActorRef. 
        // It's a handle that points to every IActorRef that matches the expression/path.

        // NOTE
        //  Avoid ActorSelections: you should always try to use IActorRef.
        //  Don't pass ActorSelections around: They are relative instead of absolute
    }

}
