﻿using Akka.Actor;

namespace AkkaDotNetBasics;

class Program
{
    static void Main(string[] args)
    {
        ActorSystem actorSystem = ActorSystem.Create("MainActorSystem");
        IActorRef actorRef = actorSystem.ActorOf(Props.Create(() => new EgReceiveActor()), "MyActor");

        while (true)
        {
            var msg = Console.ReadLine();
            if (msg == "exit")
            {
                break;
            }

            actorRef.Tell(msg);
        }

        actorSystem.Terminate();
        actorSystem.WhenTerminated.Wait();
    }
}
