﻿using Akka.Actor;
using Akka.Routing;

namespace AkkaDotNetBasics;

public class CreateActorExamples
{
    public void ActorSystemManagement()
    {
        // All actors live within the context of the actor system 
        // The actor system is a reference to the underlying system and Akka.NET framework.
        // The ActorSystem is a heavy object: create only one per application

        ActorSystem actorSystem = ActorSystem.Create("MyActorSystem");

        actorSystem.Terminate(); // shut down the entire actor system (allows the process to exit)
        actorSystem.WhenTerminated.Wait();  // blocks the main thread from exiting until the actor system is shut down
    }

    public void CreatingActors(ActorSystem actorSystem)
    {
        // The IActorRef can be used as a reference to send messages to this actor
        // When you create the actor on the ActorSystem directly, it is a top-level actor.
        IActorRef actorRef = actorSystem.ActorOf(Props.Create(() => new TopLevelActor()), "TopLevelActor"); // It is best practice to name your actors

        // Props
        // Props is a configuration class that encapsulates all the information needed to make an instance of a given type of actor
        // Props get extended to contain deployment information and other configuration details that are needed to do remote work. 
        // For example, Props are serializable, so they can be used to remotely create and deploy entire groups of actors on another machine somewhere on the network!

        Props props1 = Props.Create(typeof(TopLevelActor)); // Not reccomended. Because it has no type safety and can easily introduce bugs at runtime
        Props props2 = Props.Create(() => new TopLevelActor());
        Props props3 = Props.Create<TopLevelActor>();
    }

    // https://getakka.net/articles/actors/routers.html
    public void ScalingActorsWithRouters(ActorSystem actorSystem)
    {
        var props1 = Props.Create<TopLevelActor>().WithRouter(new RoundRobinPool(5));
        var actor1 = actorSystem.ActorOf(props1);

        // Different syntax for the same thing
        var props2 = new RoundRobinPool(5).Props(Props.Create<TopLevelActor>());
        var actor2 = actorSystem.ActorOf(props2);

        // Dynamically Resizable Pools
        var dynamicallyResizablePools =new RoundRobinPool(5).WithResizer(new DefaultResizer(1, 10));
                    
        // Types of routers
        var roundRobinPool = new RoundRobinPool(5);
        var broadcastPool = new BroadcastPool(5);
        var randomPool = new RandomPool(5);
        var scatterGatherFirstCompletedPool = new ScatterGatherFirstCompletedPool(5, TimeSpan.FromSeconds(10));
        var tailChoppingGroup = new TailChoppingPool(5, TimeSpan.FromSeconds(10), TimeSpan.FromMilliseconds(20));
        var smallestMailboxPool = new SmallestMailboxPool(5);
        var consistentHashingPool = new ConsistentHashingPool(5).WithHashMapping(o =>
        {
            if (o is IHasCustomKey)
                return ((IHasCustomKey)o).Key;

            return null;
        });

        var msg = new ConsistentHashableEnvelope("originalMsg", "haskey"); // Can define the hash key for ConsistentHashingPool by wrapping your message message, instead of WithHashMapping.

        // Pools
        // Router "Pools" are routers that create their own worker actors, that is; you provide the number of instances as a parameter to the router and the router will handle routee creation by itself.
        var egPool = new RoundRobinPool(5);

        // Groups
        // Sometimes, rather than having the router actor create its routees, it is desirable to create routees yourself and provide them to the router for its use. You can do this by passing the paths of the routees to the router's configuration. Messages will be sent with ActorSelection to these paths.
        var workers = new[] { "/user/workers/w1", "/user/workers/w3", "/user/workers/w3" };
        var egGroup = new RoundRobinGroup(workers);

        // Supervision
        // Routers are implemented as actors, so a router is supervised by it's parent, and they may supervise children.
        // Pool routers on the other hand create their own children. The router is therefore also the routee's supervisor.

        var supervisorStrategy = new OneForOneStrategy(Decider.From(Directive.Escalate));
        var propsWithSupervisorStrategy = Props.Create<TopLevelActor>().WithRouter(new RoundRobinPool(5)).WithSupervisorStrategy(supervisorStrategy);
    }

    public interface IHasCustomKey
    {
        public int Key { get; set; }
    }

    public class SomeMessage : IConsistentHashable
    {
        public Guid GroupID { get; private set; }
        public object ConsistentHashKey { get { return GroupID; } } // Can define the hash key for ConsistentHashingPool in the message, instead of WithHashMapping.
    }
}
