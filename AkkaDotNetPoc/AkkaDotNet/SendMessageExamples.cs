﻿using Akka.Actor;

namespace AkkaDotNetBasics;

public class SendMessageExamples : ReceiveActor
{
    public SendMessageExamples(IActorRef anotherActor)
    {
        anotherActor.Tell("hi");
        anotherActor.Tell("hi", Context.Parent);

        anotherActor.Forward("hi"); // Forward doesnt impact the Context, sender will be whoever this sender was

        var answer = anotherActor.Ask<int>("hi"); // returns an int
        var result = answer.Result; // wait for answer
        
        anotherActor.Ask<int>("hi").PipeTo(Self); // this thread continues and sends the result back as a message (eiter the answer or an exception)
    }
}
