﻿using Akka.Actor;

namespace AkkaDotNetBasics;

public class EgUntypedActor : UntypedActor
{
    public class NewMessage
    {
    }

    // UntypedActor's must implement an OnReceive method to accept incoming messages            
    protected override void OnReceive(object message)
    {
        // since the message recieved is an unknown type the message must be checked and parsed
        if (message is NewMessage)
        {
            var msg = message as NewMessage;
        }
        else
        {
            // Unhandled messages are automatically sent to Unhandled so the logging is done for you
            Unhandled(message);
        }
    }
}
