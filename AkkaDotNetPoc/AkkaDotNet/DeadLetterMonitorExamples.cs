﻿using Akka.Actor;
using Akka.Event;

namespace AkkaDotNetBasics;

public class DeadLetterMonitorExamples
{
    void Main()
    {
        ActorSystem system = ActorSystem.Create("MySystem");
        var deadletterWatchMonitorProps = Props.Create(() => new DeadletterMonitor());
        var deadletterWatchActorRef = system.ActorOf(deadletterWatchMonitorProps, "DeadLetterMonitoringActor");
        system.EventStream.Subscribe(deadletterWatchActorRef, typeof(DeadLetter));

        // Simulate lost message
        var expendableActorProps = Props.Create(() => new BadActor());
        var expendableActorRef = system.ActorOf(expendableActorProps, "BadActor");
        expendableActorRef.Tell(PoisonPill.Instance);
        expendableActorRef.Tell("another message");

    }

    public class DeadletterMonitor : ReceiveActor
    {

        public DeadletterMonitor()
        {
            Receive<DeadLetter>(dl => HandleDeadletter(dl));
        }

        private void HandleDeadletter(DeadLetter dl)
        {
            Console.WriteLine($"DeadLetter captured: {dl.Message}, sender: {dl.Sender}, recipient: {dl.Recipient}");
        }
    }

    public class BadActor : ReceiveActor { }
}
