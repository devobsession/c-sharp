﻿using Akka.Actor;

namespace AkkaDotNetBasics;

public class TimersOnActorExamples : ReceiveActor, IWithTimers
{
    public ITimerScheduler Timers { get; set; }

    private readonly string _singleTimerKey = "test StartSingleTimer key";
    private readonly string _periodicTimerKey = "test StartPeriodicTimer key";

    public TimersOnActorExamples()
    {
        Receive<string>((msg) => {
            Console.WriteLine(msg);

            // The key can be used to query whether that timer is still running or not
            var isTimerActive = Timers.IsTimerActive(_periodicTimerKey);
            Console.WriteLine(isTimerActive);

            // the key can be used to stop those timers
            Timers.Cancel(_periodicTimerKey);

            // Can Cancel All timers
            Timers.CancelAll();

        });
    }

    protected override void PreStart()
    {
        // timers will be automatically disposed when the actor is stopped
        Timers.StartSingleTimer(_singleTimerKey, "This is a StartSingleTimer msg", TimeSpan.FromSeconds(10));
        Timers.StartPeriodicTimer(_periodicTimerKey, "This is a StartPeriodicTimer msg", TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(10));

        // If the same key is used then it will overwrite the original timer and the actor will not consume any messages sent from it, even if all ready in the inbox.
        Timers.StartPeriodicTimer(_periodicTimerKey, "This is the second StartPeriodicTimer msg", TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(10));
    }
}