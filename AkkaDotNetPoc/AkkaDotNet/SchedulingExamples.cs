﻿using Akka.Actor;

namespace AkkaDotNetBasics;

public class SchedulingExamples
{
    public void Schedule(ActorSystem actorSystem, IActorRef actorRef, object message)
    {
        actorSystem
           .Scheduler
           .ScheduleTellRepeatedly(TimeSpan.FromSeconds(0),
                     TimeSpan.FromSeconds(5),
                     actorRef, message, ActorRefs.NoSender);
    }
}
