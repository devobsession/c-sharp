﻿using Akka.Actor;
using Akka.Configuration;

namespace AkkaRemotePeer1;

class Program
{
    static void Main(string[] args)
    {
        var hoconText = File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "hocon.json"));
        Config hoconConfig = ConfigurationFactory.ParseString(hoconText);

        ActorSystem myActorSystem = null;
        try
        {
            myActorSystem = ActorSystem.Create("myActorSystem", hoconConfig);
        }
        catch (Exception)
        {
        }      

        IActorRef localActor = myActorSystem.ActorOf(Props.Create(() => new Actor1()), "Actor1");
        localActor.Tell("Actor 1 up. Run Actor 2...");

        Console.ReadLine();
    }
}
