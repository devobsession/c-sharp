﻿using Akka.Actor;

namespace AkkaRemotePeer1;

public class Actor1 : ReceiveActor
{
    public Actor1()
    {
        Receive<string>((msg) => {
            Console.WriteLine("Actor1: " + msg);
        });
    }
}
