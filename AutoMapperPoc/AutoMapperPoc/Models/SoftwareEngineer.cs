﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoMapperPoc
{
    public class SoftwareEngineer
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string FavoriteFood { get; set; }
        public string Country { get; set; }
    }
}
