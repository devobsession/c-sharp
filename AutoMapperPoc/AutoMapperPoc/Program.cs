﻿using System;
using System.Diagnostics;

namespace AutoMapperPoc
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person()
            { 
                FirstName = "Brandon",
                Surname = "Pearman",
                Age = 33,
                Country = "South Africa",
                FavoriteFood = "Burger"
            };

            int iterationCount = 1000;
            Console.WriteLine("=========== " + iterationCount + " iterations ===========");
            ManualMappingTest(person, iterationCount);
            ReflectionMappingTest(person, iterationCount);
            AutoMapperMappingTest(person, iterationCount);
            Console.WriteLine();

            iterationCount = 1000000;
            Console.WriteLine("=========== "+ iterationCount + " iterations ===========");
            ManualMappingTest(person, iterationCount);
            ReflectionMappingTest(person, iterationCount);
            AutoMapperMappingTest(person, iterationCount);
            Console.WriteLine();

            Console.WriteLine("Manual mapping is faster but has maintenance issues");
            Console.WriteLine("AutoMapper maintainable vs Manual mapping, and it is more stable and faster than doing your own reflection");
        }
        
        private static void ManualMappingTest(Person person, int iterationCount)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < iterationCount; i++)
            {
                SoftwareEngineer softwareEngineer = ManualMapping(person);
            }
            sw.Stop();
            Console.WriteLine("Manual Mapping Test time = " + sw.Elapsed);
        }

        private static void ReflectionMappingTest(Person person, int iterationCount)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < iterationCount; i++)
            {
                SoftwareEngineer softwareEngineer = ReflectionMapping<SoftwareEngineer>(person);
            }
            sw.Stop();
            Console.WriteLine("Reflection Mapping Test time = " + sw.Elapsed);
        }
        
        private static void AutoMapperMappingTest(Person person, int iterationCount)
        {
            Stopwatch sw = new Stopwatch();
            // Mappings class for startup has been excluded
            // It is expensive and in practice should not run on every iteration but on application start up
            // It will slow down start up but not mapping
            Mappings mappings = new Mappings(); 
            sw.Start();
            for (int i = 0; i < iterationCount; i++)
            {
                SoftwareEngineer softwareEngineer = mappings.Map<SoftwareEngineer>(person);
            }
            sw.Stop();
            Console.WriteLine("AutoMapper Mapping Test time = " + sw.Elapsed);
        }
        
        private static SoftwareEngineer ManualMapping(Person person)
        {
            return new SoftwareEngineer()
            {
                FirstName = person.FirstName,
                Surname = person.Surname,
                Age = person.Age,
                Country = person.Country,
                FavoriteFood = person.FavoriteFood
            };
        }

        private static Tdirection ReflectionMapping<Tdirection>(object source)
        {
            var sourceProperties = source.GetType().GetProperties();
            var directionType = typeof(Tdirection);
            Tdirection directionObject = (Tdirection)Activator.CreateInstance(directionType);

            foreach (var sourceProperty in sourceProperties)
            {
                var directionProperty = directionType.GetProperty(sourceProperty.Name);
                directionProperty.SetValue(directionObject, sourceProperty.GetValue(source));
            }

            return directionObject;
        }

    }
}
