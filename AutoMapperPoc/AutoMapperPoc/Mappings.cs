﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoMapperPoc
{
    public class Mappings
    {
        private readonly IMapper _mapper;

        public Mappings()
        {
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Person, SoftwareEngineer>();
            });

            _mapper = config.CreateMapper();
        }

        public TDestination Map<TDestination>(object source)
        {
            return _mapper.Map<TDestination>(source);
        }
    }
}
