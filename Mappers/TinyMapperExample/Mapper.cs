﻿using System.Reflection;
using Nelibur.ObjectMapper;

namespace TinyMapperExample;
public class Mapper
{
    public static void RegisterMapping<TSource, TTarget>()
    {
        TinyMapper.Bind<TSource, TTarget>();
    }

    public static void RegisterMapping(Type source, Type target)
    {
        TinyMapper.Bind(source, target);
    }

    public static TTarget Map<TTarget>(object source)
    {
        return TinyMapper.Map<TTarget>(source);
    }

    public static void RegisterMapToBaseMappers(Assembly assembly)
    {
        var mappingRegistryTypes = assembly.GetTypes()
            .Where(prop => Attribute.IsDefined(prop, typeof(MapToBaseAttribute)));

        foreach (var mappingRegistryType in mappingRegistryTypes)
        {
            Mapper.RegisterMapping(mappingRegistryType, mappingRegistryType.BaseType);
            Mapper.RegisterMapping(mappingRegistryType.BaseType, mappingRegistryType);

            Mapper.RegisterMapping(mappingRegistryType, mappingRegistryType);
            Mapper.RegisterMapping(mappingRegistryType.BaseType, mappingRegistryType.BaseType);
        }
    }
}

