﻿
using System.Reflection;
using System.Text.Json;
using TinyMapperExample;

namespace Mappers;

public class TinyMapperMapToBaseExample
{
    public static void Run()
    {
        Mapper.RegisterMapToBaseMappers(Assembly.GetExecutingAssembly());

        NinjaRatToRat();
        RatToNinjaRat();
    }

    private static void NinjaRatToRat()
    {
        var ninjaRat = new NinjaRat()
        {
            Name = "Gregg",
            FavoriteFood = "Watermelon",
            FightingStyle = "Butt Foe"
        };
        var rat = Mapper.Map<Rat>(ninjaRat);
        Console.WriteLine(JsonSerializer.Serialize(rat));
    }

    private static void RatToNinjaRat()
    {
        var rat = new Rat()
        {
            Name = "Gregg",
            FavoriteFood = "Watermelon"
        };
        var ninjaRat = Mapper.Map<NinjaRat>(rat);
        ninjaRat.FightingStyle = "Butt Foe";
        Console.WriteLine(JsonSerializer.Serialize(ninjaRat));
    }
}
