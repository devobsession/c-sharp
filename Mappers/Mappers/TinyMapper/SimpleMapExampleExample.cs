﻿using System.Text.Json;
using TinyMapperExample;

namespace Mappers;

public class SimpleMapExampleExample
{
    public static void Run()
    {
        SimpleMap();
    }

    private static void SimpleMap()
    {
        Mapper.RegisterMapping<User, UserViewModel>();

        var user = new User()
        {
            Id = 3,
            Name = "Billy",
            Secret = "kisses frogs"
        };

        var viewModel = Mapper.Map<UserViewModel>(user);
        Console.WriteLine(JsonSerializer.Serialize(viewModel));
    }
}
