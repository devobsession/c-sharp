﻿using System.Text.Json;
using TinyMapperExample;

namespace Mappers;
public class Program
{
    public static void Main(string[] args)
    {
        TinyMapperMapToBaseExample.Run();

        SimpleMapExampleExample.Run();
    } 
}
