﻿namespace Mappers;

public class User
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Secret { get; set; }
}

public class UserViewModel
{
    public int Id { get; set; }
    public string Name { get; set; }
}