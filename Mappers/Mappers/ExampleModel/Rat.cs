﻿
using TinyMapperExample;

namespace Mappers;

[MapToBase]
public class NinjaRat : Rat
{
    public string FightingStyle { get; set; }
}

public class Rat
{
    public string Name { get; set; }
    public string FavoriteFood { get; set; }
}
