﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Overloading
{

    /// <summary>
    /// Overloaded methods are Compile time Polymorphism.
    /// This class has multiple methods with the same name but different signitures.
    /// </summary>
    public class Calculator
    {

        public void Add(int a, int b)
        {
            Console.WriteLine(a + b);
        }

        public void Add(int a, int b, int c)
        {
            Console.WriteLine(a + b + c);
        }

        public void Add(List<int> nums)
        {
            Console.WriteLine(nums.Sum());
        }
        
    }
}
