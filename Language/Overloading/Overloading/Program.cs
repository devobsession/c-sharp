﻿using Overloading.Cast_Operator_Overloading;
using System;
using System.Collections.Generic;

namespace Overloading
{
    class Program
    {
        static void Main(string[] args)
        {
            UseOverloadedCalculator();
            
            UseOverloadedOperatorsOnCollection();

            UseOverloadedCasting();
        }

        public static void UseOverloadedCalculator()
        {
            Console.WriteLine("============ Overloaded methods ============");

            Calculator calculator = new Calculator();
            calculator.Add(1, 1); // 2
            calculator.Add(1, 1, 1); // 3
            calculator.Add(new List<int>() {1, 1, 1, 1 }); // 4

            Console.WriteLine();
        }

        
        public static void UseOverloadedOperatorsOnCollection()
        {
            HobbyCollection collection1 = new HobbyCollection()
            {
                Items = new List<string>() { "stamps", "coins" },
                Value = 10000
            };
            
            HobbyCollection collection2 = new HobbyCollection()
            {
                Items = new List<string>() { "books", "precious stones", "butterflies" },
                Value = 50000
            };

            Console.WriteLine("============ Overloaded operators ============");

            HobbyCollection jointCollection = collection1 + collection2;
            PrintCollection(jointCollection);

            HobbyCollection largerCollection = collection1 + 9999;
            PrintCollection(largerCollection);
            
            if (collection1 > collection2)
            {
                Console.WriteLine("collection1 is larger");
            }


            if (collection1 == collection2)
            {
                Console.WriteLine("collections are equal");
            }

            Console.WriteLine();
        }

        private static void PrintCollection(HobbyCollection collection)
        {
            Console.Write("Items in the collection:");
            foreach (var item in collection.Items)
            {
                Console.Write(item + ", ");
            }
            Console.WriteLine("Valued at: " + collection.Value);
        }

        public static void UseOverloadedCasting()
        {
            Console.WriteLine("============ Overloaded Casts ============");


            // implicit cast
            Person person1 = new Person()
            {
                Name = "Sarah",
                Age = 24
            };

            Employee employee1 = person1;
                       

            // explict cast
            Employee employee2 = new Employee()
            {
                Name = "Joe",
                Age = 32,
                EmployeeId =2884
            };
            
            Person person2 = (Person)employee2;

            int i = (int)person2;
                        
            Console.WriteLine("printing the name... " + employee1.Name);
            Console.WriteLine("printing the name... " + person2.Name);
        }
    }
}
