﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overloading.Cast_Operator_Overloading
{
    public class Employee
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int EmployeeId { get; set; }

        /// <summary>
        /// implicit = do not have to cast it directly 
        /// Employee e = person;
        /// </summary>
        /// <param name="person"></param>
        public static implicit operator Employee(Person person)
        {
            return new Employee()
            {
                Name = person.Name,
                Age = person.Age
            };
        }

        /// <summary>
        /// explicit = have to cast it directly
        /// eg Person p = (Person)employee; but information is lost since the property EmployeeId is not used by Person
        /// In cases like this where information would be lost the cast should be explcit 
        /// this lets developer know information is lost in the cast
        /// </summary>
        /// <param name="employee"></param>
        public static explicit operator Person(Employee employee)
        {
            return new Person()
            {
                Name = employee.Name,
                Age = employee.Age
            };
        }
    }
}
