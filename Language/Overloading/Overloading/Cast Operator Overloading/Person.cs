﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overloading.Cast_Operator_Overloading
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }

        /// <summary>
        /// Converts the Person class into an int
        /// int i = (int)person;
        /// </summary>
        /// <param name="employee"></param>
        public static explicit operator int(Person person)
        {
            return person.Age;
        }

        public static implicit operator Person(int i)
        {
            return new Person()
            {
                Age = i
            };
        }
    }
}
