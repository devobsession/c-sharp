﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overloading
{
    public class HobbyCollection
    {
        public List<string> Items { get; set; }
        public int Value { get; set; }

        /// <summary>
        /// You can specify how two objects will add together
        /// </summary>
        /// <param name="collection1"></param>
        /// <param name="collection2"></param>
        /// <returns></returns>
        public static HobbyCollection operator +(HobbyCollection collection1, HobbyCollection collection2) 
        {
            List<string> jointsItems = new List<string>();
            jointsItems.AddRange(collection1.Items);
            jointsItems.AddRange(collection2.Items);

            int jointValue = collection1.Value + collection2.Value;

            return new HobbyCollection()
            {
                Items = jointsItems,
                Value = jointValue
            };
        }

        /// <summary>
        /// You can add any two objects/values together
        /// </summary>
        /// <param name="collection1"></param>
        /// <param name="extraValue"></param>
        /// <returns></returns>
        public static HobbyCollection operator +(HobbyCollection collection1, int extraValue)
        {
            collection1.Value += extraValue;

            return collection1;
        }


        // Conditional operators should have their opposite overloaded as well eg if you do > then also do <, if you do == then also do !=

        public static bool operator ==(HobbyCollection collection1, HobbyCollection collection2)
        {
            return collection1.Value == collection2.Value;
        }

        public static bool operator !=(HobbyCollection collection1, HobbyCollection collection2)
        {
            // != has to be implemented with ==
            // Reuse the == overload to stay DRY, and return the negated value
            return !(collection1 == collection2);
        }

        public static bool operator <(HobbyCollection collection1, HobbyCollection collection2)
        {
            return collection1.Value < collection2.Value;
        }

        public static bool operator >(HobbyCollection collection1, HobbyCollection collection2)
        {
            // Reuse the < overload to stay DRY, and return the negated value
            return !(collection1 < collection2);
        }

        public static bool operator <=(HobbyCollection collection1, HobbyCollection collection2)
        {
            // Reuse the < and == overloads to stay DRY
            return (collection1 < collection2 || collection1 == collection2);
        }
              
        public static bool operator >=(HobbyCollection collection1, HobbyCollection collection2)
        {
            // Reuse the > and == overloads to stay DRY
            return (collection1 > collection2 || collection1 == collection2);
        }

        public static bool operator |(HobbyCollection collection1, HobbyCollection collection2)
        {
            return (collection1.Items?.Count > 0) || (collection2.Items?.Count > 0);
        }

        public static bool operator &(HobbyCollection collection1, HobbyCollection collection2)
        {
            return (collection1.Items?.Count > 0) && (collection2.Items?.Count > 0);
        }

        /// <summary>
        /// If you overload == it is highly recommended to override Equals as well
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj is HobbyCollection hobbyCollection)
            {
                // can use the overloaded == to stay DRY
                return this == hobbyCollection;
            }

            return false;
        }

        /// <summary>
        /// If Equals has been overriden you must override GetHashCode as well
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (Value, Items).GetHashCode();
        }
    }
}
