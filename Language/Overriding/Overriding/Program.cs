﻿using System;

namespace Overriding
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("========= Overriding an explicitly inherited class =========");
            Console.WriteLine("Base classes such as Animal can have it's methods overriden from a derived class like Rat.");
            Animal animal = new Rat();
            Rat rat = new Rat();            
            HiddenTalkMethod(animal, rat);
            OverridenPoopMethod(animal, rat);
            OverridenEatMethod(animal, rat);
            HiddenField(animal, rat);

            Console.WriteLine("========= Overriding the implicitly inherited Object class =========");
            Console.WriteLine("All objects in C# inherit from the Object class, which has some methods which can be overridden.");
            Car car1 = new Car("Tesla", "Blue");
            Car car2 = new Car("Merc", "White");
            OverridenToStringMethod(car1);
            OverridenEqualsMethod_True(car1);
            OverridenEqualsMethod_False(car1, car2);
            OverridenGetHashCodeMethod(car1);  

            Console.ReadLine();
        }

        private static void HiddenField(Animal animal, Rat rat)
        {
            Console.WriteLine("========= How Many Legs? =========");

            Console.WriteLine(animal.Legs); // 0
            Console.WriteLine(((Rat)animal).Legs); // 4

            Console.WriteLine(rat.Legs); // 4
            Console.WriteLine(((Animal)rat).Legs); // 0
           
            Console.WriteLine();
        }

        private static void HiddenTalkMethod(Animal animal, Rat rat)
        {
            Console.WriteLine("========= Talk =========");

            animal.Talk(); // Squeak
            ((Rat)animal).Talk(); // Squeak

            rat.Talk(); // Squeak
            ((Animal)rat).Talk(); // Squeak

            Console.WriteLine();
        }

        private static void OverridenPoopMethod(Animal animal, Rat rat)
        {
            Console.WriteLine("========= Poop =========");

            animal.Poop(); // smelly poop
            ((Rat)animal).Poop(); // neat little ball of poop
                       
            rat.Poop(); // neat little ball of poop
            ((Animal)rat).Poop(); // smelly poop

            Console.WriteLine();
        }

        private static void OverridenEatMethod(Animal animal, Rat rat)
        {
            Console.WriteLine("========= Eat =========");

            animal.Eat(); // Nom Nom Nom
            ((Rat)animal).Eat(); // Nom Nom Nom

            rat.Eat(); // Nom Nom Nom
            ((Animal)rat).Eat(); // Nom Nom Nom

            Console.WriteLine();
        }
        
        private static void OverridenToStringMethod(Car car)
        {
            Console.WriteLine("========= ToString =========");

            Console.WriteLine(car.ToString());            

            Console.WriteLine();
        }

        private static void DefaultEqualsMethod()
        {
            Console.WriteLine("========= Default Equals =========");

            // Even though rat1 and rat2 are identical property wise, 
            // the Equals method returns false because the default behaviour of Equals is purely a reference check
            Rat rat1 = new Rat();
            Rat rat2 = new Rat();
            Console.WriteLine(rat1.Equals(rat2)); // false

            Console.WriteLine();
        }

        private static void OverridenEqualsMethod_False(Car car1, Car car2)
        {
            Console.WriteLine("========= False Equals =========");
            Console.WriteLine(car1.Equals(car2));
            
            Console.WriteLine(car1.Equals(null));

            Console.WriteLine(car1.Equals(new Rat()));

            Console.WriteLine();
        }

        private static void OverridenEqualsMethod_True(Car car1)
        {
            Console.WriteLine("========= True Equals =========");

            Car tempCar = car1;
            Console.WriteLine(car1.Equals(tempCar));

            Car car2 = new Car(car1.Model, "yellow");
            Console.WriteLine(car1.Equals(car2));

            Console.WriteLine();
        }

        private static void OverridenGetHashCodeMethod(Car car)
        {
            Console.WriteLine("========= GetHashCode =========");

            Console.WriteLine(car.GetHashCode());
            
            Console.WriteLine();
        }
    }
}
