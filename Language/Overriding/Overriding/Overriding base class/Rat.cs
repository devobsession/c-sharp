﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overriding
{
    public class Rat : Animal
    {
        // Hide base field with the "new" keyword
        public new int Legs = 4;

        /// <summary>
        /// with the "new" keyword, the base classes implementation is hidden but still exists in its form
        /// but this implementation is used in the derived class's form.
        /// </summary>
        public new void Talk()
        {
            Console.WriteLine("Squeak");
        }

        /// <summary>
        /// The "override" keyword completely overrides the base class implementation
        /// so that the base class implementation does not exist regardless of the objects form.
        /// </summary>
        public override void Poop()
        {
            Console.WriteLine("neat little ball of poop");
        }

        public override void Eat()
        {
            Console.WriteLine("Nom Nom Nom");
        }
         
    }
}
