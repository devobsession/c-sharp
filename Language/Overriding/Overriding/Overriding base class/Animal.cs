﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overriding
{
    public abstract class Animal
    {
        // Fields can also be hidden with the "new" keyword
        public int Legs = 0;

        /// <summary>
        /// Derived class can:
        /// 1. hide base method with "new" keyword
        /// 3. keep base method implementation
        /// </summary>
        public void Talk()
        {
            Console.WriteLine("...");
        }

        /// <summary>
        /// Derived class can:
        /// 1. hide base method with "new" keyword
        /// 2. override base method with "override" keyword
        /// 3. keep base method implementation
        /// </summary>
        public virtual void Poop()
        {
            Console.WriteLine("smelly poop");
        }

        /// <summary>
        /// Derived class:
        /// MUST override with "override" keyword
        /// </summary>
        public abstract void Eat();

    }
}
