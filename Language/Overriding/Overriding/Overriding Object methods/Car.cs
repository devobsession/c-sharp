﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Overriding
{
    /// <summary>
    ///  All classes in C# implicitly inherit from the Object class
    ///  therefore you can override it's methods
    /// </summary>
    public class Car
    {
        public string Model { get; set; }
        public string Color { get; set; }

        public Car(string model, string color)
        {
            Model = model;
            Color = color;
        }

        /// <summary>
        /// Can return whatever string you would like to represent this object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"original ToString:{base.ToString()}, - Overriden ToString: This car is a {Color} {Model}";
        }

        /// <summary>
        /// 1. Check the reference since null's fail immediatly (adds performance)
        /// 2. Check the same reference since that passes immediatly (adds performance)
        /// 3. Compare the Type since objects of different Types are not equal (adds performance)
        /// 4. Then cast and check the specifics of the class
        /// Of course, all of this can be done however you would like to check equality
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            Car otherCar = obj as Car;
            if (otherCar == null)
            {
                return false;
            }

            return otherCar.Model.Equals(this.Model);
        }

        /// <summary>
        /// Can set how this object will calculate its hash
        /// </summary>
        /// <returns></returns>
        //public override int GetHashCode()
        //{
        //    var hashCode = -1388735503;
        //    hashCode = hashCode * -1521134295 + Color.GetHashCode();
        //    hashCode = hashCode * -1521134295 + Model.GetHashCode();
        //    return hashCode;
        //}

        /// <summary>
        /// Since there is already a good HashCode function it's easier to use it.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            // pre C#7 
            // GetHashCode of an Anonymous Type which creates an object on the heap (Garbage) 
            // new { Color, Model }.GetHashCode();

            // C# 7
            // GetHashCode of a ValueTuple which executes on the stack (no Garbage) 
            return (Color, Model).GetHashCode();  
        }
    }
}
