﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IIS.Core;
using Microsoft.Extensions.Logging;

namespace AyncAwait.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AsyncAwaitController : ControllerBase
    {

        [HttpGet("Awaiter")]
        public IActionResult GetAwaiter()
        {
            StringBuilder logs = new StringBuilder();
            logs.AppendLine("Start GetAwaiter: " + Thread.CurrentThread.ManagedThreadId);
            AsyncMethod(logs).GetAwaiter().GetResult();
            logs.AppendLine("End GetAwaiter: " + Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine(logs.ToString());
            return Ok(logs.ToString());
        }

        [HttpGet("Wait")]
        public IActionResult GetWait()
        {
            StringBuilder logs = new StringBuilder();
            logs.AppendLine("Start GetWait: " + Thread.CurrentThread.ManagedThreadId);
            AsyncMethod(logs).Wait();
            logs.AppendLine("End GetWait: " + Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine(logs.ToString());
            return Ok(logs.ToString());
        }

        [HttpGet("Await")]
        public async Task<IActionResult> GetAwait()
        {
            StringBuilder logs = new StringBuilder();
            logs.AppendLine("Start GetAwait: " + Thread.CurrentThread.ManagedThreadId);
            await AsyncMethod(logs);
            logs.AppendLine("End GetAwait: " + Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine(logs.ToString());
            return Ok(logs.ToString());
        }

        private async Task AsyncMethod(StringBuilder logs)
        {
            logs.AppendLine("Start AsyncMethod: " + Thread.CurrentThread.ManagedThreadId);
            await Task.Delay(10);
            logs.AppendLine("End AsyncMethod: " + Thread.CurrentThread.ManagedThreadId);
        }
    }
}
