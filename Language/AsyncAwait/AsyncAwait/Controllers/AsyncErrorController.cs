﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IIS.Core;
using Microsoft.Extensions.Logging;

namespace AyncAwait.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AsyncErrorController : ControllerBase
    {        
        [HttpGet("Awaiter")]
        public IActionResult GetAwaiter()
        {
            StringBuilder logs = new StringBuilder();
            logs.AppendLine("Start GetAwaiter: " + Thread.CurrentThread.ManagedThreadId);
            try
            {
                AsyncErrorMethod(logs).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                logs.AppendLine("Exception: " + ex.Message);
            }
            logs.AppendLine("End GetAwaiter: " + Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine(logs.ToString());
            return Ok(logs.ToString());
        }

        [HttpGet("Wait")]
        public IActionResult GetWait()
        {
            StringBuilder logs = new StringBuilder();
            logs.AppendLine("Start GetWait: " + Thread.CurrentThread.ManagedThreadId);
            try
            {
                AsyncErrorMethod(logs).Wait();
            }
            catch (Exception ex)
            {
                logs.AppendLine("Exception: " + ex.Message);
            }
            logs.AppendLine("End GetWait: " + Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine(logs.ToString());
            return Ok(logs.ToString());
        }

        [HttpGet("Await")]
        public async Task<IActionResult> GetAwait()
        {
            StringBuilder logs = new StringBuilder();
            logs.AppendLine("Start GetAwait: " + Thread.CurrentThread.ManagedThreadId);

            try
            {
                await AsyncErrorMethod(logs);
            }
            catch (Exception ex)
            {
                logs.AppendLine("Exception: " + ex.Message);
            }
          
            logs.AppendLine("End GetAwait: " + Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine(logs.ToString());
            return Ok(logs.ToString());
        }

        private async Task AsyncErrorMethod(StringBuilder logs)
        {
            logs.AppendLine("Start AsyncErrorMethod: " + Thread.CurrentThread.ManagedThreadId);
            await Task.Delay(10);
            logs.AppendLine("End AsyncErrorMethod: " + Thread.CurrentThread.ManagedThreadId);
            throw new Exception("This is the original error message");
        }
    }
}
