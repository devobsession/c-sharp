﻿using System;
using System.Diagnostics;

namespace WorkingWithProcesses
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }


        private static void RetrieveLogs(string containerName)
        {
            var processInfo = new ProcessStartInfo("docker", $"logs -f {containerName}");

            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardOutput = true;
            processInfo.RedirectStandardError = true;

            int exitCode;
            using (var process = new Process())
            {
                process.StartInfo = processInfo;
                process.OutputDataReceived += new DataReceivedEventHandler((sender, args) =>
                {
                    Console.WriteLine(args.Data ?? string.Empty);
                });
                process.ErrorDataReceived += new DataReceivedEventHandler((sender, args) =>
                {
                    Console.WriteLine(args.Data ?? string.Empty);
                });

                process.Start();

                process.BeginOutputReadLine();
                process.BeginErrorReadLine();

                process.WaitForExit();
                if (!process.HasExited)
                {
                    process.Kill();
                }

                exitCode = process.ExitCode;
                Console.WriteLine(exitCode);
                process.Close();
            }
        }
    }
}
