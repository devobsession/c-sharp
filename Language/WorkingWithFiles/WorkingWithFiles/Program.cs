﻿using System;
using System.IO;

namespace WorkingWithFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }


        private static void TailFile(string filePath)
        {
            using (StreamReader reader = new StreamReader(new FileStream(filePath,
                     FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                long lastMaxOffset = reader.BaseStream.Length;

                while (true)
                {
                    System.Threading.Thread.Sleep(100);

                    if (reader.BaseStream.Length == lastMaxOffset)
                    {
                        continue;
                    }

                    reader.BaseStream.Seek(lastMaxOffset, SeekOrigin.Begin);

                    string line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                    }

                    lastMaxOffset = reader.BaseStream.Position;
                }
            }
        }

    }
}
