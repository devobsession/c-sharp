﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Strings
{
    public class Chars
    {
        public static void Comparison()
        {
            var isDigit = char.IsDigit('2');
            var isLetter = char.IsLetter('a');
            var isLetterOrDigit = char.IsLetterOrDigit('a');
            var isLower = char.IsLower('a');
            var isPunctuation = char.IsPunctuation('.');
            var isSeparator = char.IsSeparator(',');
        }

        public static void SurrogatePairs()
        {
            Console.WriteLine("========== Surrogate Pairs ==========");
            string text = "a𠈓b";

            // Char length
            Console.WriteLine("text length = " + text.Length);

            // Char elements length
            StringInfo si = new StringInfo(text);
            Console.WriteLine("Text Elements count = " + si.LengthInTextElements);
            Console.WriteLine("This shows that 3 chars are actually 4 under the covers");
            Console.WriteLine();

            // Retrieve single element
            string aChar = StringInfo.GetNextTextElement(text, 0); // a
            string surrogateChar = StringInfo.GetNextTextElement(text, 1); // 𠈓
            string bChar = StringInfo.GetNextTextElement(text, 3); // b
            Console.WriteLine(aChar);
            Console.WriteLine(surrogateChar);
            Console.WriteLine(bChar);
            Console.WriteLine();

            // get index of surrogate pair
            int[] indexes = StringInfo.ParseCombiningCharacters(text);  // 0, 1 and 3
            Console.WriteLine("Text Elements on indexes = " + string.Join(',', indexes));
            Console.WriteLine();

            // Check chars if Surrogate Pair
            bool isSurrogate = Char.IsSurrogatePair(text[1], text[2]); // true
            Console.WriteLine("is index 1 and 2 a surrogatePair = " + isSurrogate);
            Console.WriteLine();

        }
    }
}
