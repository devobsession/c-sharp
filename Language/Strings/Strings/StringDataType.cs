﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Strings
{
    public class StringDataType
    {
        public static void StringsAreCharArrays()
        {
            // A string is an array of chars, and can be treat like any other array
            string myString = "hello";

            // string implements IEnumerable
            foreach (char myStringElement in myString)
            {
                char charFromString = myStringElement;
            }

            int index = myString[2];

            char[] explicitCharArray = myString.ToCharArray();

        }

        public static void StringsAreReferenceTypes()
        {
            string myString = "hello";

            bool isValueType = myString.GetType().IsValueType; // false

            // Strings are stored on the heap because they do not have a default allocation size (0 to +-2 billion Unicode characters)

            // Even though strings are reference types they behave like value types in many cases eg
            string a = "batman";
            string b = "batman";
            bool isTrue = a == b; // true
            // the == operator for System.String always checks for string equivalence, rather than object identity
            // passing a ref around (on heap) is far more effiecient, since strings can potentially get quite large it would cause problems if it was on the stack.
        }

        // In .net strings are immutable
        // Strings can not be changes
        // When a change is attempted a new string is created
        public static void StringsAreImmutable()
        {
            string a = "apple";
            string b = a;

            Console.WriteLine("different objects pointing to same string");
            if (object.ReferenceEquals(a, b))
            {
                Console.WriteLine("Reference Equal");
            }
            else
            {
                Console.WriteLine("Reference Not Equal");
            }

            Console.WriteLine("the moment the string is changed the reference is lost because it allocates a new string");
            b = a + "n";
            if (object.ReferenceEquals(a, b))
            {
                Console.WriteLine("Reference Equal");
            }
            else
            {
                Console.WriteLine("Reference Not Equal");
            }
        }

        public static void StringsAppendVsStringBuilderTest(int iterationCount)
        {
            // Changing strings frequently(such as appending in a for loop) takes a lot of memory on the heap which will call the garbage collector more and hurt perfomance.
            // Therefore it is recommend to use stringbuilder, which reserves more space than needed so it can easily be added more chars.
            Console.WriteLine("========== String Appending Vs StringBuilder Test ==========");
            Stopwatch sw = new Stopwatch();
            string s = "";
            sw.Start();
            for (int i = 0; i < iterationCount; i++)
            {
                s += "A";
            }
            sw.Stop();
            Console.WriteLine("string appending test = " + sw.Elapsed);

            sw.Reset();
            StringBuilder sb = new StringBuilder();
            sw.Start();
            for (int i = 0; i < iterationCount; i++)
            {
                sb.Append("A");
            }
            sw.Stop();
            Console.WriteLine("StringBuilder appending test = " + sw.Elapsed);
            Console.WriteLine();
        }

        public static void HowStringBuilderWorks()
        {
            Console.WriteLine("========== How StringBuilder Works ==========");
            // StringBuilder initially reserves space for 16 characters so that appending does not require brand new memory space. 
            // Capacity then doubles each time it is reached.
            StringBuilder sb = new StringBuilder();
            Console.WriteLine("StringBuilder Capacity = " + sb.Length + " / " + sb.Capacity);

            for (int i = 0; i < 100; i++)
            {
                sb.Append("A");
                Console.WriteLine("StringBuilder Capacity = " + sb.Length + " / " + sb.Capacity);
            }
            Console.WriteLine();
        }
    }
}
