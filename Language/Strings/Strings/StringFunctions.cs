﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Strings
{
    public class StringFunctions
    {
        public static void Concatination()
        {
            var plusConcat = "word" + " sweet " + "world";
            var concatFunction = string.Concat("word", " sweet ", "world");
            var ctorConcat = new string('*', 5); // *****
        }

        public static void Format()
        {
            string you = "You";
            string stringMethod = "string method";

            var oldStringFormat = String.Format("{0} can add variables ito this {1}", you, stringMethod);
            var newStringFormat = $"{you} can add variables ito this {stringMethod}";

            // Format int string
            var placeHolder1 = String.Format("{0:00#}", 10); // 010 (Ensures 3 whole number place holder)
            var placeHolder2 = String.Format("{0:D3}", 10);// 010 (Ensures 3 whole number place holder)

            var decimalPlaceHolder1 = String.Format("{0:#0.00}", 1); // 1.00 (Ensures 2 decimal place holders and 1 whole number place holder)
            var decimalPlaceHolder2 = String.Format("{0:F2}", 1);// 1.00 (Ensures 2 decimal place holders and 1 whole number place holder)

            // Additional text
            var additionalTextAtEnd = String.Format("{0:00#a}", 9999); // 9999a ("a" will always be last)
            var additionalTextInFront = String.Format("{0:a00#}", 9999); // a9999 ("a" will always be first)
            var additionalTextInMiddle = String.Format("{0:0a0#}", 999999); // 999a99 ("a" will always be at the third last position)
        }

        public static void SubstringModifications()
        {
            string myString = "* A Short String 2. *";

            var substring = myString.Substring(4, 2); // sh
            var firstCharRemoved = myString.Remove(0, 1); // A Short String 2. *
            var wordReplace = myString.Replace("Short", "Long"); // * A Long String 2. *
            var charReplace = myString.Replace('S', 'X'); // * A Xhort Xtring 2. *

            var replacedNumbers = Regex.Replace(myString, "[0-9]", "x"); // * A Short String x. *
            var replacedNonNumbers = Regex.Replace(myString, "[^0-9]", "x"); // xxxxxxxxxxxxxxxxx2xxx
            var replacedLetters = Regex.Replace(myString, "[A-Za-z]", "x"); // * x xxxxx xxxxxx 2. *
            var replacedNonLetters = Regex.Replace(myString, "[^A-Za-z]", "x"); // xxAxShortxStringxxxxx
            var replacedNonLettersNonNumbers = Regex.Replace(myString, "[^A-Za-z0-9 _]", "x"); // x A Short String 2x x

            var lotsOfWhiteSpace = "    hello    world      ";
            var trimmedStarAndEnd = lotsOfWhiteSpace.Trim(); // "hello    world"
            var trimmedEnd = lotsOfWhiteSpace.TrimEnd(new Char[] { ' ', '*', '.' }); // "    hello    world"
            var trimmedStart = lotsOfWhiteSpace.TrimStart(new Char[] { ' ', '*', '.' }); // "hello    world      "
        }

        public static void ComparisonFunctions()
        {
            string myString = "abc";

            var isEqual = myString.Equals("abc");
            var isEqualIgnoringCase = myString.Equals("abc", StringComparison.OrdinalIgnoreCase);
            var doesContain = myString.Contains("b");
            var doesStartWith = myString.StartsWith("a");
            var doesEndWith = myString.EndsWith("c");
        }

        public static void CountCharacters()
        {
            string myString = "hello";

            //length
            int numberOfCharacters1 = myString.Length;

            //count
            int numberOfCharacters2 = myString.Count();
            int numberOfLetters = myString.Count(char.IsLetter);
            int numberOfDigits = myString.Count(char.IsDigit);

            //get index
            var repetitiveString = "abc abc";
            var indexOfFirstOccurance = repetitiveString.IndexOf('a'); // 0
            var indexOfLastOccurance = repetitiveString.LastIndexOf('a'); // 4
        }

        public static void Delimiters()
        {
            string delimitedString = "one\ttwo three::four,five six seven";
            string[] splitStrings1 = delimitedString.Split(':');
            string[] splitStrings2 = delimitedString.Split(new Char[] { ' ', ',', '.', ':', '\t' });
            string[] splitStrings3 = delimitedString.Split(new Char[] { ' ', ',', '.', ':', '\t' }, System.StringSplitOptions.RemoveEmptyEntries);

            // Join array
            string[] words = new string[]{ "burgers","jet","frog" };
            string joinedString = (string.Join(";", words)); // burgers;jet;frog
        }
        
        public static void EscapeSequences()
        {
            string nextline = "\n";
            string tab = "\t";
            string backspace = "\b";
            string carriageReturn = "\r";
            string formFeed = "\f";
            string singleQuote = "\'";
            string doubleQuote = "\"";
            string backSlash = "\\";
            string ignoreSlashes = @"the \ slash is now ignored. so \n is treated as a literal";
        }

    }
}
