﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            StringDataType.StringsAreImmutable();

            string myString = "* A Short String 2. *";

            var replacedNumbers = Regex.Replace(myString, "[0-9]", "x");
            var replacedNonNumbers = Regex.Replace(myString, "[^0-9]", "x");
            var replacedLetters = Regex.Replace(myString, "[A-Za-z]", "x");
            var replacedNonLetters = Regex.Replace(myString, "[^A-Za-z]", "x");
            var replacedMany = Regex.Replace(myString, "[^A-Za-z0-9 _]", "x");

            Console.WriteLine(replacedNumbers);
            Console.WriteLine(replacedNonNumbers);
            Console.WriteLine(replacedLetters);
            Console.WriteLine(replacedNonLetters);
            Console.WriteLine(replacedMany);
        }
    }
}
