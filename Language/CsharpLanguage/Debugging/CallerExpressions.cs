﻿using System.Runtime.CompilerServices;

namespace Debugging;
public class CallerExpressions
{
    public static void Main()
    {
        GetCallerArgument("hello " + "there");
        GetCallerLineNumber("hello");
        GetCallerMemberName("hello");
    }
    
    public static void GetCallerArgument(string myParam, [CallerArgumentExpression("myParam")] string callerArgumentOfMyParam = "")
    {
        Console.WriteLine();
        Console.WriteLine($"{myParam} was passed in as: {callerArgumentOfMyParam}");
    }

    public static void GetCallerLineNumber(string message, [CallerLineNumber] int lineNumber = 0)
    {
        Console.WriteLine();
        Console.WriteLine($"{message} at line {lineNumber}");
    }

    public static void GetCallerMemberName(string message, [CallerMemberName] string caller = null)
    {
        Console.WriteLine();
        Console.WriteLine($"{message} from {caller}");
    }
}
