using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Mongo;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ScheduleAbstraction;

namespace AspHangfirePoc
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Environment.GetEnvironmentVariable("connectionstring");
           
            var db = Environment.GetEnvironmentVariable("db");
            if (db?.ToLower() == "mongo")
            {
                if (string.IsNullOrEmpty(connectionString))
                {
                    connectionString = "mongodb://adminuser:mypassword@localhost:27018/hangfire?authSource=admin";
                }

                services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseMongoStorage(connectionString, new MongoStorageOptions
                {
                    CheckConnection = true,
                    ConnectionCheckTimeout = new TimeSpan(0,0,30),
                    QueuePollInterval = new TimeSpan(0, 0, 30),
                    InvisibilityTimeout = TimeSpan.FromMinutes(5),
                    MigrationOptions = new MongoMigrationOptions()
                    {
                        Strategy = MongoMigrationStrategy.Migrate,
                        BackupStrategy = MongoBackupStrategy.Collections,
                        BackupPostfix = "hangfire_backup"
                    }
                }));
            }
            else
            {
                if (string.IsNullOrEmpty(connectionString))
                {
                    connectionString = "Server=(localdb)\\mssqllocaldb;Database=HangfireTestAspNetCore;Integrated Security=true";
                }

                services.AddHangfire(configuration => configuration
               .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
               .UseSimpleAssemblyNameTypeSerializer()
               .UseRecommendedSerializerSettings()
               .UseSqlServerStorage(connectionString, new SqlServerStorageOptions
               {
                   CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                   SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                   QueuePollInterval = TimeSpan.Zero,
                   UseRecommendedIsolationLevel = true,
                   UsePageLocksOnDequeue = true,
                   DisableGlobalLocks = true
               }));
            }

            Console.WriteLine("connectionstring = " + connectionString);

            // Add the processing server as IHostedService
            services.AddHangfireServer((options) => {
                options.WorkerCount = Environment.ProcessorCount * 5;
            });

            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute() 
            { 
                Attempts = 4, 
                DelaysInSeconds = new int[] { 1, 10, 30, 300 }
            });

            services.AddTransient<IScheduler, Scheduler>();
            services.AddTransient<MethodsToRun>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseHangfireDashboard();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
