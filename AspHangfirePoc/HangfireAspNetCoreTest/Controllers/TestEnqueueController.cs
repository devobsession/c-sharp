﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ScheduleAbstraction;

namespace AspHangfirePoc.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestEnqueueController : ControllerBase
    {
        private readonly IScheduler _scheduler;
        private readonly MethodsToRun _methodsToRun;

        public TestEnqueueController(IScheduler scheduler, MethodsToRun methodsToRun)
        {
            _scheduler = scheduler;
            _methodsToRun = methodsToRun;
        }

        [HttpGet("Log")]
        public IActionResult Log()
        {
            _scheduler.Enqueue(() => _methodsToRun.Log("Brandon"));
            return Ok("Success");
        }

        [HttpGet("LongWork")]
        public IActionResult LongWork()
        {
            _scheduler.Enqueue(() => _methodsToRun.LongWork(60));
            return Ok("Success");
        }

        [HttpGet("Error")]
        public IActionResult Error()
        {
            _scheduler.Enqueue(() => _methodsToRun.Error("Broken"));
            return Ok("Success");
        }

        [HttpGet("AsyncWork")]
        public IActionResult AsyncWork()
        {
            _scheduler.Enqueue(() => _methodsToRun.AsyncWork().GetAwaiter().GetResult());
            return Ok("Success");
        }
    }
}
