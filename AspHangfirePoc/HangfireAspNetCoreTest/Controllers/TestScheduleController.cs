﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ScheduleAbstraction;

namespace AspHangfirePoc.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestScheduleController : ControllerBase
    {
        private readonly IScheduler _scheduler;
        private readonly MethodsToRun _methodsToRun;

        public TestScheduleController(IScheduler scheduler, MethodsToRun methodsToRun)
        {
            _scheduler = scheduler;
            _methodsToRun = methodsToRun;
        }
        
        [HttpGet("Log")]
        public IActionResult Log()
        {
            _scheduler.Schedule(() => _methodsToRun.Log("Brandon"), DateTime.Now.AddSeconds(10));
            return Ok("Success");
        }

        [HttpGet("LongWork")]
        public IActionResult LongWork()
        {
            _scheduler.Schedule(() => _methodsToRun.LongWork(60), DateTime.Now.AddSeconds(30));
            return Ok("Success");
        }

        [HttpGet("Error")]
        public IActionResult Error()
        {
            _scheduler.Schedule(() => _methodsToRun.Error("Broken"), DateTime.Now.AddSeconds(10));
            return Ok("Success");
        }

        [HttpGet("AsyncWork")]
        public IActionResult AsyncWork()
        {
            _scheduler.Schedule(() => _methodsToRun.AsyncWork().GetAwaiter().GetResult(), DateTime.Now.AddSeconds(10));
            return Ok("Success");
        }

    }
}
