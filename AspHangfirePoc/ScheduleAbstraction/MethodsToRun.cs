﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleAbstraction
{
    public class MethodsToRun
    {

        public void Log(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "Brandon";
            }
            Console.WriteLine("hello "+ name);
        }

        public void LongWork(int seconds = 10)
        {
            Console.WriteLine("Work STARTED");
            if (seconds <= 0)
            {
                seconds = 10;
            }

            Thread.Sleep(1000 * seconds);
            Console.WriteLine("Work DONE");
        }

        public void Error(string errorMessage = "Test error")
        {
            throw new Exception(errorMessage);
        }

        public async Task AsyncWork()
        {
            Console.WriteLine("Async Work STARTED");
            await Task.Delay(1000);
            await Task.Run(() => {
                Console.WriteLine("Async Work DONE");
            });
        }

    }
}
