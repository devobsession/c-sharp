﻿using Hangfire;
using System;
using System.Linq.Expressions;

namespace ScheduleAbstraction
{
    public class Scheduler : IScheduler
    {
        private readonly IBackgroundJobClient _backgroundJobs;

        public Scheduler(IBackgroundJobClient backgroundJobs)
        {
            _backgroundJobs = backgroundJobs;
        }

        public string Schedule(Expression<Action> action, DateTimeOffset dateTimeUtc)
        {
            var jobId = _backgroundJobs.Schedule(action, dateTimeUtc);
            return jobId;
        }

        public string Enqueue(Expression<Action> action)
        {
            var jobId = _backgroundJobs.Enqueue(action);
            return jobId;
        }

    }
}
