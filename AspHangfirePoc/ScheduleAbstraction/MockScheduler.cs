﻿using Hangfire;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleAbstraction
{
    public class MockScheduler : IScheduler
    {       
        public string Enqueue(Expression<Action> action)
        {
            try
            {
                var compiledExpression = action.Compile();
                compiledExpression();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return Guid.NewGuid().ToString();
        }

        public string Schedule(Expression<Action> action, DateTimeOffset dateTimeUtc)
        {
            Task.Run(() =>
            {
                while (DateTime.UtcNow < dateTimeUtc)
                {
                    Thread.Sleep(450);
                }

                var compiledExpression = action.Compile();
                compiledExpression();
            });

            return Guid.NewGuid().ToString();
        }
    }
}
