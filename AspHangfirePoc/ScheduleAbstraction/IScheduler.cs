﻿using System;
using System.Linq.Expressions;

namespace ScheduleAbstraction
{
    public interface IScheduler
    {
        string Enqueue(Expression<Action> action);
        string Schedule(Expression<Action> action, DateTimeOffset dateTimeUtc);
    }
}