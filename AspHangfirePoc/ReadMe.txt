
1. docker build -t thf .

2. docker network create --driver bridge testnet

3. Run a database container

====== Sql ======
docker run -e ACCEPT_EULA=Y -e SA_PASSWORD=zaq1ZAQ! -p 1434:1433 -d --name sqlserver --network testnet --net-alias ss mcr.microsoft.com/mssql/server:2017-CU8-ubuntu

Connect to sql db and Run HangfireTableCreate.Sql
server name: 127.0.0.1,1434
login: sa
password: zaq1ZAQ!

====== Mongo ======
docker run -e MONGO_INITDB_ROOT_USERNAME=adminuser -e MONGO_INITDB_ROOT_PASSWORD=mypassword -p 27018:27017 -d --name mymongo --network testnet --net-alias mdb mongo:4

Connect to mongo db
connection string: mongodb://adminuser:mypassword@mymongo:27018/admin

4. Run multiple containers

====== Sql ======
docker run -DB=sql -e connectionstring="Server=ss,1433;Database=HangfireTestAspNetCore;User Id=sa;Password=zaq1ZAQ!;" --network testnet --net-alias thf1 --name thf1 -p 8091:80 thf
docker run -DB=sql -e connectionstring="Server=ss,1433;Database=HangfireTestAspNetCore;User Id=sa;Password=zaq1ZAQ!;" --network testnet --net-alias thf2 --name thf2 -p 8092:80 thf
docker run -DB=sql -e connectionstring="Server=ss,1433;Database=HangfireTestAspNetCore;User Id=sa;Password=zaq1ZAQ!;" --network testnet --net-alias thf3 --name thf3 -p 8093:80 thf
docker run -DB=sql -e connectionstring="Server=ss,1433;Database=HangfireTestAspNetCore;User Id=sa;Password=zaq1ZAQ!;" --network testnet --net-alias thf4 --name thf4 -p 8094:80 thf

====== Mongo ======
docker run -DB=mongo -e connectionstring="mongodb://adminuser:mypassword@mymongo:27018/hangfire?authSource=admin" --network testnet --net-alias thf1 --name thf1 -p 8091:80 thf
docker run -DB=mongo -e connectionstring="mongodb://adminuser:mypassword@mymongo:27018/hangfire?authSource=admin" --network testnet --net-alias thf2 --name thf2 -p 8092:80 thf
docker run -DB=mongo -e connectionstring="mongodb://adminuser:mypassword@mymongo:27018/hangfire?authSource=admin" --network testnet --net-alias thf3 --name thf3 -p 8093:80 thf
docker run -DB=mongo -e connectionstring="mongodb://adminuser:mypassword@mymongo:27018/hangfire?authSource=admin" --network testnet --net-alias thf4 --name thf4 -p 8094:80 thf

5. Hit endpoints to test

http://localhost:8091/TestSchedule/Log
http://localhost:8091/TestSchedule/LongWork
http://localhost:8091/TestSchedule/Error
http://localhost:8091/TestSchedule/AsyncWork

http://localhost:8091/TestEnqueue/Log
http://localhost:8091/TestEnqueue/LongWork
http://localhost:8091/TestEnqueue/Error
http://localhost:8091/TestEnqueue/AsyncWork